<map version="freeplane 1.5.9">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Arbor Code&#xa;v3" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1470990819970"><hook NAME="MapStyle">
    <properties fit_to_viewport="false;"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="15" RULE="ON_BRANCH_CREATION"/>
<attribute_layout NAME_WIDTH="47.36842105263158 pt" VALUE_WIDTH="47.36842105263158 pt"/>
<node TEXT=".git" FOLDED="true" POSITION="right" ID="ID_1706345735" CREATED="1470990787472" MODIFIED="1470990787497" LINK="file:./.git/">
<edge COLOR="#808080"/>
<attribute_layout NAME_WIDTH="47.36842105263158 pt" VALUE_WIDTH="47.36842105263158 pt"/>
<node TEXT="branches" ID="ID_591055382" CREATED="1470990787498" MODIFIED="1470990787499" LINK="file:./.git/branches/"/>
<node TEXT="hooks" FOLDED="true" ID="ID_425439299" CREATED="1470990787500" MODIFIED="1470990787501" LINK="file:./.git/hooks/">
<node TEXT="applypatch-msg.sample" ID="ID_1659513847" CREATED="1470990787503" MODIFIED="1470990787504" LINK="file:./.git/hooks/applypatch-msg.sample"/>
<node TEXT="commit-msg.sample" ID="ID_837272141" CREATED="1470990787504" MODIFIED="1470990787504" LINK="file:./.git/hooks/commit-msg.sample"/>
<node TEXT="post-update.sample" ID="ID_1956562660" CREATED="1470990787505" MODIFIED="1470990787505" LINK="file:./.git/hooks/post-update.sample"/>
<node TEXT="pre-applypatch.sample" ID="ID_1301983278" CREATED="1470990787505" MODIFIED="1470990787506" LINK="file:./.git/hooks/pre-applypatch.sample"/>
<node TEXT="pre-commit.sample" ID="ID_1340054615" CREATED="1470990787506" MODIFIED="1470990787507" LINK="file:./.git/hooks/pre-commit.sample"/>
<node TEXT="pre-push.sample" ID="ID_216396426" CREATED="1470990787507" MODIFIED="1470990787508" LINK="file:./.git/hooks/pre-push.sample"/>
<node TEXT="pre-rebase.sample" ID="ID_555087499" CREATED="1470990787509" MODIFIED="1470990787509" LINK="file:./.git/hooks/pre-rebase.sample"/>
<node TEXT="prepare-commit-msg.sample" ID="ID_632684361" CREATED="1470990787510" MODIFIED="1470990787511" LINK="file:./.git/hooks/prepare-commit-msg.sample"/>
<node TEXT="update.sample" ID="ID_1744266938" CREATED="1470990787511" MODIFIED="1470990787512" LINK="file:./.git/hooks/update.sample"/>
</node>
<node TEXT="info" FOLDED="true" ID="ID_619945827" CREATED="1470990787513" MODIFIED="1470990787519" LINK="file:./.git/info/">
<node TEXT="exclude" ID="ID_1062937416" CREATED="1470990787520" MODIFIED="1470990787521" LINK="file:./.git/info/exclude"/>
</node>
<node TEXT="logs" FOLDED="true" ID="ID_1052769832" CREATED="1470990787521" MODIFIED="1470990787522" LINK="file:./.git/logs/">
<node TEXT="refs" FOLDED="true" ID="ID_1552523656" CREATED="1470990787522" MODIFIED="1470990787523" LINK="file:./.git/logs/refs/">
<node TEXT="heads" FOLDED="true" ID="ID_1198909465" CREATED="1470990787523" MODIFIED="1470990787524" LINK="file:./.git/logs/refs/heads/">
<node TEXT="master" ID="ID_103957191" CREATED="1470990787525" MODIFIED="1470990787525" LINK="file:./.git/logs/refs/heads/master"/>
</node>
<node TEXT="remotes" FOLDED="true" ID="ID_1737538228" CREATED="1470990787526" MODIFIED="1470990787527" LINK="file:./.git/logs/refs/remotes/">
<node TEXT="origin" FOLDED="true" ID="ID_708790536" CREATED="1470990787527" MODIFIED="1470990787528" LINK="file:./.git/logs/refs/remotes/origin/">
<node TEXT="HEAD" ID="ID_1785400623" CREATED="1470990787529" MODIFIED="1470990787530" LINK="file:./.git/logs/refs/remotes/origin/HEAD"/>
</node>
</node>
</node>
<node TEXT="HEAD" ID="ID_513609700" CREATED="1470990787531" MODIFIED="1470990787532" LINK="file:./.git/logs/HEAD"/>
</node>
<node TEXT="objects" FOLDED="true" ID="ID_122877387" CREATED="1470990787533" MODIFIED="1470990787534" LINK="file:./.git/objects/">
<node TEXT="info" ID="ID_509871967" CREATED="1470990787534" MODIFIED="1470990787535" LINK="file:./.git/objects/info/"/>
<node TEXT="pack" FOLDED="true" ID="ID_1047251578" CREATED="1470990787536" MODIFIED="1470990787537" LINK="file:./.git/objects/pack/">
<node TEXT="pack-f4cf0a3c6b65c3cbe8df79374932fb9450ca3e68.idx" ID="ID_588705626" CREATED="1470990787537" MODIFIED="1470990787538" LINK="file:./.git/objects/pack/pack-f4cf0a3c6b65c3cbe8df79374932fb9450ca3e68.idx"/>
<node TEXT="pack-f4cf0a3c6b65c3cbe8df79374932fb9450ca3e68.pack" ID="ID_1223073011" CREATED="1470990787538" MODIFIED="1470990787538" LINK="file:./.git/objects/pack/pack-f4cf0a3c6b65c3cbe8df79374932fb9450ca3e68.pack"/>
</node>
</node>
<node TEXT="refs" FOLDED="true" ID="ID_1914026389" CREATED="1470990787539" MODIFIED="1470990787540" LINK="file:./.git/refs/">
<node TEXT="heads" FOLDED="true" ID="ID_81261859" CREATED="1470990787540" MODIFIED="1470990787541" LINK="file:./.git/refs/heads/">
<node TEXT="master" ID="ID_1766676941" CREATED="1470990787542" MODIFIED="1470990787542" LINK="file:./.git/refs/heads/master"/>
</node>
<node TEXT="remotes" FOLDED="true" ID="ID_1047505346" CREATED="1470990787543" MODIFIED="1470990787543" LINK="file:./.git/refs/remotes/">
<node TEXT="origin" FOLDED="true" ID="ID_1682113325" CREATED="1470990787544" MODIFIED="1470990787544" LINK="file:./.git/refs/remotes/origin/">
<node TEXT="HEAD" ID="ID_1620390154" CREATED="1470990787545" MODIFIED="1470990787553" LINK="file:./.git/refs/remotes/origin/HEAD"/>
</node>
</node>
<node TEXT="tags" ID="ID_854408897" CREATED="1470990787554" MODIFIED="1470990787554" LINK="file:./.git/refs/tags/"/>
</node>
<node TEXT="HEAD" ID="ID_1639295901" CREATED="1470990787555" MODIFIED="1470990787555" LINK="file:./.git/HEAD"/>
<node TEXT="config" ID="ID_1516467669" CREATED="1470990787555" MODIFIED="1470990787556" LINK="file:./.git/config"/>
<node TEXT="description" ID="ID_793036647" CREATED="1470990787556" MODIFIED="1470990787557" LINK="file:./.git/description"/>
<node TEXT="index" ID="ID_338789246" CREATED="1470990787557" MODIFIED="1470990787558" LINK="file:./.git/index"/>
<node TEXT="packed-refs" ID="ID_707161147" CREATED="1470990787558" MODIFIED="1470990787558" LINK="file:./.git/packed-refs"/>
</node>
<node TEXT="examples" POSITION="right" ID="ID_922515461" CREATED="1470990787923" MODIFIED="1470990787924" LINK="file:./examples/">
<edge COLOR="#808080"/>
<attribute_layout NAME_WIDTH="47.36842105263158 pt" VALUE_WIDTH="47.36842105263158 pt"/>
<node TEXT="Arbor3_libo.xml" ID="ID_1969106949" CREATED="1470990787924" MODIFIED="1470990787924" LINK="file:./examples/Arbor3_libo.xml"/>
</node>
<node TEXT="CMakeLists.txt" POSITION="right" ID="ID_1443174940" CREATED="1470990788094" MODIFIED="1470990788095" LINK="file:./CMakeLists.txt">
<edge COLOR="#808080"/>
<attribute_layout NAME_WIDTH="47.36842105263158 pt" VALUE_WIDTH="47.36842105263158 pt"/>
</node>
<node TEXT="README" POSITION="right" ID="ID_1966189591" CREATED="1470990788095" MODIFIED="1470990788095" LINK="file:./README">
<edge COLOR="#808080"/>
<attribute_layout NAME_WIDTH="47.36842105263158 pt" VALUE_WIDTH="47.36842105263158 pt"/>
</node>
<node TEXT="source" POSITION="right" ID="ID_747832051" CREATED="1470990787980" MODIFIED="1470990787981" LINK="file:./source/">
<edge COLOR="#808080"/>
<attribute_layout NAME_WIDTH="47.36842105263158 pt" VALUE_WIDTH="47.36842105263158 pt"/>
<node TEXT="CaloTracking" ID="ID_1560297669" CREATED="1470990787981" MODIFIED="1470990787982" LINK="file:./source/CaloTracking/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="include" FOLDED="true" ID="ID_1349477472" CREATED="1470990787982" MODIFIED="1470990787983" LINK="file:./source/CaloTracking/include/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="CaloEventGen.h" ID="ID_424803292" CREATED="1470990787984" MODIFIED="1470990787985" LINK="file:./source/CaloTracking/include/CaloEventGen.h"/>
<node TEXT="CaloHYBTrack.h" ID="ID_218276243" CREATED="1470990787985" MODIFIED="1470990787985" LINK="file:./source/CaloTracking/include/CaloHYBTrack.h"/>
<node TEXT="CaloHit.h" ID="ID_853784345" CREATED="1470990787985" MODIFIED="1470990787986" LINK="file:./source/CaloTracking/include/CaloHit.h"/>
<node TEXT="CaloKalDetector.h" ID="ID_515983411" CREATED="1470990787986" MODIFIED="1470990787986" LINK="file:./source/CaloTracking/include/CaloKalDetector.h"/>
<node TEXT="CaloMeasLayer.h" ID="ID_531332914" CREATED="1470990787987" MODIFIED="1470990787987" LINK="file:./source/CaloTracking/include/CaloMeasLayer.h"/>
<node TEXT="CaloTest.h" ID="ID_1527170829" CREATED="1470990787987" MODIFIED="1470990787988" LINK="file:./source/CaloTracking/include/CaloTest.h"/>
<node TEXT="CaloVKalDetector.h" ID="ID_640915836" CREATED="1470990787988" MODIFIED="1470990787988" LINK="file:./source/CaloTracking/include/CaloVKalDetector.h"/>
<node TEXT="CaloVMeasLayer.h" ID="ID_1695822552" CREATED="1470990787988" MODIFIED="1470990787989" LINK="file:./source/CaloTracking/include/CaloVMeasLayer.h"/>
</node>
<node TEXT="src" ID="ID_1890877056" CREATED="1470990787989" MODIFIED="1470990787990" LINK="file:./source/CaloTracking/src/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="CaloEventGen.cc" ID="ID_1709474818" CREATED="1470990787991" MODIFIED="1470990787992" LINK="file:./source/CaloTracking/src/CaloEventGen.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="CaloHYBTrack.cc" ID="ID_1278502584" CREATED="1470990787992" MODIFIED="1470990787992" LINK="file:./source/CaloTracking/src/CaloHYBTrack.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="CaloHit.cc" ID="ID_1538919541" CREATED="1470990787992" MODIFIED="1470990787993" LINK="file:./source/CaloTracking/src/CaloHit.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="CaloKalDetector.cc" ID="ID_1213565076" CREATED="1470990787993" MODIFIED="1470990787993" LINK="file:./source/CaloTracking/src/CaloKalDetector.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="CaloMeasLayer.cc" ID="ID_1573083341" CREATED="1470990787994" MODIFIED="1470990787994" LINK="file:./source/CaloTracking/src/CaloMeasLayer.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="CaloTest.cc" ID="ID_602377966" CREATED="1470990787994" MODIFIED="1470990787994" LINK="file:./source/CaloTracking/src/CaloTest.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="CaloVKalDetector.cc" ID="ID_100517420" CREATED="1470990787995" MODIFIED="1470990787995" LINK="file:./source/CaloTracking/src/CaloVKalDetector.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="CaloVMeasLayer.cc" ID="ID_1802486856" CREATED="1470990787995" MODIFIED="1470990787996" LINK="file:./source/CaloTracking/src/CaloVMeasLayer.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
</node>
</node>
<node TEXT="Clustering" ID="ID_1332032620" CREATED="1470990787997" MODIFIED="1470990787997" LINK="file:./source/Clustering/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="include" FOLDED="true" ID="ID_1078987793" CREATED="1470990787998" MODIFIED="1470990787998" LINK="file:./source/Clustering/include/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="Arbor3.h" ID="ID_927159279" CREATED="1470990787999" MODIFIED="1470990787999" LINK="file:./source/Clustering/include/Arbor3.h"/>
<node TEXT="ArborClustering.h" ID="ID_959395848" CREATED="1470990788000" MODIFIED="1470990788000" LINK="file:./source/Clustering/include/ArborClustering.h"/>
<node TEXT="ECALBarrelClustering.h" ID="ID_1093239192" CREATED="1470990788000" MODIFIED="1470990788000" LINK="file:./source/Clustering/include/ECALBarrelClustering.h"/>
</node>
<node TEXT="src" ID="ID_339926214" CREATED="1470990788001" MODIFIED="1470990788001" LINK="file:./source/Clustering/src/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="Arbor3.cc" ID="ID_1002126827" CREATED="1470990788002" MODIFIED="1470990788002" LINK="file:./source/Clustering/src/Arbor3.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="ArborClustering.cc" ID="ID_1929832738" CREATED="1470990788002" MODIFIED="1470990788003" LINK="file:./source/Clustering/src/ArborClustering.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="ECALBarrelClustering.cc" ID="ID_129273969" CREATED="1470990788003" MODIFIED="1470990788003" LINK="file:./source/Clustering/src/ECALBarrelClustering.cc"/>
</node>
</node>
<node TEXT="Data" ID="ID_1308147894" CREATED="1470990788004" MODIFIED="1470990788005" LINK="file:./source/Data/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="include" FOLDED="true" ID="ID_1322785884" CREATED="1470990788005" MODIFIED="1470990788006" LINK="file:./source/Data/include/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="ArborCaloCluster.h" ID="ID_1762725623" CREATED="1470990788011" MODIFIED="1470990788012" LINK="file:./source/Data/include/ArborCaloCluster.h"/>
<node TEXT="ArborCaloHit.h" ID="ID_570292970" CREATED="1470990788012" MODIFIED="1470990788012" LINK="file:./source/Data/include/ArborCaloHit.h"/>
<node TEXT="ArborHit3.h" ID="ID_1193506291" CREATED="1470990788013" MODIFIED="1470990788013" LINK="file:./source/Data/include/ArborHit3.h"/>
<node TEXT="ArborTrack.h" ID="ID_877822690" CREATED="1470990788013" MODIFIED="1470990788014" LINK="file:./source/Data/include/ArborTrack.h"/>
</node>
<node TEXT="src" ID="ID_1604491544" CREATED="1470990788014" MODIFIED="1470990788015" LINK="file:./source/Data/src/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="ArborCaloCluster.cc" ID="ID_88976779" CREATED="1470990788016" MODIFIED="1470990788017" LINK="file:./source/Data/src/ArborCaloCluster.cc"/>
<node TEXT="ArborCaloHit.cc" ID="ID_887259620" CREATED="1470990788017" MODIFIED="1470990788018" LINK="file:./source/Data/src/ArborCaloHit.cc"/>
<node TEXT="ArborHit3.cc" ID="ID_1681024115" CREATED="1470990788018" MODIFIED="1470990788018" LINK="file:./source/Data/src/ArborHit3.cc"/>
<node TEXT="ArborTrack.cc" ID="ID_1932914636" CREATED="1470990788018" MODIFIED="1470990788019" LINK="file:./source/Data/src/ArborTrack.cc"/>
</node>
</node>
<node TEXT="Geometry" ID="ID_1702186742" CREATED="1470990788051" MODIFIED="1470990788051" LINK="file:./source/Geometry/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="include" FOLDED="true" ID="ID_759440284" CREATED="1470990788052" MODIFIED="1470990788052" LINK="file:./source/Geometry/include/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="ArborGeometry.h" ID="ID_711268088" CREATED="1470990788053" MODIFIED="1470990788054" LINK="file:./source/Geometry/include/ArborGeometry.h"/>
<node TEXT="EcalBarrel.h" ID="ID_153951221" CREATED="1470990788054" MODIFIED="1470990788054" LINK="file:./source/Geometry/include/EcalBarrel.h"/>
</node>
<node TEXT="src" ID="ID_683060985" CREATED="1470990788054" MODIFIED="1470990788055" LINK="file:./source/Geometry/src/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="ArborGeometry.cc" ID="ID_603202978" CREATED="1470990788055" MODIFIED="1470990788056" LINK="file:./source/Geometry/src/ArborGeometry.cc"/>
<node TEXT="EcalBarrel.cc" ID="ID_1997716299" CREATED="1470990788056" MODIFIED="1470990788056" LINK="file:./source/Geometry/src/EcalBarrel.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
</node>
</node>
<node TEXT="Processors" ID="ID_1528915332" CREATED="1470990788072" MODIFIED="1470990788073" LINK="file:./source/Processors/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="include" FOLDED="true" ID="ID_1113081074" CREATED="1470990788073" MODIFIED="1470990788074" LINK="file:./source/Processors/include/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="ArborDef.h" ID="ID_864977263" CREATED="1470990788075" MODIFIED="1470990788075" LINK="file:./source/Processors/include/ArborDef.h"/>
<node TEXT="ArborParameters.h" ID="ID_221391207" CREATED="1470990788075" MODIFIED="1470990788076" LINK="file:./source/Processors/include/ArborParameters.h"/>
<node TEXT="BushConnect3.h" ID="ID_59720683" CREATED="1470990788076" MODIFIED="1470990788076" LINK="file:./source/Processors/include/BushConnect3.h"/>
<node TEXT="CaloTrkBuildingProcessor.h" ID="ID_689347388" CREATED="1470990788076" MODIFIED="1470990788077" LINK="file:./source/Processors/include/CaloTrkBuildingProcessor.h"/>
<node TEXT="CaloTrkHitClusteringProcessor.h" ID="ID_332265669" CREATED="1470990788077" MODIFIED="1470990788077" LINK="file:./source/Processors/include/CaloTrkHitClusteringProcessor.h"/>
<node TEXT="MarlinArbor2.h" ID="ID_1638959189" CREATED="1470990788077" MODIFIED="1470990788078" LINK="file:./source/Processors/include/MarlinArbor2.h"/>
<node TEXT="MarlinArbor3.h" ID="ID_1286654779" CREATED="1470990788078" MODIFIED="1470990788078" LINK="file:./source/Processors/include/MarlinArbor3.h"/>
</node>
<node TEXT="src" ID="ID_72069296" CREATED="1470990788078" MODIFIED="1470990788079" LINK="file:./source/Processors/src/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="ArborParameters.cc" ID="ID_1303948422" CREATED="1470990788080" MODIFIED="1470990788080" LINK="file:./source/Processors/src/ArborParameters.cc"/>
<node TEXT="BushConnect3.cc" ID="ID_313370227" CREATED="1470990788080" MODIFIED="1470990788081" LINK="file:./source/Processors/src/BushConnect3.cc"/>
<node TEXT="CaloTrkBuildingProcessor.cc" ID="ID_1797617146" CREATED="1470990788081" MODIFIED="1470990788081" LINK="file:./source/Processors/src/CaloTrkBuildingProcessor.cc"/>
<node TEXT="CaloTrkHitClusteringProcessor.cc" ID="ID_865441531" CREATED="1470990788081" MODIFIED="1470990788081" LINK="file:./source/Processors/src/CaloTrkHitClusteringProcessor.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="MarlinArbor2.cc" ID="ID_138600425" CREATED="1470990788082" MODIFIED="1470990788082" LINK="file:./source/Processors/src/MarlinArbor2.cc"/>
<node TEXT="MarlinArbor3.cc" ID="ID_128857812" CREATED="1470990788082" MODIFIED="1470990788082" LINK="file:./source/Processors/src/MarlinArbor3.cc"/>
</node>
<node TEXT="README" ID="ID_179622463" CREATED="1470990788083" MODIFIED="1470990788083" LINK="file:./source/Processors/README">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
</node>
<node TEXT="monitoring" ID="ID_1335918112" CREATED="1470990788091" MODIFIED="1470990788092" LINK="file:./source/monitoring/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="calibration.py" ID="ID_1589058665" CREATED="1470990788092" MODIFIED="1470990788093" LINK="file:./source/monitoring/calibration.py">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="parameters.py" ID="ID_1070622520" CREATED="1470990788093" MODIFIED="1470990788093" LINK="file:./source/monitoring/parameters.py">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
</node>
<node TEXT="Unchanged" ID="ID_1036894696" CREATED="1474445822724" MODIFIED="1474445825803">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="Diagnosis" ID="ID_1764396021" CREATED="1470990788019" MODIFIED="1474445839294" LINK="file:./source/Diagnosis/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<font BOLD="true"/>
<node TEXT="include" FOLDED="true" ID="ID_361806074" CREATED="1470990788020" MODIFIED="1470990788021" LINK="file:./source/Diagnosis/include/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="AnaSinglePart.h" ID="ID_215396368" CREATED="1470990788026" MODIFIED="1470990788027" LINK="file:./source/Diagnosis/include/AnaSinglePart.h"/>
<node TEXT="AnaTreeNew.h" ID="ID_412536179" CREATED="1470990788027" MODIFIED="1470990788028" LINK="file:./source/Diagnosis/include/AnaTreeNew.h"/>
<node TEXT="ArborAna.h" ID="ID_1360155763" CREATED="1470990788028" MODIFIED="1470990788029" LINK="file:./source/Diagnosis/include/ArborAna.h"/>
<node TEXT="ArborPID.h" ID="ID_155325892" CREATED="1470990788030" MODIFIED="1470990788031" LINK="file:./source/Diagnosis/include/ArborPID.h"/>
<node TEXT="FragAna.h" ID="ID_1000287551" CREATED="1470990788031" MODIFIED="1470990788032" LINK="file:./source/Diagnosis/include/FragAna.h"/>
<node TEXT="HiggsRecoil.h" ID="ID_858480218" CREATED="1470990788032" MODIFIED="1470990788033" LINK="file:./source/Diagnosis/include/HiggsRecoil.h"/>
<node TEXT="HitEff.h" ID="ID_67955613" CREATED="1470990788033" MODIFIED="1470990788034" LINK="file:./source/Diagnosis/include/HitEff.h"/>
<node TEXT="PrintReco_MCP.h" ID="ID_1424131565" CREATED="1470990788034" MODIFIED="1470990788034" LINK="file:./source/Diagnosis/include/PrintReco_MCP.h"/>
<node TEXT="SepEff.h" ID="ID_925906940" CREATED="1470990788035" MODIFIED="1470990788035" LINK="file:./source/Diagnosis/include/SepEff.h"/>
<node TEXT="TauAna.h" ID="ID_106397533" CREATED="1470990788035" MODIFIED="1470990788036" LINK="file:./source/Diagnosis/include/TauAna.h"/>
<node TEXT="TotalInvMass.h" ID="ID_931894091" CREATED="1470990788036" MODIFIED="1470990788036" LINK="file:./source/Diagnosis/include/TotalInvMass.h"/>
<node TEXT="TreeAna.h" ID="ID_689761962" CREATED="1470990788037" MODIFIED="1470990788037" LINK="file:./source/Diagnosis/include/TreeAna.h"/>
</node>
<node TEXT="src" ID="ID_853938493" CREATED="1470990788038" MODIFIED="1470990788039" LINK="file:./source/Diagnosis/src/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="AnaSinglePart.cc" ID="ID_589956262" CREATED="1470990788041" MODIFIED="1470990788042" LINK="file:./source/Diagnosis/src/AnaSinglePart.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="AnaTreeNew.cc" ID="ID_1721390901" CREATED="1470990788042" MODIFIED="1470990788043" LINK="file:./source/Diagnosis/src/AnaTreeNew.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="ArborAna.cc" ID="ID_1408288332" CREATED="1470990788043" MODIFIED="1470990788043" LINK="file:./source/Diagnosis/src/ArborAna.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="ArborPID.cc" ID="ID_35155488" CREATED="1470990788043" MODIFIED="1470990788044" LINK="file:./source/Diagnosis/src/ArborPID.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="FragAna.cc" ID="ID_1433954963" CREATED="1470990788044" MODIFIED="1470990788045" LINK="file:./source/Diagnosis/src/FragAna.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="HiggsRecoil.cc" ID="ID_871196969" CREATED="1470990788045" MODIFIED="1470990788045" LINK="file:./source/Diagnosis/src/HiggsRecoil.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="HitEff.cc" ID="ID_1387689205" CREATED="1470990788046" MODIFIED="1470990788046" LINK="file:./source/Diagnosis/src/HitEff.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="PrintReco_MCP.cc" ID="ID_854560692" CREATED="1470990788046" MODIFIED="1470990788047" LINK="file:./source/Diagnosis/src/PrintReco_MCP.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="SepEff.cc" ID="ID_506557110" CREATED="1470990788047" MODIFIED="1470990788048" LINK="file:./source/Diagnosis/src/SepEff.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="TauAna.cc" ID="ID_838190823" CREATED="1470990788048" MODIFIED="1470990788048" LINK="file:./source/Diagnosis/src/TauAna.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="TotalInvMass.cc" ID="ID_14929511" CREATED="1470990788048" MODIFIED="1470990788049" LINK="file:./source/Diagnosis/src/TotalInvMass.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="TreeAna.cc" ID="ID_1729547617" CREATED="1470990788049" MODIFIED="1470990788050" LINK="file:./source/Diagnosis/src/TreeAna.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
</node>
</node>
<node TEXT="Ranger" ID="ID_157105583" CREATED="1470990788083" MODIFIED="1474445845428" LINK="file:./source/Ranger/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<font BOLD="true"/>
<node TEXT="include" FOLDED="true" ID="ID_753956945" CREATED="1470990788084" MODIFIED="1470990788085" LINK="file:./source/Ranger/include/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="Arbor.h" ID="ID_1665333141" CREATED="1470990788085" MODIFIED="1470990788086" LINK="file:./source/Ranger/include/Arbor.h"/>
<node TEXT="ArborHit.h" ID="ID_539068170" CREATED="1470990788086" MODIFIED="1470990788086" LINK="file:./source/Ranger/include/ArborHit.h"/>
<node TEXT="ArborTool.h" ID="ID_863409560" CREATED="1470990788087" MODIFIED="1470990788087" LINK="file:./source/Ranger/include/ArborTool.h"/>
<node TEXT="KDTreeLinkerAlgoT.h" ID="ID_349725317" CREATED="1470990788087" MODIFIED="1470990788087" LINK="file:./source/Ranger/include/KDTreeLinkerAlgoT.h"/>
<node TEXT="KDTreeLinkerToolsT.h" ID="ID_1755387197" CREATED="1470990788088" MODIFIED="1470990788088" LINK="file:./source/Ranger/include/KDTreeLinkerToolsT.h"/>
</node>
<node TEXT="src" ID="ID_1257658156" CREATED="1470990788088" MODIFIED="1470990788089" LINK="file:./source/Ranger/src/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="Arbor.cc" ID="ID_948506781" CREATED="1470990788089" MODIFIED="1470990788090" LINK="file:./source/Ranger/src/Arbor.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="ArborHit.cc" ID="ID_1139115489" CREATED="1470990788090" MODIFIED="1470990788090" LINK="file:./source/Ranger/src/ArborHit.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="ArborTool.cc" ID="ID_845442619" CREATED="1470990788091" MODIFIED="1470990788091" LINK="file:./source/Ranger/src/ArborTool.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
</node>
</node>
<node TEXT="PluginMatch" ID="ID_1999475398" CREATED="1470990788057" MODIFIED="1474445866548" LINK="file:./source/PluginMatch/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<font BOLD="true"/>
<node TEXT="include" FOLDED="true" ID="ID_1742599152" CREATED="1470990788058" MODIFIED="1470990788058" LINK="file:./source/PluginMatch/include/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="ArborToolLCIO.h" ID="ID_62641306" CREATED="1470990788063" MODIFIED="1470990788064" LINK="file:./source/PluginMatch/include/ArborToolLCIO.h"/>
<node TEXT="BushConnect.h" ID="ID_900784582" CREATED="1470990788064" MODIFIED="1470990788065" LINK="file:./source/PluginMatch/include/BushConnect.h"/>
<node TEXT="DetectorPos.h" ID="ID_1280223499" CREATED="1470990788065" MODIFIED="1470990788065" LINK="file:./source/PluginMatch/include/DetectorPos.h"/>
<node TEXT="G2CDArbor.h" ID="ID_1086527153" CREATED="1470990788065" MODIFIED="1470990788066" LINK="file:./source/PluginMatch/include/G2CDArbor.h"/>
<node TEXT="HelixClass.h" ID="ID_1503818583" CREATED="1470990788066" MODIFIED="1470990788066" LINK="file:./source/PluginMatch/include/HelixClass.h"/>
<node TEXT="MarlinArbor.h" ID="ID_668593383" CREATED="1470990788066" MODIFIED="1470990788067" LINK="file:./source/PluginMatch/include/MarlinArbor.h"/>
</node>
<node TEXT="src" ID="ID_165276777" CREATED="1470990788067" MODIFIED="1470990788068" LINK="file:./source/PluginMatch/src/">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
<node TEXT="ArborToolLCIO.cc" ID="ID_73406119" CREATED="1470990788068" MODIFIED="1470990788069" LINK="file:./source/PluginMatch/src/ArborToolLCIO.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="BushConnect.cc" ID="ID_1575936712" CREATED="1470990788069" MODIFIED="1470990788069" LINK="file:./source/PluginMatch/src/BushConnect.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="DetectorPos.cc" ID="ID_685054237" CREATED="1470990788069" MODIFIED="1470990788070" LINK="file:./source/PluginMatch/src/DetectorPos.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="G2CDArbor.cc" ID="ID_25363797" CREATED="1470990788070" MODIFIED="1470990788070" LINK="file:./source/PluginMatch/src/G2CDArbor.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="HelixClass.cc" ID="ID_1033701340" CREATED="1470990788071" MODIFIED="1470990788071" LINK="file:./source/PluginMatch/src/HelixClass.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
<node TEXT="MarlinArbor.cc" ID="ID_984946463" CREATED="1470990788071" MODIFIED="1470990788071" LINK="file:./source/PluginMatch/src/MarlinArbor.cc">
<attribute_layout NAME_WIDTH="47.78761142608526 pt" VALUE_WIDTH="47.78761142608526 pt"/>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
