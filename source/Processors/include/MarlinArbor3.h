#ifndef _MarlinArbor3_h_
#define _MarlinArbor3_h_

#include <string>
#include <iostream>

#include <EVENT/CalorimeterHit.h>
#include <EVENT/LCCollection.h>

#include <marlin/Processor.h>
#include <gear/GearMgr.h>

#include <Arbor3.h>

class MarlinArbor3 : public marlin::Processor
{
	public:

		Processor*  newProcessor() { return new MarlinArbor3 ; }

		MarlinArbor3();

		~MarlinArbor3() {};

		void init();

		void processEvent( LCEvent * evtP );

		void end();

		void LinkVisulization( LCEvent * evtPP, std::string Name, std::vector<CalorimeterHit*> Hits, linkcoll inputLinks );

		void MakeIsoHits( LCEvent * evtPP, std::vector<CalorimeterHit*> inputCaloHits, std::string outputBushCollection );

		static marlin::StringParameters* getParameters() { return arborParas; }
		static gear::GearMgr* getGearMgr() { return arborGearMgr; }

	protected:
		Arbor3 arbor;

		std::map<std::string, CaloHitType> m_hitTypeMap;

		std::string _colName;
		std::vector<std::string> _CalCollections;
		std::vector<std::string> _SimCalCollections;
		std::vector<std::string> _garlicCollections;
		std::vector<std::string> _endHitTrackCollections;
		std::vector<std::string> _EcalPreShowerCollections;

		std::vector<std::string> _EcalCalCollections;
		std::vector<std::string> _HcalCalCollections;
		std::vector<std::string> _LcalCalCollections;
		std::vector<std::string> _LhcalCalCollections;
		std::vector<std::string> _MuonHitCollections;

		int _eventNr;

        std::vector<std::string> CaloHitCollections;

		static marlin::StringParameters* arborParas;
		static gear::GearMgr* arborGearMgr;
};
#endif
