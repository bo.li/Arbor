#ifndef _MARLINARBOR2_H_
#define _MARLINARBOR2_H_

#include <marlin/Processor.h>
#include <EVENT/LCCollection.h>

#include "ArborCaloHit.h"
#include "ArborCaloCluster.h"
#include "ArborGeometry.h"

typedef std::pair<std::vector<ArborCaloHit>::iterator, std::vector<ArborCaloHit>::iterator> ArborCaloHitRange;

class MarlinArbor2  : public marlin::Processor
{
	public:

		Processor*  newProcessor() { return new MarlinArbor2 ; }

		MarlinArbor2();

		void init();

		void processEvent(LCEvent* evt);

		void end();

		void FillCaloHit(LCEvent* evt);

		void ClearCaloHit();

		void hitClustering();

		void writeCluster2LCIO(ArborCaloHitRange& range, LCCollection* clusterVec);

		void writeCluster2LCIO(ArborCaloCluster& cluster, LCCollection* clusterVec);

        void FindNearHitsInLayer(std::vector<ArborCaloCluster*>* moduleClusterVec, 
								 ARBORCALOHITS&                  hitsInLayer, 
								 int                             newLayer);

		static marlin::StringParameters* getParameters() { return arborParas; }

	private:

		std::vector<std::string> m_CaloHitCollections;

	   	std::map<std::string, CaloHitType> m_hitTypeMap;

		std::vector<std::string> _EcalCalCollections;
		std::vector<std::string> _HcalCalCollections;
	
		std::vector<ArborCaloHit> m_CaloHitVectors[CaloHitType::CALOHITTYPES];

	   	std::map<std::string, LCCollection*> m_clusterCol;

		// steering parameters
		double m_hitDistanceECalStarting;
		double m_hitDistanceECal;

		int    m_Test;

		static marlin::StringParameters* arborParas;

		//int    m_coreHit;

		//std::vector< std::vector<EcalBarrelModule> > m_ecalBarrelModules;
};

#endif
