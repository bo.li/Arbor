#ifndef _BushConnect3_h_
#define _BushConnect3_h_

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <marlin/Processor.h>
#include <EVENT/CalorimeterHit.h>
#include <EVENT/Cluster.h>
#include <EVENT/MCParticle.h>
#include <EVENT/ReconstructedParticle.h>
#include <EVENT/Track.h>
#include <IMPL/LCFlagImpl.h>
#include <TNtuple.h>

#include <TTree.h>
#include <TFile.h>
#include <TH3.h>
#include <TVector3.h>

#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/Config.h"


class TTree;

  class BushConnect3  : public marlin::Processor
  {
  public:

    Processor*  newProcessor() { return new BushConnect3 ; }
 
    BushConnect3();
    
    ~BushConnect3() {};

    void init();

    void CleanVectors();

    void NewClusterFlag(Cluster* a_tree, Track* a_trk, LCCollection *col_TPCTrk);

    void SortTrack(EVENT::LCEvent* evtPP);
	void SortTrack2(LCEvent* evt);

    void SelfMergeBush(EVENT::LCEvent* evtPP);
    void SelfMergeBush2(EVENT::LCEvent* evtPP);
	
    void TagCore(EVENT::LCEvent* evtPP);

    void ReconstructParticle(LCEvent * evtPP);

    void processEvent( LCEvent * evtP );   

    void end();


  protected:

	std::string _treeFileName;
    std::vector<std::string> _BushCollections; 
    std::vector<std::string> _HitCollections;
    LCFlagImpl Cluflag;
    std::ostream *_output;
    std::vector<Cluster*> SortedSMBushes;
    std::vector<Track*> SortedTracks;
    std::map<Track*, float> Track_Energy;
    std::map<Track*, TVector3> Track_P3;
    std::map<Track*, int> Track_Type;
    std::map<Track*, float> Track_Theta;
    std::map<Track*, float> Track_Phi;	
  
    std::map<Cluster*, int> ClusterType_1stID;
    std::map<ReconstructedParticle*, int> ChCoreID; 

    std::vector<Cluster*> ecalchcore_tight;         //TightCores
    std::vector<Cluster*> ecalchcore_medium;
    std::vector<Cluster*> ecalchcore_loose;         //LooseCores    Let's also get
    std::vector<Cluster*> ecalchcore; 		    //Above three
    std::vector<Cluster*> ecalnecore;
    std::vector<Cluster*> ecalnecore_EM;
    std::vector<Cluster*> ecalnecore_NonEM;
    std::vector<Cluster*> ecalfrag;
    std::vector<Cluster*> ecalundef;
    std::vector<Cluster*> ecalfrag_TBM_CH;
    std::vector<Cluster*> ecalfrag_TBM_NE_EM;
    std::vector<Cluster*> ecalfrag_TBM_NE_NonEM;
    std::vector<Cluster*> ecalundef_iso;
    std::vector<Cluster*> ecalpotentialbackscattering;

    std::vector<Cluster*> chargedclustercore;
    std::vector<Cluster*> chargedclustercore_abs;
    std::vector<Cluster*> neutralClusterVec;

    std::map<Track*, TVector3> trkendposition;
    std::map<Track*, TVector3> TrackEndPoint;       //Last hit
    std::map<Track*, TVector3> TrackStartPoint;
    std::map<Cluster*, float> CluFD; 
    std::map<Cluster*, float> CluEnergy;

	// TMVA based PID
    std::vector<TMVA::Reader*> reader_all;
    int _eventNr;

    TTree *_outputTree;
	
    float _Dis[3], _DisEP, _Time, _EClu, _AngDiff, _TrkP3[3];
    int _Type, _NCircle;		
	int EcalNHit, HcalNHit, CluNHit, NLEcal, NLHcal, NH[16], NL[16];
	float maxDisHtoL, minDisHtoL, avDisHtoL, avEnDisHtoL;
	float EcalEn, HcalEn, EClu, graDepth, cluDepth, graAbsDepth, maxDepth, minDepth, MaxDisHel, MinDisHel, FD_all, FD_ECAL, FD_HCAL, FD[16];
	float crdis, EEClu_L10, EEClu_R, EEClu_r, EEClu_p, rms_Ecal, rms_Hcal, rms_Ecal2, rms_Hcal2, av_NHE, av_NHH;
        int AL_Ecal, AL_Hcal;
	float FD_ECALF10, FD_ECALL20;
	int NH_ECALF10, NH_ECALL20;
	int CluIDFlag, ClusterID;
	float dEdx, mvaVal_pi, mvaVal_mu, mvaVal_e, cosTheta, Phi;
	float _EcalNHit;
	float _HcalNHit;
	float _NLEcal;
	float _NLHcal;
	float _av_NHH;
	float _AL_Ecal;
	float _NH_ECALF10;
	float EE, E_10, E_R, E_r;
		
	int eventNr;
	float mvacut_pi=0.5;
	float mvacut_mu=0.5;
	float mvacut_e=0.5;
	int _outputtestroot;
  };
#endif
