#ifndef _ARBORPARAMETERS_
#define _ARBORPARAMETERS_

class ArborParameterManager
{
public:

	//singleton
	static ArborParameterManager& GetArborParameterManager();

	static void Delete();

private:
	ArborParameterManager();

	static ArborParameterManager* m_ArborParameterManager;

	//void GetParameter(std::string paraName);
};

#endif
