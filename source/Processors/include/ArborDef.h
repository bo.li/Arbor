#ifndef _ARBORDEF_H_
#define _ARBORDEF_H_

enum CaloHitType {
	 UnknownType = 0,        //
	 ECALBarrel,
	 ECALEndcap,
	 ECALOther,
	 HCALBarrel,
	 HCALEndcap,
	 HCALOther,
	 LCAL,
	 LHCAL,
	 MUON,
	 CALOHITTYPES            //
};

enum StringType {
	DecoderString = 0,
	LayerString,
	ModuleString,
	StaveString,
	STRINGTYPES
};

enum ModuleSide {
	RIGHT = 0,
	LEFT, 
	UP,
	DOWN,
	TOP,
	BOTTOM,
	NSIDES
};

#endif
