#include "ArborParameters.h"

#include <iostream>

ArborParameterManager* ArborParameterManager::m_ArborParameterManager = 0;

ArborParameterManager& ArborParameterManager::GetArborParameterManager()
{ 
	if(!m_ArborParameterManager) {
		m_ArborParameterManager = new ArborParameterManager();
	}

	return *m_ArborParameterManager;
}

ArborParameterManager::ArborParameterManager()
{
	std::cout << "ArborParameterManager constructor" << std::endl;
}

void ArborParameterManager::Delete()
{
	delete m_ArborParameterManager;
	m_ArborParameterManager = 0;
}
