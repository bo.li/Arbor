#include "MarlinArbor2.h"
#include "ArborClustering.h"
#include "ECALBarrelClustering.h"
#include "EcalBarrel.h"

#include <IMPL/ClusterImpl.h>
#include <IMPL/LCFlagImpl.h>
#include <IMPL/LCCollectionVec.h>

#include <marlin/Global.h>

#include <TVector3.h>

#include <string>
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;
using namespace lcio;
using namespace marlin;

string clusterCollectionNames[] = {
   "ArborHitsByLayer",
   "ArborCores",
   "ArborMergedCores"
};

marlin::StringParameters* MarlinArbor2::arborParas = 0;

MarlinArbor2 aMarlinArbor2;
MarlinArbor2::MarlinArbor2() : Processor("MarlinArbor2")
{
    _description = "Hit clustering algorithm for calorimeter" ;

	vector<string> EcalCaloHitCollections;

	//FIXME
	EcalCaloHitCollections.push_back(string("ECALBarrel"));
	EcalCaloHitCollections.push_back(string("ECALEndcap"));

	registerInputCollections( LCIO::CALORIMETERHIT,     
		                      "EcalHitCollections" ,
		                      "Hit Collection Names" ,
		                      _EcalCalCollections ,
		                      EcalCaloHitCollections);
	///
	vector<string> HcalCaloHitCollections;

	//FIXME
    HcalCaloHitCollections.push_back(std::string("HCALBarrel"));
    
	registerInputCollections( LCIO::CALORIMETERHIT,      
                              "HcalHitCollections" ,
                              "Hit Collection Names" ,
                              _HcalCalCollections ,
                              HcalCaloHitCollections);

	registerProcessorParameter("HitsDistanceEcalStarting",
		    	               "The distance cut of hits in Ecal at the starting of clustering",
								m_hitDistanceECalStarting,
								double(20.));

	registerProcessorParameter("HitsDistanceEcal",
		    	               "The distance cut of hits in Ecal for clustering",
								m_hitDistanceECal,
								double(20.));

	registerProcessorParameter("Test",
		    	               "A test parameter",
								m_Test,
								int(21));
}

void MarlinArbor2::init() 
{
	printParameters();

	for(unsigned int i1 = 0; i1 < _EcalCalCollections.size(); ++i1) {
		m_CaloHitCollections.push_back(_EcalCalCollections[i1]);
	}

	for(unsigned int i2 = 0; i2 < _HcalCalCollections.size(); ++i2) {
		m_CaloHitCollections.push_back(_HcalCalCollections[i2]);
	}

	// the map between name of hit (string) and the hit type (CaloHitType)
	m_hitTypeMap["ECALBarrel"] = ECALBarrel;
	m_hitTypeMap["ECALEndcap"] = ECALEndcap;
	m_hitTypeMap["ECALOther" ] = ECALOther;
	m_hitTypeMap["HCALBarrel"] = HCALBarrel;
	m_hitTypeMap["HCALEndcap"] = HCALEndcap;
	m_hitTypeMap["HCALOther" ] = HCALOther;
	m_hitTypeMap["LCAL"      ] = LCAL;
	m_hitTypeMap["LHCAL"     ] = LHCAL;
	m_hitTypeMap["MUON"      ] = MUON;

	StringParameters* paras = parameters();

	arborParas = paras;
}

void MarlinArbor2::processEvent(LCEvent * evt)
{
	int test = arborParas->getIntVal("Test");

	std::cout << "Test : " << test << std::endl;
	//if(paras!=NULL) {
	//	paras->
	//}

#if 1
	cout << "MarlinArbor2::process event " << evt->getEventNumber() << endl;

	LCFlagImpl cluFlag;
	cluFlag.setBit(LCIO::CHBIT_LONG);

	for(auto it=std::begin(clusterCollectionNames); it!=std::end(clusterCollectionNames); ++it) {
		LCCollectionVec* clusteVec = new LCCollectionVec(LCIO::CLUSTER);
		clusteVec->setFlag(cluFlag.getFlag());
	   	m_clusterCol[*it] = clusteVec;
	}

	FillCaloHit(evt);
	hitClustering();

	for(auto it=std::begin(clusterCollectionNames); it!=std::end(clusterCollectionNames); ++it) {
	   	evt->addCollection(m_clusterCol[*it], *it);
	}

	ClearCaloHit();
#endif
}

void MarlinArbor2::end()
{
	EcalBarrel::Clear();

	streamlog_out(MESSAGE) << "MarlinArbor2 ends. " << std::endl;
}

/////////////////////////////////////////////////////////////////////////
// functions declared in MarlinArbor2
/////////////////////////////////////////////////////////////////////////
void MarlinArbor2::ClearCaloHit()
{
	for(int iStave=0; iStave<8; ++iStave) {
		for(int iModule=0; iModule<5; ++iModule) {
		   	EcalBarrel::GetEcalBarrel()->GetModule(iStave, iModule).ClearNearSideHits();
		}
	}

	for(int iCaloHitType=UnknownType; iCaloHitType<CALOHITTYPES; ++iCaloHitType) {
		m_CaloHitVectors[iCaloHitType].clear();
	}
}

void MarlinArbor2::FillCaloHit(LCEvent* evt)
{
	for(unsigned int iString=0; iString<m_CaloHitCollections.size(); ++iString) {
		std::cout << m_CaloHitCollections[iString] << std::endl;
	
		// get the hit type from the map of ( calorimeter hit colleciton -> hit type)
		// if the string of calorimeter hit colleciton is not found in the map,
		// the zero (i.e., UnknownType, see the defintion of CaloHitType) will be returned.
		CaloHitType hitType = m_hitTypeMap[m_CaloHitCollections[iString]];

		try {
			LCCollection* CaloHitCol = evt->getCollection(m_CaloHitCollections[iString]);

		    // for each CalorimeterHit in the collection
			for(int iCaloHit=0; iCaloHit<CaloHitCol->getNumberOfElements(); ++iCaloHit) {
    			CalorimeterHit* aCaloHit = 
    				    dynamic_cast<CalorimeterHit*>(CaloHitCol->getElementAt(iCaloHit));

				// m_CaloHitVectors is array of ArborCaloHit vector (in STL).
				// Each hit type correspond to a specified calorimeter hit collection or sub calorimeter.
				m_CaloHitVectors[hitType].emplace_back(ArborCaloHit(aCaloHit, hitType));
    		}
		}
		catch(lcio::DataNotAvailableException e) { 
			std::cout << "--------------- data not available : " << m_CaloHitCollections[iString] << std::endl;
		}

		//ArborCaloHit::printHitVector(m_CaloHitVectors[hitType]);
	}
}

// Write the hits in a range to the LCIO file
void MarlinArbor2::writeCluster2LCIO(ArborCaloHitRange& range, LCCollection* clusterCol) 
{
	if(clusterCol==NULL) return;

	ClusterImpl* cluster = new ClusterImpl;

	for(auto it=range.first; it<range.second; ++it) { 
		CalorimeterHit* caloHit = it->GetLCIOHit(); 
		cluster->addHit(caloHit, 1.);
	}

	clusterCol->addElement(cluster);
}

void MarlinArbor2::writeCluster2LCIO(ArborCaloCluster& cluster, LCCollection* clusterCol)
{
	if(clusterCol==NULL || cluster.isMergedByNearModuleCluster()) return;

	ClusterImpl* clusterLCIO = new ClusterImpl;

	for(int iLayer=0; iLayer<MAXLAYER; ++iLayer) {
		ARBORCALOHITS& hitsInLayer = cluster.GetHitsInLayer(iLayer);

		for(auto hitIt=hitsInLayer.begin(); hitIt!=hitsInLayer.end(); ++hitIt) {
			CalorimeterHit* caloHit = (*hitIt)->GetLCIOHit();
			clusterLCIO->addHit(caloHit, 1.);
		}
	}

	std::vector<ArborCaloCluster*>& mergedClusters = cluster.getMergedClusters();

	for(auto it=mergedClusters.begin(); it!=mergedClusters.end(); ++it) {
		std::cout << "---------- cluster with subcluster..." << std::endl;
		auto& mergedCluster = **it;

		for(int iLayer=0; iLayer<MAXLAYER; ++iLayer) {
			ARBORCALOHITS& hitsInLayer = mergedCluster.GetHitsInLayer(iLayer);
	
			for(auto hitIt=hitsInLayer.begin(); hitIt!=hitsInLayer.end(); ++hitIt) {
				CalorimeterHit* caloHit = (*hitIt)->GetLCIOHit();
				clusterLCIO->addHit(caloHit, 1.);
			}
		}
	}

	clusterCol->addElement(clusterLCIO);
}

void MarlinArbor2::hitClustering()
{
	//////////////////////////////////////
	// for each sub vector of ArborCaloHit
	//////////////////////////////////////
	for(int iCaloHitType=UnknownType; iCaloHitType<CALOHITTYPES; ++iCaloHitType) {
		std::vector<ArborCaloHit>& arborCaloHitVec = m_CaloHitVectors[iCaloHitType];

#if 1
		ArborClustering* clusteringAlgo = 0;
		switch(iCaloHitType) {
			case ECALBarrel:
				cout << "hit clustering in ECALBarrel" << endl;
				clusteringAlgo = new ECALBarrelClustering(arborCaloHitVec);
				clusteringAlgo->Clustering();
				clusteringAlgo->print();
				break;
			default: 
				;
		}
#endif

		// now we are just working for ECALBarrel
		if(iCaloHitType==ECALBarrel) {
			std::cout << "collection " << iCaloHitType << ", size: " << arborCaloHitVec.size() << std::endl; 

			// to be a function
			//////////////////////////////////////////////////////////////////////
			// sort the ArborCaloHit in this vector by stave, module and layer
			std::sort(arborCaloHitVec.begin(), arborCaloHitVec.end(), arborCaloHitCompare());
			
			auto it = arborCaloHitVec.begin();
  
			// the range will constain two iterators for the first hit and last hit
			// with the same stave, module and layer by using equal_range()
			vector<ArborCaloHitRange> Ranges;

			while (it!=arborCaloHitVec.end()) {
				ArborCaloHit& referenceHit = *it;

				ArborCaloHitRange bounds = 
				    std::equal_range(it, arborCaloHitVec.end(), referenceHit, arborCaloHitCompare());

				//std::cout << "stave: " << referenceHit.GetStave() << ", module: " << referenceHit.GetModule()
				//	      << ", layer: " << referenceHit.GetLayer() << std::endl;
				
				//////////////////////////// check if the hit is close to the module boundary
				int stave = referenceHit.GetStave();
				int module = referenceHit.GetModule();

				for(auto it=bounds.first; it<bounds.second; ++it) { 
					TVector3 hitPos = it->GetPosition();

					std::cout << "stave: " << stave << ", module: " << module 
						      << "  hit position: " << hitPos.X() << ", " << hitPos.Y() << ", " << hitPos.Z() 
							  << std::endl;

					std::vector< std::pair<ModuleSide, double> > sides = 
							  EcalBarrel::GetEcalBarrel()->GetModule(stave, module-1).getNearSides(hitPos);
					
					std::vector< std::pair<ModuleSide, double> > validSides;

					for(auto sideIt=sides.begin(); sideIt!=sides.end(); ++sideIt) {
						auto& side = (*sideIt).first;
						auto& distance = (*sideIt).second;
					   	//std::cout << "------------close side: " << side << std::endl;

						// the condition of valid side based on ILD geometry
						if(side==LEFT||side==TOP||side==BOTTOM||side==DOWN) {
							it->SetNearModuleBoundary();
							validSides.emplace_back(std::pair<ModuleSide, double>(side, distance));
							EcalBarrel::GetEcalBarrel()->GetModule(stave, module-1).PutHit2Side(it, side);
						}
				   	}

					it->SetNearSides(validSides);
				}
				////////////////////////////

				it += bounds.second - bounds.first;

				Ranges.push_back(bounds);
				writeCluster2LCIO(bounds, m_clusterCol["ArborHitsByLayer"]);
			}
			//////////////////////////////////////////////////////////////////////

			//ArborCaloHit::printHitVector(arborCaloHitVec);
		
			std::cout << "stave-module-layer size: " << Ranges.size() << std::endl;

			/////////////////////////////////////
			// try clustering
			///////////////////////////////////// 

			int currentStave  = -1;
			int currentModule = -1;

			std::vector< std::vector<ArborCaloCluster*> > allClusters; // for all stave and module

			int nModuleHited = 0;
			// what we want to do here is clustering hits in each MODULE, LAYER by LAYER
		    for(auto it=Ranges.begin(); it!=Ranges.end(); ++it) {
				ArborCaloHit& startingHit = *(it->first);

				int newStave  = startingHit.GetStave ();
				int newModule = startingHit.GetModule();
				int newLayer  = startingHit.GetLayer ();

				if(currentStave!=newStave || currentModule!=newModule) {
				   	currentStave  = newStave; 
					currentModule = newModule;

				    // make a new moduleClusterVec
					// this is for each stave-module
					std::cout << "a new module start *****************" << std::endl;
					allClusters.emplace_back(std::vector<ArborCaloCluster*>());
					++nModuleHited;
				}

				// collect all hits in this layer
				ARBORCALOHITS hitsInLayer;
				for(auto arborCaloHitIt=it->first; arborCaloHitIt!=it->second; ++arborCaloHitIt) {
					hitsInLayer.push_back(arborCaloHitIt);
					//std::cout << hitsInLayer[hitsInLayer.size()-1]->GetLCIOHit() << std::endl;
				}
#if 0
				for(auto it=hitsInLayer.begin(); it!=hitsInLayer.end(); ++it) {
					std::cout << "Checking: " << (**it).GetLCIOHit() << std::endl;
				}
#endif
				std::cout << hitsInLayer.size() << " hits in stave " << newStave << " module " << newModule 
					      << ", layer " << newLayer << std::endl;

				// the vector for storing all the clusters in the module
				// now it is empty
				auto* moduleClusterVec = &(allClusters[allClusters.size()-1]);

				// FIXME:: what if there is no cluster in the last layer
				// maybe retrieve the hits in last last layer... and we should change the cut distance

				//--> clusteringCloseHitsInLayer
				//std::cout << moduleClusterVec << ", size: " << moduleClusterVec->size() << std::endl;
				FindNearHitsInLayer(moduleClusterVec, hitsInLayer, newLayer);

				std::cout << "stave: " << newStave << ", module: " << newModule << ", layer: " << newLayer << 
					", cluster size in this module up to this layer: " << moduleClusterVec->size() << std::endl;

				for(auto it=moduleClusterVec->begin(); it!=moduleClusterVec->end(); ++it) {
					int nhitInCluster = (*it)->GetHitsNumber();
					if(nhitInCluster>=10) std::cout << "cluster hit: " << nhitInCluster << endl;
				}
			}

			std::cout << "allCluster size: " << allClusters.size() << "; check: " << nModuleHited << std::endl;

			// write all the clusters done by FindNearHitsInLayer()
			for(auto it1=allClusters.begin(); it1!=allClusters.end(); ++it1) {
				for(auto it2=(*it1).begin(); it2!=(*it1).end(); ++it2) {
					if((*it2)->GetHitsNumber()>=10) writeCluster2LCIO(**it2, m_clusterCol["ArborCores"]);
				}
			}

#if 1
			// merge cluster 1)
			// geometry merger at module boundary 
			for(auto it1=allClusters.begin(); it1!=allClusters.end(); ++it1) {
				for(auto it2=(*it1).begin(); it2!=(*it1).end(); ++it2) {
					ArborCaloCluster& cluster = **it2;
					if(cluster.GetHitsNumber()<10) continue;

					if(cluster.isNearModuleBoundary() && !cluster.isMergedByNearModuleCluster() ) {
						std::cout << "a cluster at boundary" << std::endl;
						//(*it2)->MergeNearCluster();
						/////////////////////////////////////////////////////////////////////////
						ARBORCALOHITS& hitsNearBoundary = cluster.GetHitsNearBoundary();
						std::cout << "the hits in cluster:  " << hitsNearBoundary.size() << std::endl;
						for(auto it=hitsNearBoundary.begin(); it!=hitsNearBoundary.end(); ++it) {
							auto& hit = **it;

							int stave = hit.GetStave();
							int module = hit.GetModule();

							std::cout << "stave: " << stave << ", module: "  << module << std::endl;

							// the near sides of this module
							auto nearSides = hit.GetNearSides();

							for(int iSide=0; iSide<nearSides.size(); ++iSide) {
								// one of the near sides in this module
								auto& side = nearSides[iSide].first;

								bool isExsiting = false;

								std::pair<EcalBarrelModule*, ModuleSide> anotherSide = 
								    EcalBarrel::GetEcalBarrel()->GetModule(stave, module-1).getNearModuleSide(side, isExsiting);

								if(!isExsiting) continue;

								std::cout << "the near side to this hit exsits" << std::endl;

								//std::pair<EcalBarrelModule*, ModuleSide> ;
								EcalBarrelModule* modulePtr = anotherSide.first;
							    ModuleSide moduleSide = anotherSide.second;
								std::vector<ARBORCALOHIT>& hitsInNearModule = modulePtr->GetHitNearSide(moduleSide);
								std::cout << "the near hits: " << hitsInNearModule.size() << std::endl;

								for(auto itNearMod=hitsInNearModule.begin(); itNearMod!=hitsInNearModule.end(); ++itNearMod) {
									auto& hitInNearModule = **itNearMod;
									hit.print();
									hitInNearModule.print();
									double hitsDist = hit.getDistanceWith(hitInNearModule);
									std::cout << "distance of two hits at near modules: " << hitsDist << std::endl;

									// FIXME
									if(hitsDist<30) {
										// merge clusters
									    ArborCaloCluster* clusterNearMod = hitInNearModule.GetCluster();
										std::cout << "cluster address: " << clusterNearMod << std::endl; 
										if(!clusterNearMod->isMergedByNearModuleCluster() && 
										   clusterNearMod->GetHitsNumber()>=5) { 
											std::cout << "merge two near clusters..." << std::endl;
											cluster.MergeNearModuleCluster(clusterNearMod); 
											std::cout << "merger done." << std::endl;
											break;
										}
									}// if distance cut is fulfilled
								}
							}
						}
						/////////////////////////////////////////////////////////////////////////
					}
				}
			}
#endif
			////// cores after merger
			for(auto it1=allClusters.begin(); it1!=allClusters.end(); ++it1) {
				for(auto it2=(*it1).begin(); it2!=(*it1).end(); ++it2) {
					if((*it2)->GetHitsNumber()>=5) writeCluster2LCIO(**it2, m_clusterCol["ArborMergedCores"]);
				}
			}

			// merge cluster
			// core cluster absorbs the nearby tiny cluster

			// allClusters: delete the clusters and c.clear();lear vector
			for(auto it1=allClusters.begin(); it1!=allClusters.end(); ++it1) {
				for(auto it2=(*it1).begin(); it2!=(*it1).end(); ++it2) {
					delete (*it2);
				}
			}

			//allClusters.clear();
		}

	}
}

void MarlinArbor2::FindNearHitsInLayer(std::vector<ArborCaloCluster*>* moduleClusterVec, 
									   ARBORCALOHITS&                  hitsInLayer, 
									   int                             newLayer)
{
#if 1
	if(moduleClusterVec->size()==0) {
	    //std::cout << "hitsInLayer: " << hitsInLayer.size() << std::endl;
	
		// the begining of clustering: there is no cluster in moduleClusterVec or
		// moduleClusterLastVec, so we are going to build clusters from hits in this layer.
		for(auto it=hitsInLayer.begin(); it!=hitsInLayer.end(); ++it) {
			for(auto it1=it+1; it1!=hitsInLayer.end(); ++it1) { 
				//std::cout << "hisDist: ";
				double hitDist = (**it).getDistanceWith(**it1);
				std::cout << "Dist: " << hitDist << std::endl;
				// two close hits can make a cluster
				// FIXME:: just for non-clustered hits?
				if(hitDist<m_hitDistanceECalStarting && !(**it1).isClustered()){ 
					std::cout << "distance OK. max dist(starting): " << m_hitDistanceECalStarting << std::endl;
					if((*it)->isClustered()) {
						//std::cout << "one hit is clustered by: ";
						ArborCaloCluster* cluster = (*it)->GetCluster();
						std::cout << cluster << std::endl;
						cluster->PutHitInLayer(*it1, newLayer);
					}
					else {
						//std::cout << "both hits are not clusterd. ";
						ArborCaloCluster* cluster = new ArborCaloCluster();
						//std::cout << "new cluster: " << cluster;
						cluster->PutHitInLayer(*it, newLayer);
						cluster->PutHitInLayer(*it1, newLayer);
						//std::cout << ". Hits filled." << endl;
						moduleClusterVec->push_back(cluster);
					}
				}//if 
			}//for

			// not a clustered hit in this layer, make a new cluster, to be a function
			if(!(*it)->isClustered()) {
				ArborCaloCluster* cluster = new ArborCaloCluster();
				cluster->PutHitInLayer(*it, newLayer);
				moduleClusterVec->push_back(cluster);
			}
		}//for

		std::cout << "clusters in the begining: " << moduleClusterVec->size() << std::endl;
	}
	else {
		std::cout << "----------a new layer starts and clusters exist----------" << std::endl;
		// If there is clusters already, get all clusters and use them to cluster the hits in this layer
		for(auto clusterIt=moduleClusterVec->begin(); clusterIt!=moduleClusterVec->end(); ++clusterIt) {
			ArborCaloCluster& cluster = **clusterIt;

			// get hits in the last layer
			// FIXME:: the last layer can be n-1 or n-2 layer ...(n is the current layer)
			ARBORCALOHITS& hitsInLastLayer = cluster.GetHitsInLastLayer();

			//TODO:
			// make a function to get hits (at this layer) in the near region of these hits at last layer 
			
			std::cout << "hits in one cluster of last layer, size: " << hitsInLastLayer.size() << std::endl;
			std::cout << "hits in this layer, size: " << hitsInLayer.size() << std::endl;

			for(auto hitIt=hitsInLastLayer.begin(); hitIt!=hitsInLastLayer.end(); ++hitIt) {
				ArborCaloHit& hit = **hitIt;         // a hit in the last layer

			   	for(auto hitItThisLayer=hitsInLayer.begin(); hitItThisLayer!=hitsInLayer.end(); ++hitItThisLayer) {
					ArborCaloHit& hitThisLayer = **hitItThisLayer;  // a hit in this layer
				
					if(hitThisLayer.isClustered()) continue; // check if clustered by this cluster ?

					double dist = hit.getDistanceWith(hitThisLayer);
					std::cout << "two hits dist: " << dist << ". max dist: " << m_hitDistanceECal << std::endl;

					if(dist<m_hitDistanceECal) {
						TVector3 hit1 = hit.GetPosition(); hit1.Print();
						TVector3 hit2 = hitThisLayer.GetPosition(); hit2.Print();

						cluster.PutHitInLayer(*hitItThisLayer, newLayer);
					}
				}// for hit in this layer
			}//for hit in the last clusters
		}// for clusters

#if 1
		// for the not clutered hits, try clustering with the hits in this layer
		for(auto hitIt=hitsInLayer.begin(); hitIt!=hitsInLayer.end(); ++hitIt) {
			if(!(*hitIt)->isClustered()) {
				////////////////////////////////////////////////////////////////////
				// FIXME:: copied code ??
				////////////////////////////////////////////////////////////////////
	        	for(auto clusterIt=moduleClusterVec->begin(); clusterIt!=moduleClusterVec->end(); ++clusterIt) {
	        		ArborCaloCluster& cluster = **clusterIt;

	        		// FIXME:: the last layer can be n-1 or n-2 layer ...(n is the current layer)
	        		ARBORCALOHITS& clusteredHits = cluster.GetHitsInLayer(newLayer);

	        		//TODO:
	        		// make a function to get hits (at this layer) in the near region of these hits at last layer 
	        		
	        		std::cout << "hits in one cluster of this layer, size: " << clusteredHits.size() << std::endl;

	        		for(auto clusteredHitIt=clusteredHits.begin(); clusteredHitIt!=clusteredHits.end(); ++clusteredHitIt) {
	        			ArborCaloHit& clusteredHit = **clusteredHitIt;  // a hit in this layer
	        		
	        			double dist = (**hitIt).getDistanceWith(clusteredHit);
	        			std::cout << "two hits dist: " << dist << ". max dist: " << m_hitDistanceECal << std::endl;

	        			if(dist<m_hitDistanceECal) {
	        				TVector3 hit1 = (**hitIt).GetPosition(); hit1.Print();
	        				TVector3 hit2 = clusteredHit.GetPosition(); hit2.Print();

	        				cluster.PutHitInLayer(*hitIt, newLayer);
							break;
	        			}
	        		}// for hit in this layer
	        	}// for clusters

				////////////////////////////////////////////////////////////////////
			}
		}
#endif

#if 1
		// for the still not clutered hits, making new cluster, to be a function
		for(auto hitItThisLayer=hitsInLayer.begin(); hitItThisLayer!=hitsInLayer.end(); ++hitItThisLayer) {
			if(!(*hitItThisLayer)->isClustered()) {
				ArborCaloCluster* cluster = new ArborCaloCluster();
				cluster->PutHitInLayer(*hitItThisLayer, newLayer);
				moduleClusterVec->push_back(cluster);
			}
		}
#endif
	}
#endif
}
