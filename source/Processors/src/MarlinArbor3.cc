#include <MarlinArbor3.h>
#include <ArborToolLCIO.h>

#include <EVENT/LCCollection.h>
#include <EVENT/CalorimeterHit.h>
#include <EVENT/LCFloatVec.h>
#include <EVENT/LCParameters.h>
#include <EVENT/LCGenericObject.h>
#include <IMPL/LCRelationImpl.h>
#include <IMPL/LCCollectionVec.h>
#include <IMPL/CalorimeterHitImpl.h>
#include <IMPL/LCFlagImpl.h>
#include <IMPL/ClusterImpl.h>

#include <marlin/Global.h>


#include <string>
#include <iostream>
#include <TVector3.h>
#include <vector>

using namespace std;
using namespace lcio ;
using namespace marlin ;

marlin::StringParameters* MarlinArbor3::arborParas   = 0;
gear  ::GearMgr*          MarlinArbor3::arborGearMgr = 0;

const int STAVEOFFSET = 100;

MarlinArbor3 aMarlinArbor3;

MarlinArbor3::MarlinArbor3()
             :Processor("MarlinArbor3")
{
    _description = "Arbor3 Algorithm in Marlin" ;

	// default collection
	std::vector<std::string> EcalCaloHitCollections;
	//EcalCaloHitCollections.push_back(std::string("ECALBarrel"));
	//EcalCaloHitCollections.push_back(std::string("ECALEndcap"));
	
	std::vector<std::string> HcalCaloHitCollections;
    //HcalCaloHitCollections.push_back(std::string("HCALBarrel"));

	std::vector<std::string> LcalCaloHitCollections;
	std::vector<std::string> LhcalCaloHitCollections;
	
	std::vector<std::string> MuonCaloHitCollections;

	/////
	registerInputCollections( LCIO::CALORIMETERHIT,     
			                  "EcalHitCollections" ,
			                  "Hit Collection Names" ,
			                  _EcalCalCollections ,
			                  EcalCaloHitCollections);

    registerInputCollections( LCIO::CALORIMETERHIT,     
                              "HcalHitCollections" ,
                              "Hit Collection Names" ,
                              _HcalCalCollections ,
                              HcalCaloHitCollections);

    registerInputCollections( LCIO::CALORIMETERHIT,     
                              "LcalHitCollections" ,
                              "Hit Collection Names" ,
                              _LcalCalCollections ,
                              LcalCaloHitCollections);

    registerInputCollections( LCIO::CALORIMETERHIT,     
                              "LhcalHitCollections" ,
                              "Hit Collection Names" ,
                              _LhcalCalCollections ,
                              LhcalCaloHitCollections);

    registerInputCollections( LCIO::CALORIMETERHIT,     
                              "MuonHitCollections" ,
                              "Hit Collection Names" ,
                              _MuonHitCollections,
                              MuonCaloHitCollections);
}

void MarlinArbor3::init() {

	printParameters();

	for(unsigned int i=0; i<_EcalCalCollections.size(); ++i) {
		CaloHitCollections.push_back(_EcalCalCollections[i].c_str());
	}

	for(unsigned int i=0; i<_HcalCalCollections.size(); ++i) {
		CaloHitCollections.push_back(_HcalCalCollections[i].c_str());
	}

	for(unsigned int i=0; i<_LcalCalCollections.size(); ++i) {
		CaloHitCollections.push_back(_LcalCalCollections[i].c_str());
	}

	for(unsigned int i=0; i<_LhcalCalCollections.size(); ++i) {
		CaloHitCollections.push_back(_LhcalCalCollections[i].c_str());
	}

	for(unsigned int i=0; i<_MuonHitCollections.size(); ++i) {
		CaloHitCollections.push_back(_MuonHitCollections[i].c_str());
	}

#if 0
	for(int iCol=0; iCol<CaloHitCollections.size(); ++iCol) {
		cout << "The CalorimeterHit collection name: " << CaloHitCollections[iCol] << endl;
	}
#endif

	// the map between name of hit (string) and the hit type (CaloHitType)
	m_hitTypeMap["ECALBarrel"] = ECALBarrel;
	m_hitTypeMap["ECALEndcap"] = ECALEndcap;
	m_hitTypeMap["ECALOther" ] = ECALOther;
	m_hitTypeMap["HCALBarrel"] = HCALBarrel;
	m_hitTypeMap["HCALEndcap"] = HCALEndcap;
	m_hitTypeMap["HCALOther" ] = HCALOther;
	m_hitTypeMap["LCAL"      ] = LCAL;
	m_hitTypeMap["LHCAL"     ] = LHCAL;
	m_hitTypeMap["MUON"      ] = MUON;


	// set the parameters
	arborParas = parameters();

	// set gear
	arborGearMgr = Global::GEAR;
	//std::cout << "----------------DETNAME: " << arborGearMgr->getDetectorName() << std::endl;
	
	// Arbor initialization from the steering and geometry parameters we get
	arbor.init();
}

void MarlinArbor3::LinkVisulization( LCEvent* evtPP, std::string colName, std::vector<CalorimeterHit*> caloHits, linkcoll inputLinks ) 
{
	LCCollectionVec *linkCol = new LCCollectionVec(LCIO::LCRELATION);

	LCFlagImpl linkflag;
	linkflag.setBit(LCIO::CHBIT_LONG);
	linkCol->setFlag(linkflag.getFlag());

	int nLink = inputLinks.size();

	//cout << "Link: " << nLink << endl;

	std::pair<int, int> currlink; 

	for(int iLink=0; iLink<nLink; ++iLink)
	{
		currlink = inputLinks[iLink];

		CalorimeterHit* aHit = caloHits[currlink.first];
		CalorimeterHit* bHit = caloHits[currlink.second];

		LCRelationImpl *hitLink = new LCRelationImpl(aHit, bHit);
		linkCol->addElement(hitLink);
	}

	evtPP->addCollection(linkCol, colName);
}

void MarlinArbor3::MakeIsoHits( LCEvent * evtPP, std::vector<CalorimeterHit*> inputCaloHits, std::string outputBushCollection )
{
	LCCollection * isohitcoll = new LCCollectionVec(LCIO::CALORIMETERHIT);

   	//FIXME: Need to verify. verify what?
	string initString = "M:3,S-1:3,I:9,J:9,K-1:6";         
	
	isohitcoll->parameters().setValue(LCIO::CellIDEncoding, initString);

	LCFlagImpl flag;
	flag.setBit(LCIO::CHBIT_LONG);                  
	flag.setBit(LCIO::CHBIT_ID1);
	flag.setBit(LCIO::RCHBIT_ENERGY_ERROR);
	isohitcoll->setFlag(flag.getFlag());

	int nhit = inputCaloHits.size();

	for(int i = 0; i < nhit; ++i) {
		CalorimeterHit* a_hit = inputCaloHits[i];
		CalorimeterHitImpl * collhit = new CalorimeterHitImpl();
		collhit->setPosition(a_hit->getPosition());
		collhit->setCellID0(a_hit->getCellID0());
		collhit->setCellID1(a_hit->getCellID1());
		collhit->setEnergy(a_hit->getEnergy());
		isohitcoll->addElement(collhit);
	}

	evtPP->addCollection(isohitcoll, outputBushCollection);
}

void MarlinArbor3::processEvent( LCEvent * evtP )
{
	if(evtP==NULL) return;

	_eventNr = evtP->getEventNumber();

	streamlog_out(MESSAGE) << "Event " << _eventNr << std::endl;

	// make a ArborHit3 vector from LCIO hit collection
	std::vector<ArborHit3>       inputABHit; 
	std::vector<CalorimeterHit*> inputHits;  
	std::vector<CalorimeterHit*> preCluHits;  
	std::vector<CalorimeterHit*> IsoHits;

	for(unsigned int iCaloHit=0; iCaloHit<CaloHitCollections.size(); ++iCaloHit)
	{
		try{

			LCCollection* CaloHitColl = evtP ->getCollection(CaloHitCollections[iCaloHit].c_str());
#if 1
			std::cout << "Sub Collection " << CaloHitCollections[iCaloHit].c_str() << ", size: " 
				      << CaloHitColl->getNumberOfElements() << endl; 
#endif


			//if(CaloHitCollections[iCaloHit]!="ECALBarrel") continue;
			//if(CaloHitCollections[iCaloHit]!="HCALBarrel") continue;
			//if(CaloHitCollections[iCaloHit]!="ECALEndcap") continue;
			//if(CaloHitCollections[iCaloHit]!="MUON") continue;

#if 1
			// FIXME
			// this is for testing each sub detector
			if(CaloHitCollections[iCaloHit]!="ECALBarrel" && 
			   CaloHitCollections[iCaloHit]!="ECALEndcap" &&
			   CaloHitCollections[iCaloHit]!="ECALOther"  &&
			   CaloHitCollections[iCaloHit]!="HCALBarrel" &&
			   CaloHitCollections[iCaloHit]!="HCALEndcap" &&
			   CaloHitCollections[iCaloHit]!="HCALOther"  &&
			   CaloHitCollections[iCaloHit]!="LCAL"       && 
			   CaloHitCollections[iCaloHit]!="LHCAL"      &&
			   CaloHitCollections[iCaloHit]!="MUON"
			   ) continue;
#endif
			
			//streamlog_out(DEBUG4)<<"Sub Collection "<<CaloHitCollections[iCaloHit].c_str()<<endl; 
	
			CaloHitType hitType = m_hitTypeMap[CaloHitCollections[iCaloHit]];

			int SubDId = -1; 

			// TODO: check staveOffset ... for LCAL and LHCAL
			if(hitType<=ECALOther||hitType==LCAL) 
				SubDId = 1;
			else if(hitType<=HCALOther||hitType==LHCAL)
				SubDId = 2;
			else if(hitType==MUON) 
				SubDId = 3;

			if(SubDId==3) cout << SubDId << endl;

			int staveOffset = hitType * STAVEOFFSET;
			
			for(int iCaloHit=0; iCaloHit<CaloHitColl->getNumberOfElements(); ++iCaloHit)
			{
				CalorimeterHit * caloHit = dynamic_cast<CalorimeterHit*>(CaloHitColl->getElementAt(iCaloHit));

				ArborHit3 abHit(caloHit, hitType, staveOffset, SubDId);
#if 0
				abHit.print();
#endif

				inputABHit.push_back(abHit);
				inputHits.push_back(caloHit);
			}
		} catch(lcio::DataNotAvailableException zero) { }
	} // the end of for loop

	cout << "inputABHit size: " << inputABHit.size() << endl;

	arbor.Clustering(inputABHit); 

	// -------------------------------------------------------------------------------
	// write preClusteredHits to lcio
	// -------------------------------------------------------------------------------
	bool writePreClusteredHits = true;

    std::vector<ArborHit3>& preClusteredHits = arbor.getPreClusteredHits();

	if(writePreClusteredHits) {
    	LCFlagImpl flag;
    	flag.setBit(LCIO::CHBIT_LONG);
    	flag.setBit(LCIO::RCHBIT_TIME);

    	LCCollectionVec *lcioHits = new LCCollectionVec(LCIO::CALORIMETERHIT);

    	lcioHits->setFlag(flag.getFlag());

		cout << "preClusteredHits.size: " << preClusteredHits.size() << endl;
    
    	for(int iHit=0; iHit<preClusteredHits.size(); ++iHit)
    	{
			//if(preClusteredHits[iHit].IsDropped() || preClusteredHits[iHit].GetTime()>100.) continue;
			//if(preClusteredHits[iHit].IsDropped()) continue;
			//preClusteredHits[iHit].print();

			CalorimeterHitImpl * calhit = new CalorimeterHitImpl();
			calhit->setEnergy(preClusteredHits[iHit].GetEnergy());

            TVector3 pos = preClusteredHits[iHit].GetPosition();
			
			float hitPos[3];
			hitPos[0] = pos.X();
			hitPos[1] = pos.Y();
			hitPos[2] = pos.Z();

			calhit->setPosition(hitPos);

			double hitTime = preClusteredHits[iHit].GetTime();
			calhit->setTime(hitTime);

			//FIXME::
			// the layer information is missing...
			calhit->setCellID0( preClusteredHits[iHit].GetCellID0() );
			calhit->setCellID1( preClusteredHits[iHit].GetCellID1() );

			preCluHits.push_back(calhit);

    		lcioHits->addElement(calhit);
    	}

		cout << "lcio hits size: " << lcioHits->getNumberOfElements() << endl;
    
    	evtP->addCollection(lcioHits, "PreClusteredHits");
	}
	// -------------------------------------------------------------------------------

	ClusterBuilding( evtP, "EHBushes", preCluHits, arbor.getTrees(), 0 );

	// FIXME:: does saving link need significant CPU time ? If yes, maybe we should
	// put these into an option of steering file.
	LinkVisulization(evtP, "Links_init",    preCluHits, arbor.getlinks_debug() );
	LinkVisulization(evtP, "Links_cleaned", preCluHits, arbor.getCleanedLinks() );
	LinkVisulization(evtP, "Links_iter_1",  preCluHits, arbor.getIterLinks_1() );
	LinkVisulization(evtP, "Links_iter_2",  preCluHits, arbor.getIterLinks() );

	// saving all the isolated hits
	std::vector<int>& isoHits = arbor.getIsoHitsIndex();
	cout << "iso hits size: " << isoHits.size() << endl;

	for(unsigned int iIsoHit = 0; iIsoHit < isoHits.size(); ++iIsoHit)
	{
		//cout << "iso hit index: " << isoHits[iIsoHit] << endl;
		//CalorimeterHit* a_Isohit = inputHits[isoHits[iIsoHit]];
		CalorimeterHit* a_Isohit = preClusteredHits[isoHits[iIsoHit]].GetLCIOHit();

		if(a_Isohit->getEnergy() > 0)
		{
			IsoHits.push_back(a_Isohit);
		}
	}

	// FIXME:: leave it as an option of steering file
	MakeIsoHits(evtP, IsoHits, "AllIsolatedHits");
}

void MarlinArbor3::end()
{
}
