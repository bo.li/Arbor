#ifndef HistogramManager_h 
#define HistogramManager_h 

//#include <iomanip>
#include <string>

#include "TTree.h"
#include "TFile.h"

class TDirectory;

using namespace std;

class HistogramManager {

private:
    TDirectory* hmdir;
    TFile* hmfile;
    string hmname;
//    std::vector<TH1*> histos;

public:
    HistogramManager(const char * filename="Monitoring.root");
    virtual ~HistogramManager();

    // Create (if doesn't exist) and Fill a fixed bin histogram (TH1F or TH2F)
    void CreateFill(string name,
                    float xvar, int nxbins, float xmin, float xmax,
                    float yvar=0, int nybins=0, float ymin=0, float ymax=0);
    // Create & Fill a flexible (rebinnable histogramm)
    void CreateFillFlex(string name,
                        float xvar, int nxbins, float xmin, float xmax,
                        float yvar=0, int nybins=0, float ymin=0, float ymax=0);    
    // Find name and Fill (no error provided)
    void Fill(string name, float xvar, float yvar=0);

    void Reset();
    void Write();
    TDirectory* GetDir(){ return hmdir; };
    TFile* GetFile(){ return hmfile; };

private:
    void CreateFillRstRbn(bool rst, bool rbn, string name,
                          float xvar, int nxbins, float xmin, float xmax, 
                          float yvar=0, int nybins=0, float ymin=0, float ymax=0);
//    void GetOrCreateHistograms(bool reset = false);
//    void Fill(MyTree* event);
//    void Fill(TTree* T, int nevent=9999999);

//    ClassDef(HistogramManager,1)  //Manages all histograms
};

#endif 		// HistogramManager_h
