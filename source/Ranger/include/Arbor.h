#ifndef _Arbor_h_
#define _Arbor_h_

#include "ArborHit.h"
#include <string>
#include <iostream>
#include <TVector3.h>
#include "ArborTool.h"

void init(float CellSize, float LayerThickness);

void HitsCleaning( std::vector<ArborHit> inputHits );

void HitsClassification( linkcoll inputLinks );

void BuildInitLink(float Threshold);

void LinkIteration(int time);

void BranchBuilding();

branchcoll Arbor( std::vector<ArborHit>, float CellSize, float LayerThickness );

/*
 int NLayer_A, NStave_A, SubD_A; 
 int NLayer_B, NStave_B, SubD_B;
 float MagA, MagB, Depth_A, Depth_B, ECCorr, DisAB; 
 int FlagTrkPS, FlagEH; 
 int FlagPSEE, FlagHH;       
 int FlagStaveSame;
 int FlagStaveDiff;
 TVector3 PosA, PosB, PosDiffAB, PosDiffBA, linkDir;
*/

#endif


