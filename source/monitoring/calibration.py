#!/usr/bin/python

#####################################
# $ python
# >>> execfile("name_of_this_file")
# >>> exit()
#####################################

import os

marlinStatus = os.system("Marlin xxx.xml > /dev/null 2>&1")
print 'The status code of Marlin: ', marlinStatus

##### draw a histogram #####
#from ROOT import gROOT, gRandom, TCanvas, TH1F
#
#hpx = TH1F( 'hpx', 'px', 100, -4, 4 )
#
#hpxFill = hpx.Fill                # cache bound method
#
#for i in xrange( 25000 ):
#	   px = gRandom.Gaus()
#	   hpxFill( px )              # use bound method: no lookup needed
#
#del hpxFill                       # done with cached method
#
#canvas = TCanvas( 'canvas', '', 200, 10, 700, 500 )
#hpx.Draw()
#
#canvas.Print("hist.pdf")
 
##### draw a function #####
#from ROOT import TF1
#gROOT.Reset()
#c1 = TCanvas( 'c1', 'Example with Formula', 200, 10, 700, 500 )
#	 
##
## Create a one dimensional function and draw it
##
#fun1 = TF1( 'fun1', 'abs(sin(x)/x)', 0, 10 )
#c1.SetGridx()
#c1.SetGridy()
#fun1.Draw()
#c1.Update()
