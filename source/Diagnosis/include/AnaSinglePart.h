#ifndef _AnaSinglePart_h_
#define _AnaSinglePart_h_

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <marlin/Processor.h>
#include <EVENT/CalorimeterHit.h>
#include <EVENT/ReconstructedParticle.h>
#include <TNtuple.h>
#include <TObject.h>
#include <TTree.h>
#include <TFile.h>
#include <TVector3.h>

class TTree;

class AnaSinglePart  : public marlin::Processor
{
	public:

		Processor*  newProcessor() { return new AnaSinglePart ; }

		AnaSinglePart();

		~AnaSinglePart() {};

		void init();

		void processEvent( LCEvent * evtP );
            
                
		void end();

	protected:
		std::string _treeFileName;
		std::ostream *_output;
		int _overwrite;
		TTree *_outputEvt;
		TTree *_outputMCP;
		TTree *_outputHit;
		TTree *_outputTrk;
		TTree *_outputClu;
		TTree *_outputPFO;
		TTree *_outputPFOp;
		TTree *_outputIsoH;
                int _eventNr, _Num, _filenum, _flag;

/////////////////////Evt///////////////////////////
		int _nMCP, _nSelMCP12B2, _nSelMCP12, _nSelMCP21, _nSelMCP11, _nSelMCP22;
		int _MCPOID, _HDCat;
		float _MCPOTheta, _MCPOPhi, _MCPOEn;
		float _MCPOPos[3];
		int _nSelMCP12B2LE, _nSelMCP12B2ME, _nSelMCP12B2HE;
		int _nSelMCP12B2CH, _nSelMCP12B2NE, _nSelMCP12B2Mu, _nSelMCP12B2Ele, _nSelMCP12B2Pi, _nSelMCP12B2Gam, _nSelMCP12B2Neu;
		int _nSelMCP12B2CHLE, _nSelMCP12B2NELE, _nSelMCP12B2MuLE, _nSelMCP12B2EleLE, _nSelMCP12B2PiLE, _nSelMCP12B2GamLE, _nSelMCP12B2NeuLE;
		int _nSelMCP12B2CHME, _nSelMCP12B2NEME, _nSelMCP12B2MuME, _nSelMCP12B2EleME, _nSelMCP12B2PiME, _nSelMCP12B2GamME, _nSelMCP12B2NeuME;
		int _nSelMCP12B2CHHE, _nSelMCP12B2NEHE, _nSelMCP12B2MuHE, _nSelMCP12B2EleHE, _nSelMCP12B2PiHE, _nSelMCP12B2GamHE, _nSelMCP12B2NeuHE;

		int _nTrk, _nTrkHQ;

		int _nHit;
		float _THEn;

		int _nClu;
		float _LCEn, _TCEn;
		float _minCluDepth;

		int _nPFO_a, _nCHPFOs_a, _nNEPFOs_a, _nCMIPPFOs_a, _nCEMPFOs_a, _nCHADPFOs_a, _nNEMPFOs_a, _nNHADPFOs_a;
		int _nPFO_p, _nCHPFOs_p, _nNEPFOs_p, _nCMIPPFOs_p, _nCEMPFOs_p, _nCHADPFOs_p, _nNEMPFOs_p, _nNHADPFOs_p;

		int _nPFO_aLE, _nCHPFOs_aLE, _nNEPFOs_aLE, _nCMIPPFOs_aLE, _nCEMPFOs_aLE, _nCHADPFOs_aLE, _nNEMPFOs_aLE, _nNHADPFOs_aLE;
		int _nPFO_aME, _nCHPFOs_aME, _nNEPFOs_aME, _nCMIPPFOs_aME, _nCEMPFOs_aME, _nCHADPFOs_aME, _nNEMPFOs_aME, _nNHADPFOs_aME;
		int _nPFO_aHE, _nCHPFOs_aHE, _nNEPFOs_aHE, _nCMIPPFOs_aHE, _nCEMPFOs_aHE, _nCHADPFOs_aHE, _nNEMPFOs_aHE, _nNHADPFOs_aHE;
		int _nPFO_pHE, _nCHPFOs_pHE, _nNEPFOs_pHE, _nCMIPPFOs_pHE, _nCEMPFOs_pHE, _nCHADPFOs_pHE, _nNEMPFOs_pHE, _nNHADPFOs_pHE;
		int _nPFO_pLE, _nCHPFOs_pLE, _nNEPFOs_pLE, _nCMIPPFOs_pLE, _nCEMPFOs_pLE, _nCHADPFOs_pLE, _nNEMPFOs_pLE, _nNHADPFOs_pLE;
		int _nPFO_pME, _nCHPFOs_pME, _nNEPFOs_pME, _nCMIPPFOs_pME, _nCEMPFOs_pME, _nCHADPFOs_pME, _nNEMPFOs_pME, _nNHADPFOs_pME;

		int _nPFOClu_a, _nPFOClu_p;
		float _TotalRecoP4_a[4], _TotalRecoP4_p[4];
		float _Mass_a, _Mass_p;
		float _TotChPFOEn_a, _TotChPFOEn_p, _TotNePFOEn_a, _TotNePFOEn_p;
		float _leadingPFOEn, _leadingNePFOEn, _leadingChPFOEn;
		float _LPFOPos[3], _LChPFOPos[3], _LNePFOPos[3];
		float _LChPFOTheta, _LNePFOTheta, _LChPFOPhi, _LNePFOPhi;
		
/////////////////////MCP///////////////////////////
		int _nP, _nD, _ChargeMCP, _PIDMCP, _MCPCat, _MCPCatB2;
		float _MCPVtx[3], _MCPEndP[3], _MCPP[4];
		float _MCPTheta, _MCPPhi;

/////////////////////Trk///////////////////////////
		int _NTrkHit, _TrackType;
		float _TrkP[3], _TstartP[3], _TendP[3];
		float _TTheta, _TPhi, _D0, _Z0, _Omega, _TanLambda;

/////////////////////Hit///////////////////////////
		int _HitCat, _HLayer, _NNb;
		float _HPos[3];
		float _HitEn, _HitTime;

/////////////////////Clu///////////////////////////

		int _NCalHit, _CluType;
		float _CluEn, _CPos[3];
		float _CluDepth, _DisTLC;
		

/////////////////////PFO///////////////////////////

		int _Charge_p, _Type_p;
		float _PPFO_p[4], _PFOCluEn_p;
		
		int _Charge_a, _Type_a, _MCTType_a;
		float _PPFO_a[4], _PFOCluEn_a;
		float _PFOTheta, _PFOPhi;
		float _trkD0, _trkZ0;
		float _trkSP[3], _trkEP[3];
		int  _NHTrk;
		float _P4MCTL[4];

/////////////////////ISO///////////////////////////
		float _CDepthIso, _CEnIso, _IsoPos[3];
		float _IsoHEn, _DisIHC;
		int _PFOType;

};

#endif




