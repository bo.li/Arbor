#include <AnaSinglePart.h>
#include "ArborTool.h"
#include "ArborToolLCIO.h"
#include "DetectorPos.h"
#include <EVENT/LCCollection.h>
#include <EVENT/MCParticle.h>
#include <EVENT/Track.h>
#include <EVENT/TrackerHit.h>
#include <EVENT/ReconstructedParticle.h>
#include <EVENT/CalorimeterHit.h>
#include <EVENT/SimCalorimeterHit.h>
#include <string>
#include <iostream>
#include <cmath>
#include <vector>
#include <stdlib.h>
#include <EVENT/LCFloatVec.h>
#include <EVENT/LCParameters.h>
#include <EVENT/Cluster.h>
#include <EVENT/LCRelation.h>
#include <UTIL/CellIDDecoder.h>
#include <UTIL/LCRelationNavigator.h>
#include <stdexcept>
#include <TFile.h> 
#include <TTree.h>
#include <TMath.h>
#include <Rtypes.h> 
#include <sstream>		
#include <TH1.h>
#include <TVector3.h>
#include <TLorentzVector.h>



using namespace std;

AnaSinglePart aAnaSinglePart ;
AnaSinglePart::AnaSinglePart()
	: Processor("AnaSinglePart"),
	_output(0)
{
	_description = "Cluster Ana" ;

	_treeFileName="AnaSinglePart.root";
	registerProcessorParameter( "TreeOutputFile" , 
			"The name of the file to which the ROOT tree will be written" ,
			_treeFileName ,
			_treeFileName);

	_overwrite=0;
	registerProcessorParameter( "OverwriteFile" , 
			"If zero an already existing file will not be overwritten." ,
			_overwrite ,
			_overwrite);

	_filenum=0;
	registerProcessorParameter( "FileNum" , 
			"File Numero" ,
			_filenum ,
			_filenum);

	_flag=111111;
	registerProcessorParameter( "Flag" , 
			"The running flag." ,
			_flag ,
			_flag);

}

void AnaSinglePart::init() {

	printParameters();

	TFile *tree_file=new TFile(_treeFileName.c_str(),(_overwrite ? "RECREATE" : "UPDATE"));

	if (!tree_file->IsOpen()) {
		delete tree_file;
		tree_file=new TFile(_treeFileName.c_str(),"NEW");
	}

	_outputEvt = new TTree("Evt", "Evt");
	_outputEvt->SetAutoSave(32*1024*1024); 
	_outputEvt->Branch("EventNr", &_eventNr,"EventNr/I");
	_outputEvt->Branch("Num", &_Num,"Num/I");  
	_outputEvt->Branch("File", &_filenum,"File/I");
	_outputEvt->Branch("NMCP", &_nMCP,"NMCP/I");
	_outputEvt->Branch("NSelMCP12B2", &_nSelMCP12B2, "NSelMCP12B2/I");
	_outputEvt->Branch("NSelMCP12B2CH", &_nSelMCP12B2CH, "NSelMCP12B2CH/I");
	_outputEvt->Branch("NSelMCP12B2NE", &_nSelMCP12B2NE, "NSelMCP12B2NE/I");
	_outputEvt->Branch("NSelMCP12B2LE", &_nSelMCP12B2LE, "NSelMCP12B2LE/I");
	_outputEvt->Branch("NSelMCP12B2ME", &_nSelMCP12B2ME, "NSelMCP12B2ME/I");
	_outputEvt->Branch("NSelMCP12B2HE", &_nSelMCP12B2HE, "NSelMCP12B2HE/I");
	_outputEvt->Branch("NSelMCP12B2CHLE", &_nSelMCP12B2CHLE, "NSelMCP12B2CHLE/I");
	_outputEvt->Branch("NSelMCP12B2CHME", &_nSelMCP12B2CHME, "NSelMCP12B2CHME/I");
	_outputEvt->Branch("NSelMCP12B2CHHE", &_nSelMCP12B2CHHE, "NSelMCP12B2CHHE/I");
	_outputEvt->Branch("NSelMCP12B2NELE", &_nSelMCP12B2NELE, "NSelMCP12B2NELE/I");
	_outputEvt->Branch("NSelMCP12B2NEME", &_nSelMCP12B2NEME, "NSelMCP12B2NEME/I");
	_outputEvt->Branch("NSelMCP12B2NEHE", &_nSelMCP12B2NEHE, "NSelMCP12B2NEHE/I");
	_outputEvt->Branch("NSelMCP12B2Mu", &_nSelMCP12B2Mu, "NSelMCP12B2Mu/I");
	_outputEvt->Branch("NSelMCP12B2Ele", &_nSelMCP12B2Ele, "NSelMCP12B2Ele/I");
	_outputEvt->Branch("NSelMCP12B2Pi", &_nSelMCP12B2Pi, "NSelMCP12B2Pi/I");
	_outputEvt->Branch("NSelMCP12B2Gam", &_nSelMCP12B2Gam, "NSelMCP12B2Gam/I");
	_outputEvt->Branch("NSelMCP12B2Neu", &_nSelMCP12B2Neu, "NSelMCP12B2Neu/I");
	_outputEvt->Branch("NSelMCP12B2MuLE", &_nSelMCP12B2MuLE, "NSelMCP12B2MuLE/I");
	_outputEvt->Branch("NSelMCP12B2EleLE", &_nSelMCP12B2EleLE, "NSelMCP12B2EleLE/I");
	_outputEvt->Branch("NSelMCP12B2PiLE", &_nSelMCP12B2PiLE, "NSelMCP12B2PiLE/I");
	_outputEvt->Branch("NSelMCP12B2GamLE", &_nSelMCP12B2GamLE, "NSelMCP12B2GamLE/I");
	_outputEvt->Branch("NSelMCP12B2NeuLE", &_nSelMCP12B2NeuLE, "NSelMCP12B2NeuLE/I");
	_outputEvt->Branch("NSelMCP12B2MuME", &_nSelMCP12B2MuME, "NSelMCP12B2MuME/I");
	_outputEvt->Branch("NSelMCP12B2EleME", &_nSelMCP12B2EleME, "NSelMCP12B2EleME/I");
	_outputEvt->Branch("NSelMCP12B2PiME", &_nSelMCP12B2PiME, "NSelMCP12B2PiME/I");
	_outputEvt->Branch("NSelMCP12B2GamME", &_nSelMCP12B2GamME, "NSelMCP12B2GamME/I");
	_outputEvt->Branch("NSelMCP12B2NeuME", &_nSelMCP12B2NeuME, "NSelMCP12B2NeuME/I");
	_outputEvt->Branch("NSelMCP12B2MuHE", &_nSelMCP12B2MuHE, "NSelMCP12B2MuHE/I");
	_outputEvt->Branch("NSelMCP12B2EleHE", &_nSelMCP12B2EleHE, "NSelMCP12B2EleHE/I");
	_outputEvt->Branch("NSelMCP12B2PiHE", &_nSelMCP12B2PiHE, "NSelMCP12B2PiHE/I");
	_outputEvt->Branch("NSelMCP12B2GamHE", &_nSelMCP12B2GamHE, "NSelMCP12B2GamHE/I");
	_outputEvt->Branch("NSelMCP12B2NeuHE", &_nSelMCP12B2NeuHE, "NSelMCP12B2NeuHE/I");
	_outputEvt->Branch("NSelMCP12", &_nSelMCP12, "NSelMCP12/I");
        _outputEvt->Branch("NSelMCP11", &_nSelMCP11, "NSelMCP11/I");
        _outputEvt->Branch("NSelMCP21", &_nSelMCP21, "NSelMCP21/I");
        _outputEvt->Branch("NSelMCP22", &_nSelMCP22, "NSelMCP22/I");
	_outputEvt->Branch("MCPTheta", &_MCPOTheta, "MCPTheta/F");
	_outputEvt->Branch("MCPPhi", &_MCPOPhi, "MCPPhi/F"); 
	_outputEvt->Branch("MCPPos", _MCPOPos, "MCPPos[3]/F");  
	_outputEvt->Branch("MCPEn", &_MCPOEn, "MCPEn/F");    
	_outputEvt->Branch("MCPID", &_MCPOID, "MCPID/I");    
	_outputEvt->Branch("HDCat", &_HDCat, "HDCat/I");
	_outputEvt->Branch("NTrk", &_nTrk,"NTrk/I");
	_outputEvt->Branch("NTrkHQ", &_nTrkHQ,"NTrkHQ/I");
	_outputEvt->Branch("NCalHit", &_nHit,"NCalHit/I"); 
	_outputEvt->Branch("THEn", &_THEn,"THEn/F");   
	_outputEvt->Branch("NClu", &_nClu,"NClu/I");  
	_outputEvt->Branch("TCEn", &_TCEn,"TCEn/F");    
	_outputEvt->Branch("LCEn", &_LCEn,"LCEn/F");  
	_outputEvt->Branch("MinCluDepth", &_minCluDepth,"MinCluDepth/F");
	_outputEvt->Branch("NPFOa", &_nPFO_a,"NPFOa/I");
	_outputEvt->Branch("NCHPFOa", &_nCHPFOs_a,"NCHPFOa/I");
	_outputEvt->Branch("NNEPFOa", &_nNEPFOs_a,"NNEPFOa/I");
	_outputEvt->Branch("NCMIPPFOa", &_nCMIPPFOs_a,"NCMIPPFOa/I");
	_outputEvt->Branch("NCEMPFOa", &_nCEMPFOs_a,"NCEMPFOa/I");
	_outputEvt->Branch("NCHADPFOa", &_nCHADPFOs_a,"NCHADPFOa/I");
	_outputEvt->Branch("NNEMPFOa", &_nNEMPFOs_a,"NNEMPFOa/I");
	_outputEvt->Branch("NNHADPFOa", &_nNHADPFOs_a,"NNHADPFOa/I");
	_outputEvt->Branch("NPFOp", &_nPFO_p,"NPFOp/I");
	_outputEvt->Branch("NCHPFOp", &_nCHPFOs_p,"NCHPFOp/I");
	_outputEvt->Branch("NNEPFOp", &_nNEPFOs_p,"NNEPFOp/I");
	_outputEvt->Branch("NCMIPPFOp", &_nCMIPPFOs_p,"NCMIPPFOp/I");
	_outputEvt->Branch("NCEMPFOp", &_nCEMPFOs_p,"NCEMPFOp/I");
	_outputEvt->Branch("NCHADPFOp", &_nCHADPFOs_p,"NCHADPFOp/I");
	_outputEvt->Branch("NNEMPFOp", &_nNEMPFOs_p,"NNEMPFOp/I");
	_outputEvt->Branch("NNHADPFOp", &_nNHADPFOs_p,"NNHADPFOp/I");
	_outputEvt->Branch("NPFOaLE", &_nPFO_aLE,"NPFOaLE/I");
	_outputEvt->Branch("NCHPFOaLE", &_nCHPFOs_aLE,"NCHPFOaLE/I");
	_outputEvt->Branch("NNEPFOaLE", &_nNEPFOs_aLE,"NNEPFOaLE/I");
	_outputEvt->Branch("NCMIPPFOaLE", &_nCMIPPFOs_aLE,"NCMIPPFOaLE/I");
	_outputEvt->Branch("NCEMPFOaLE", &_nCEMPFOs_aLE,"NCEMPFOaLE/I");
	_outputEvt->Branch("NCHADPFOaLE", &_nCHADPFOs_aLE,"NCHADPFOaLE/I");
	_outputEvt->Branch("NNEMPFOaLE", &_nNEMPFOs_aLE,"NNEMPFOaLE/I");
	_outputEvt->Branch("NNHADPFOaLE", &_nNHADPFOs_aLE,"NNHADPFOaLE/I");
	_outputEvt->Branch("NPFOpLE", &_nPFO_pLE,"NPFOpLE/I");
	_outputEvt->Branch("NCHPFOpLE", &_nCHPFOs_pLE,"NCHPFOpLE/I");
	_outputEvt->Branch("NNEPFOpLE", &_nNEPFOs_pLE,"NNEPFOpLE/I");
	_outputEvt->Branch("NCMIPPFOpLE", &_nCMIPPFOs_pLE,"NCMIPPFOpLE/I");
	_outputEvt->Branch("NCEMPFOpLE", &_nCEMPFOs_pLE,"NCEMPFOpLE/I");
	_outputEvt->Branch("NCHADPFOpLE", &_nCHADPFOs_pLE,"NCHADPFOpLE/I");
	_outputEvt->Branch("NNEMPFOpLE", &_nNEMPFOs_pLE,"NNEMPFOpLE/I");
	_outputEvt->Branch("NNHADPFOpLE", &_nNHADPFOs_pLE,"NNHADPFOpLE/I");
	_outputEvt->Branch("NPFOaME", &_nPFO_aME,"NPFOaME/I");
	_outputEvt->Branch("NCHPFOaME", &_nCHPFOs_aME,"NCHPFOaME/I");
	_outputEvt->Branch("NNEPFOaME", &_nNEPFOs_aME,"NNEPFOaME/I");
	_outputEvt->Branch("NCMIPPFOaME", &_nCMIPPFOs_aME,"NCMIPPFOaME/I");
	_outputEvt->Branch("NCEMPFOaME", &_nCEMPFOs_aME,"NCEMPFOaME/I");
	_outputEvt->Branch("NCHADPFOaME", &_nCHADPFOs_aME,"NCHADPFOaME/I");
	_outputEvt->Branch("NNEMPFOaME", &_nNEMPFOs_aME,"NNEMPFOaME/I");
	_outputEvt->Branch("NNHADPFOaME", &_nNHADPFOs_aME,"NNHADPFOaME/I");
	_outputEvt->Branch("NPFOpME", &_nPFO_pME,"NPFOpME/I");
	_outputEvt->Branch("NCHPFOpME", &_nCHPFOs_pME,"NCHPFOpME/I");
	_outputEvt->Branch("NNEPFOpME", &_nNEPFOs_pME,"NNEPFOpME/I");
	_outputEvt->Branch("NCMIPPFOpME", &_nCMIPPFOs_pME,"NCMIPPFOpME/I");
	_outputEvt->Branch("NCEMPFOpME", &_nCEMPFOs_pME,"NCEMPFOpME/I");
	_outputEvt->Branch("NCHADPFOpME", &_nCHADPFOs_pME,"NCHADPFOpME/I");
	_outputEvt->Branch("NNEMPFOpME", &_nNEMPFOs_pME,"NNEMPFOpME/I");
	_outputEvt->Branch("NNHADPFOpME", &_nNHADPFOs_pME,"NNHADPFOpME/I");
	_outputEvt->Branch("NPFOaHE", &_nPFO_aHE,"NPFOaHE/I");
	_outputEvt->Branch("NCHPFOaHE", &_nCHPFOs_aHE,"NCHPFOaHE/I");
	_outputEvt->Branch("NNEPFOaHE", &_nNEPFOs_aHE,"NNEPFOaHE/I");
	_outputEvt->Branch("NCMIPPFOaHE", &_nCMIPPFOs_aHE,"NCMIPPFOaHE/I");
	_outputEvt->Branch("NCEMPFOaHE", &_nCEMPFOs_aHE,"NCEMPFOaHE/I");
	_outputEvt->Branch("NCHADPFOaHE", &_nCHADPFOs_aHE,"NCHADPFOaHE/I");
	_outputEvt->Branch("NNEMPFOaHE", &_nNEMPFOs_aHE,"NNEMPFOaHE/I");
	_outputEvt->Branch("NNHADPFOaHE", &_nNHADPFOs_aHE,"NNHADPFOaHE/I");
	_outputEvt->Branch("NPFOpHE", &_nPFO_pHE,"NPFOpHE/I");
	_outputEvt->Branch("NCHPFOpHE", &_nCHPFOs_pHE,"NCHPFOpHE/I");
	_outputEvt->Branch("NNEPFOpHE", &_nNEPFOs_pHE,"NNEPFOpHE/I");
	_outputEvt->Branch("NCMIPPFOpHE", &_nCMIPPFOs_pHE,"NCMIPPFOpHE/I");
	_outputEvt->Branch("NCEMPFOpHE", &_nCEMPFOs_pHE,"NCEMPFOpHE/I");
	_outputEvt->Branch("NCHADPFOpHE", &_nCHADPFOs_pHE,"NCHADPFOpHE/I");
	_outputEvt->Branch("NNEMPFOpHE", &_nNEMPFOs_pHE,"NNEMPFOpHE/I");
	_outputEvt->Branch("NNHADPFOpHE", &_nNHADPFOs_pHE,"NNHADPFOpHE/I");
	_outputEvt->Branch("TotRecoP4a", _TotalRecoP4_a,"TotRecoP4a[4]/F"); 
	_outputEvt->Branch("TotRecoP4p", _TotalRecoP4_p,"TotRecoP4p[4]/F"); 
	_outputEvt->Branch("Mass_a", &_Mass_a,"Mass_a/F");    
	_outputEvt->Branch("Mass_p", &_Mass_p,"Mass_p/F");    
	_outputEvt->Branch("TotChPFOCluEn_a", &_TotChPFOEn_a,"TotChPFOCluEn_a/F");    
	_outputEvt->Branch("TotChPFOCluEn_p", &_TotChPFOEn_p,"TotChPFOCluEn_p/F");    
	_outputEvt->Branch("TotNePFOCluEn_a", &_TotNePFOEn_a,"TotNePFOCluEn_a/F");    
	_outputEvt->Branch("TotNePFOCluEn_p", &_TotNePFOEn_p,"TotNePFOCluEn_p/F");
        _outputEvt->Branch("LPFOEn_a", &_leadingPFOEn,"LPFOEn_a/F");
	_outputEvt->Branch("LChPFOEn_a", &_leadingChPFOEn,"LChPFOEn_a/F");
	_outputEvt->Branch("LNePFOEn_a", &_leadingNePFOEn,"LNePFOEn_a/F");
	_outputEvt->Branch("LPosCh_a", _LChPFOPos,"LPosCh_a[3]/F");
	_outputEvt->Branch("LPosNe_a", _LNePFOPos,"LPosNe_a[3]/F");
        _outputEvt->Branch("LPos_a", _LPFOPos,"LPos_a/F");
	_outputEvt->Branch("LThetaCh_a", &_LChPFOTheta,"LThetaCh_a/F");
	_outputEvt->Branch("LThetaNe_a", &_LNePFOTheta,"LThetaNe_a/F");
	_outputEvt->Branch("LPhiCh_a", &_LChPFOPhi,"LPhiCh_a/F");
	_outputEvt->Branch("LPhiNe_a", &_LNePFOPhi,"LPhiNe_a/F");

	_outputMCP = new TTree("MCP", "MCP" );
	_outputMCP->SetAutoSave(32*1024*1024);
	_outputMCP->Branch("EventNr", &_eventNr,"EventNr/I");
	_outputMCP->Branch("Num", &_Num,"Num/I");    
	_outputMCP->Branch("File", &_filenum,"File/I"); 
	_outputMCP->Branch("Cat", &_MCPCat,"Cat/I"); 
	_outputMCP->Branch("CatB2", &_MCPCatB2,"CatB2/I");
	_outputMCP->Branch("Theta", &_MCPTheta, "Theta/F");
	_outputMCP->Branch("Phi", &_MCPPhi, "Phi/F");   
	_outputMCP->Branch("P4", _MCPP,"P4[4]/F"); 
	_outputMCP->Branch("Vtx", _MCPVtx,"Vtx[3]/F");
	_outputMCP->Branch("EndP", _MCPEndP,"EndP[3]/F");
	_outputMCP->Branch("PID", &_PIDMCP,"PID/I");
	_outputMCP->Branch("Charge", &_ChargeMCP,"Charge/I");
	_outputMCP->Branch("ND", &_nD,"ND/I");
	_outputMCP->Branch("NP", &_nP,"NP/I");

	_outputTrk = new TTree("Trk", "Trk");
	_outputTrk->SetAutoSave(32*1024*1024);
	_outputTrk->Branch("EventNr", &_eventNr,"EventNr/I");
	_outputTrk->Branch("Num", &_Num,"Num/I");  
	_outputTrk->Branch("File", &_filenum,"File/I"); 
	_outputTrk->Branch("MCPTheta", &_MCPOTheta, "MCPTheta/F");
	_outputTrk->Branch("MCPPhi", &_MCPOPhi, "MCPPhi/F");  
	_outputTrk->Branch("MCPEn", &_MCPOEn, "MCPEn/F");    
	_outputTrk->Branch("P3", _TrkP,"P3[3]/F"); 
	_outputTrk->Branch("SP", _TstartP,"SP[3]/F");
	_outputTrk->Branch("EP", _TendP,"EP[3]/F");
	_outputTrk->Branch("NHit", &_NTrkHit,"NHit/I");
	_outputTrk->Branch("Theta", &_TTheta,"Theta/F");
	_outputTrk->Branch("Phi", &_TPhi,"Phi/F");
	_outputTrk->Branch("D0", &_D0,"D0/F");
	_outputTrk->Branch("Z0", &_Z0,"Z0/F");
	_outputTrk->Branch("Omega", &_Omega,"Omega/F");
	_outputTrk->Branch("TLambda", &_TanLambda,"TLambda/F");
	_outputTrk->Branch("Type", &_TrackType,"Type/I");	

	_outputHit = new TTree("Hit", "Hit");
	_outputHit->SetAutoSave(32*1024*1024);
	_outputHit->Branch("EventNr", &_eventNr,"EventNr/I");
	_outputHit->Branch("Num", &_Num,"Num/I");  
	_outputHit->Branch("File", &_filenum,"File/I");
	_outputHit->Branch("MCPTheta", &_MCPOTheta, "MCPTheta/F");
	_outputHit->Branch("MCPPhi", &_MCPOPhi, "MCPPhi/F");  
	_outputHit->Branch("MCPEn", &_MCPOEn, "MCPEn/F");    
	_outputHit->Branch("Pos", _HPos,"Pos[3]/F"); 
	_outputHit->Branch("En", &_HitEn,"En/F");  
	_outputHit->Branch("Time", &_HitTime,"Time/F"); 
	_outputHit->Branch("Cat", &_HitCat,"Cat/I"); 
	_outputHit->Branch("NLayer", &_HLayer,"NLayer/I");  
	_outputHit->Branch("NNb", &_NNb,"NNb/I");      

	_outputClu = new TTree("Clu", "Clu");
	_outputClu->SetAutoSave(32*1024*1024);
	_outputClu->Branch("EventNr", &_eventNr,"EventNr/I");
	_outputClu->Branch("Num", &_Num,"Num/I");  
	_outputClu->Branch("File", &_filenum,"File/I");
	_outputClu->Branch("MCPTheta", &_MCPOTheta, "MCPTheta/F");
	_outputClu->Branch("MCPPhi", &_MCPOPhi, "MCPPhi/F");  
	_outputClu->Branch("MCPEn", &_MCPOEn, "MCPEn/F");
	_outputClu->Branch("Pos", _CPos,"Pos[3]/F"); 
	_outputClu->Branch("En", &_CluEn,"En/F");   
	_outputClu->Branch("Depth", &_CluDepth,"Depth/F");  
	_outputClu->Branch("Type", &_CluType,"Type/I");
	_outputClu->Branch("NH", &_NCalHit,"NH/I");
	_outputClu->Branch("DisTLC", &_DisTLC,"DisTLC/F");  

	_outputPFO = new TTree("PFO", "PFO");
	_outputPFO->SetAutoSave(32*1024*1024);
	_outputPFO->Branch("EventNr", &_eventNr,"EventNr/I");
	_outputPFO->Branch("Num", &_Num,"Num/I");  
	_outputPFO->Branch("File", &_filenum,"File/I");
	_outputPFO->Branch("MCPTheta", &_MCPOTheta, "MCPTheta/F");
	_outputPFO->Branch("MCPPhi", &_MCPOPhi, "MCPPhi/F");  
	_outputPFO->Branch("Theta", &_PFOTheta, "Theta/F");
	_outputPFO->Branch("Phi", &_PFOPhi, "Phi/F");  
	_outputPFO->Branch("MCPEn", &_MCPOEn, "MCPEn/F");     
	_outputPFO->Branch("P4", _PPFO_a,"P4[4]/F");      
	_outputPFO->Branch("P4MCTL", _P4MCTL,"P4MCTL[4]/F");     
	_outputPFO->Branch("Type", &_Type_a,"Type/I");    
	_outputPFO->Branch("MCTruthType", &_MCTType_a,"MCTruthType/I");
	_outputPFO->Branch("Charge", &_Charge_a,"Charge/I");    
	_outputPFO->Branch("CluEn", &_PFOCluEn_a,"CluEn/F");  
	_outputPFO->Branch("TrkD0", &_trkZ0, "TrkD0/F");  
	_outputPFO->Branch("TrkZ0", &_trkD0, "TrkZ0/F");  
	_outputPFO->Branch("TrkSP", _trkSP, "TrkSP[3]/F");  
	_outputPFO->Branch("TrkEP", _trkEP, "TrkEP[3]/F");  
	_outputPFO->Branch("TrkNH", &_NHTrk, "TrkNH/I");


	_outputPFOp = new TTree("PFOp", "PFOp");
	_outputPFOp->SetAutoSave(32*1024*1024);
	_outputPFOp->Branch("EventNr", &_eventNr,"EventNr/I");
	_outputPFOp->Branch("Num", &_Num,"Num/I");  
	_outputPFOp->Branch("File", &_filenum,"File/I");
	_outputPFOp->Branch("MCPTheta", &_MCPOTheta, "MCPTheta/F");
	_outputPFOp->Branch("MCPPhi", &_MCPOPhi, "MCPPhi/F");  
	_outputPFOp->Branch("MCPEn", &_MCPOEn, "MCPEn/F");    
	_outputPFOp->Branch("P4", _PPFO_p,"P4[4]/F");   
	_outputPFOp->Branch("Type", &_Type_p,"Type/I");   
	_outputPFOp->Branch("Charge", &_Charge_p,"Charge/I");    
	_outputPFOp->Branch("CluEn", &_PFOCluEn_p,"CluEn/F");

	_outputIsoH = new TTree("IsoHitClu", "IsoHitClu");
	_outputIsoH->SetAutoSave(32*1024*1024);
	_outputIsoH->Branch("EventNr", &_eventNr,"EventNr/I");
	_outputIsoH->Branch("Num", &_Num,"Num/I");  
	_outputIsoH->Branch("File", &_filenum,"File/I");
	_outputIsoH->Branch("MCPTheta", &_MCPOTheta, "MCPTheta/F");
	_outputIsoH->Branch("MCPPhi", &_MCPOPhi, "MCPPhi/F");  
	_outputIsoH->Branch("MCPEn", &_MCPOEn, "MCPEn/F");    
	_outputIsoH->Branch("Pos", _IsoPos,"Pos[3]/F");   
	_outputIsoH->Branch("Type", &_PFOType,"Type/I");    
	_outputIsoH->Branch("CluEn", &_CEnIso,"CluEn/F");    
	_outputIsoH->Branch("CluDep", &_CDepthIso,"CluDep/F");    
	_outputIsoH->Branch("Dis", &_DisIHC,"Dis/F");    
	_outputIsoH->Branch("IsoHEn", &_IsoHEn,"IsoHEn/F");   

	_Num = 0;	
       
}

void AnaSinglePart::processEvent( LCEvent * evtP ) 
{		

	if (evtP) 								
	{
		_eventNr=evtP->getEventNumber();	
                cout << "Event:" << _eventNr << endl;
		const string ECALCellIDDecoder = "M:3,S-1:3,I:9,J:9,K-1:6";
        	CellIDDecoder<CalorimeterHit> idDecoder(ECALCellIDDecoder);

		_nMCP = 0;
		_nSelMCP12B2 = 0;
		_nSelMCP12B2HE = 0;
		_nSelMCP12B2LE = 0;
		_nSelMCP12B2ME = 0;
		_nSelMCP12 = 0;
		_nSelMCP21 = 0;
		_nSelMCP11 = 0;
		_nSelMCP22 = 0;
		_MCPOTheta = 0;
		_MCPOPhi = 0;
		_MCPOEn = 0;
		_MCPOID = 0;

		_nSelMCP12B2CH = 0;
		_nSelMCP12B2NE = 0;
		_nSelMCP12B2Mu = 0;
		_nSelMCP12B2Ele = 0;
		_nSelMCP12B2Pi = 0;
		_nSelMCP12B2Gam = 0;
		_nSelMCP12B2Neu = 0;

		_nSelMCP12B2CHLE = 0;
		_nSelMCP12B2NELE = 0;
		_nSelMCP12B2MuLE = 0;
		_nSelMCP12B2EleLE = 0;
		_nSelMCP12B2PiLE = 0;
		_nSelMCP12B2GamLE = 0;
		_nSelMCP12B2NeuLE = 0;

		_nSelMCP12B2CHME = 0;
		_nSelMCP12B2NEME = 0;
		_nSelMCP12B2MuME = 0;
		_nSelMCP12B2EleME = 0;
		_nSelMCP12B2PiME = 0;
		_nSelMCP12B2GamME = 0;
		_nSelMCP12B2NeuME = 0;

		_nSelMCP12B2CHHE = 0;
		_nSelMCP12B2NEHE = 0;
		_nSelMCP12B2MuHE = 0;
		_nSelMCP12B2EleHE = 0;
		_nSelMCP12B2PiHE = 0;
		_nSelMCP12B2GamHE = 0;
		_nSelMCP12B2NeuHE = 0;

		_nTrk = 0;
		_nTrkHQ = 0;

		_nHit = 0;
		_THEn = 0;

		_nClu = 0;
		_TCEn = 0;
		_LCEn = 0;
				
		_minCluDepth = 1.0e6;


		TLorentzVector ArborTotalP(0, 0, 0, 0);
		TLorentzVector PandoraTotalP(0, 0, 0, 0);

		_nPFO_a = 0;
		_nCHPFOs_a = 0;
		_nNEPFOs_a = 0;
		_nCMIPPFOs_a = 0;
		_nCEMPFOs_a = 0;
		_nCHADPFOs_a = 0;
		_nNEMPFOs_a = 0;
		_nNHADPFOs_a = 0;

		_nPFO_p = 0;
		_nCHPFOs_p = 0;
		_nNEPFOs_p = 0;
		_nCMIPPFOs_p = 0;
		_nCEMPFOs_p = 0;
		_nCHADPFOs_p = 0;
		_nNEMPFOs_p = 0;
		_nNHADPFOs_p = 0;

		_nPFO_aLE = 0;
		_nCHPFOs_aLE = 0;
		_nNEPFOs_aLE = 0;
		_nCMIPPFOs_aLE = 0;
		_nCEMPFOs_aLE = 0;
		_nCHADPFOs_aLE = 0;
		_nNEMPFOs_aLE = 0;
		_nNHADPFOs_aLE = 0;

		_nPFO_pLE = 0;
		_nCHPFOs_pLE = 0;
		_nNEPFOs_pLE = 0;
		_nCMIPPFOs_pLE = 0;
		_nCEMPFOs_pLE = 0;
		_nCHADPFOs_pLE = 0;
		_nNEMPFOs_pLE = 0;
		_nNHADPFOs_pLE = 0;

		_nPFO_aME = 0;
		_nCHPFOs_aME = 0;
		_nNEPFOs_aME = 0;
		_nCMIPPFOs_aME = 0;
		_nCEMPFOs_aME = 0;
		_nCHADPFOs_aME = 0;
		_nNEMPFOs_aME = 0;
		_nNHADPFOs_aME = 0;

		_nPFO_pME = 0;
		_nCHPFOs_pME = 0;
		_nNEPFOs_pME = 0;
		_nCMIPPFOs_pME = 0;
		_nCEMPFOs_pME = 0;
		_nCHADPFOs_pME = 0;
		_nNEMPFOs_pME = 0;
		_nNHADPFOs_pME = 0;

		_nPFO_aHE = 0;
		_nCHPFOs_aHE = 0;
		_nNEPFOs_aHE = 0;
		_nCMIPPFOs_aHE = 0;
		_nCEMPFOs_aHE = 0;
		_nCHADPFOs_aHE = 0;
		_nNEMPFOs_aHE = 0;
		_nNHADPFOs_aHE = 0;

		_nPFO_pHE = 0;
		_nCHPFOs_pHE = 0;
		_nNEPFOs_pHE = 0;
		_nCMIPPFOs_pHE = 0;
		_nCEMPFOs_pHE = 0;
		_nCHADPFOs_pHE = 0;
		_nNEMPFOs_pHE = 0;
		_nNHADPFOs_pHE = 0;

		_TotalRecoP4_a[0] = 0;
		_TotalRecoP4_a[1] = 0;
		_TotalRecoP4_a[2] = 0;
		_TotalRecoP4_a[3] = 0;
		_TotalRecoP4_p[0] = 0;
		_TotalRecoP4_p[1] = 0;
		_TotalRecoP4_p[2] = 0;
		_TotalRecoP4_p[3] = 0;
		_Mass_a = 0;
		_Mass_p = 0; 
		
		_leadingPFOEn = 0;
	        _leadingChPFOEn = 0;
		_leadingNePFOEn = 0;
        	_TotChPFOEn_a = 0;
		_TotChPFOEn_p = 0;

		_TotNePFOEn_a = 0;
		_TotNePFOEn_p = 0;
		
		_LPFOPos[0] = 0;
		_LPFOPos[1] = 0;
		_LPFOPos[2] = 0;
		
		_LChPFOPos[0] = 0;
                _LChPFOPos[1] = 0;
                _LChPFOPos[2] = 0;

		_LNePFOPos[0] = 0;
                _LNePFOPos[1] = 0;
                _LNePFOPos[2] = 0;	
		
		_LNePFOTheta = 0;
		_LNePFOPhi = 0;		
		
		_LChPFOTheta = 0;
		_LChPFOPhi = 0;	

		std::vector<MCParticle *> NeMCPB2;
		NeMCPB2.clear();

		std::vector<MCParticle *> HDLepton;
		std::vector<MCParticle *> HDTau;
		std::vector<MCParticle *> HDBQuark;
		std::vector<MCParticle *> HDCQuark;
		std::vector<MCParticle *> HDGluon;
		std::vector<MCParticle *> HDGamma;
		std::vector<MCParticle *> HDZ;
		std::vector<MCParticle *> HDW;
		HDLepton.clear();
		HDTau.clear();
		HDBQuark.clear();
		HDCQuark.clear();
		HDGluon.clear();
		HDGamma.clear();
		HDZ.clear();
		HDW.clear();
		
		if(_flag/10000-_flag/100000*10 == 1)
		{
			try 	
			{  
                   
	            		LCCollection * MCP = evtP->getCollection("MCParticle");
		    		int NMC= MCP->getNumberOfElements();
				TVector3 VtxPos, EndPPos;
			
				for(int ii = 0; ii < 4; ii++)
				{
					
					_MCPP[ii] = 0;
					
					if(ii < 3)
					{
						_MCPVtx[ii] = 0;
						_MCPEndP[ii] = 0;
					}					
				}
				_nD = 0;
				_nP = 0;
				_ChargeMCP = 0;
				_PIDMCP = 0;
				_MCPCat = 0;
				
			
                    		for(int i0 = 0; i0 < NMC; i0++)
		    		{
					_nMCP++;
					MCParticle * a_MCP = dynamic_cast<MCParticle*>(MCP->getElementAt(i0));

					_nD=a_MCP->getDaughters().size();
					_nP=a_MCP->getParents().size();
					VtxPos = a_MCP->getVertex();
					EndPPos = a_MCP->getEndpoint();
					_PIDMCP=a_MCP->getPDG();
					_ChargeMCP = a_MCP->getCharge();

					TVector3 MCPP(a_MCP->getMomentum()[0],a_MCP->getMomentum()[1],a_MCP->getMomentum()[2]);
					_MCPP[0] = a_MCP->getMomentum()[0];
					_MCPP[1] = a_MCP->getMomentum()[1];
					_MCPP[2] = a_MCP->getMomentum()[2];
					_MCPP[3] = a_MCP->getEnergy();

					_MCPVtx[0] = VtxPos.X();
					_MCPVtx[1] = VtxPos.Y();
					_MCPVtx[2] = VtxPos.Z();

					_MCPEndP[0] = EndPPos.X();
					_MCPEndP[1] = EndPPos.Y();
					_MCPEndP[2] = EndPPos.Z();
					
					if(((VtxPos.Perp() < 1750 && VtxPos.Perp() > 200 && fabs(VtxPos.Z()) < 2250) || (VtxPos.Perp() < 200 && fabs(VtxPos.Z()) < 1130)) && (EndPPos.Perp() > 1750 || fabs(EndPPos.Z()) > 2250 || (EndPPos.Perp() < 200 && fabs(EndPPos.Z()) > 1130)) && fabs(_PIDMCP) != 12 &&  fabs(_PIDMCP) != 14 &&  fabs(_PIDMCP) != 16 && (VtxPos-EndPPos).Mag() > 500 )
					{
						_nSelMCP12B2++;
						_MCPCatB2 = 12;
						if(_MCPP[3]<1) _nSelMCP12B2LE++;
						else if(_MCPP[3]<5) _nSelMCP12B2ME++;
						else _nSelMCP12B2HE++;
						if(_ChargeMCP == 0)
						{
							NeMCPB2.push_back(a_MCP);
							_nSelMCP12B2NE++;
							if(_MCPP[3]<1) _nSelMCP12B2NELE++;
							else if(_MCPP[3]<5) _nSelMCP12B2NEME++;
							else _nSelMCP12B2NEHE++;
							if(_PIDMCP == 22) 
							{
								_nSelMCP12B2Gam++;
								if(_MCPP[3]<1) _nSelMCP12B2GamLE++;
								else if(_MCPP[3]<5) _nSelMCP12B2GamME++;
								else _nSelMCP12B2GamHE++;
							}
							else
							{
								_nSelMCP12B2Neu++;
								if(_MCPP[3]<1) _nSelMCP12B2NeuLE++;
								else if(_MCPP[3]<5) _nSelMCP12B2NeuME++;
								else _nSelMCP12B2NeuHE++;
							}
						}
						else
						{
							_nSelMCP12B2CH++;
							if(_MCPP[3]<1) _nSelMCP12B2CHLE++;
							else if(_MCPP[3]<5) _nSelMCP12B2CHME++;
							else _nSelMCP12B2CHHE++;
							if(abs(_PIDMCP) == 13) 
							{
								_nSelMCP12B2Mu++;
								if(_MCPP[3]<1) _nSelMCP12B2MuLE++;
								else if(_MCPP[3]<5) _nSelMCP12B2MuME++;
								else _nSelMCP12B2MuHE++;
							}
							else if (abs(_PIDMCP) == 11)
							{
								_nSelMCP12B2Ele++;
								if(_MCPP[3]<1) _nSelMCP12B2EleLE++;
								else if(_MCPP[3]<5) _nSelMCP12B2EleME++;
								else _nSelMCP12B2EleHE++;
							}
							else
							{
								_nSelMCP12B2Pi++;
								if(_MCPP[3]<1) _nSelMCP12B2PiLE++;
								else if(_MCPP[3]<5) _nSelMCP12B2PiME++;
								else _nSelMCP12B2PiHE++;
							}
						}
					}
					else
					{
						_MCPCatB2 = 0;
					}

					if( VtxPos.Perp() < 1830 && fabs(VtxPos.Z()) < 2400 && (EndPPos.Perp() > 1830 || fabs(EndPPos.Z()) > 2400 ) && fabs(_PIDMCP) != 12 &&  fabs(_PIDMCP) != 14 &&  fabs(_PIDMCP) != 16 )
					{
						_nSelMCP12++;
						_MCPCat = 12;
					}	
					else if( EndPPos.Perp() < 1830 && fabs(EndPPos.Z()) < 2400 && (VtxPos.Perp() > 1830 || fabs(VtxPos.Z()) > 2400 ) && fabs(_PIDMCP) != 12 &&  fabs(_PIDMCP) != 14 &&  fabs(_PIDMCP) != 16 )
                                	{
						_nSelMCP21++;
						_MCPCat = 21;
					}
					else if( (EndPPos.Perp() > 1830 || fabs(EndPPos.Z()) > 2400 ) && (VtxPos.Perp() > 1830 || fabs(VtxPos.Z()) > 2400 ) && fabs(_PIDMCP) != 12 &&  fabs(_PIDMCP) != 14 &&  fabs(_PIDMCP) != 16 )
                                	{
                                        	_nSelMCP22++;
						_MCPCat = 22;
                                	}
					else if( EndPPos.Perp() < 1830 && fabs(EndPPos.Z()) < 2400 && VtxPos.Perp() < 1830 && fabs(VtxPos.Z()) < 2400 && fabs(_PIDMCP) != 12 &&  fabs(_PIDMCP) != 14 &&  fabs(_PIDMCP) != 16 )
                                	{
                                        	_nSelMCP11++;
						_MCPCat = 11;
                                	}
					else 
					{
						_MCPCat = 1;
					}
					if(i0 == 0)
					{
						_MCPOTheta = MCPP.Theta();
						_MCPOPhi = MCPP.Phi();
						_MCPOEn = a_MCP->getEnergy();
						_MCPOID = _PIDMCP;
					}
				
					_MCPTheta = MCPP.Theta();
					_MCPPhi = MCPP.Phi();

					if(_PIDMCP == 25 && _nD > 1 && _nP !=0 ) //Higgs
					{
					
						for(int i1 = 0; i1 < _nD; i1++)  
						{
							MCParticle *b_MCP = a_MCP->getDaughters()[i1];
							float HdaughterPID = b_MCP->getPDG();

							if(abs(HdaughterPID) == 11 || abs(HdaughterPID) == 13)
							{
								HDLepton.push_back(b_MCP);
							}
							else if(abs(HdaughterPID) == 15)
							{
                                                       		HDTau.push_back(b_MCP);
                                                	}
							else if(abs(HdaughterPID) == 22)
							{
								HDGamma.push_back(b_MCP);
							}
							else if(abs(HdaughterPID) == 4)
							{
								HDCQuark.push_back(b_MCP);
							}
							else if(abs(HdaughterPID) == 5)
							{
								HDBQuark.push_back(b_MCP);
							}
							else if(abs(HdaughterPID) == 21)
							{
								HDGluon.push_back(b_MCP);
							}
							else if(abs(HdaughterPID) == 23)
							{
								HDZ.push_back(b_MCP);
							}
							else if(abs(HdaughterPID) == 24)
							{
								HDW.push_back(b_MCP);
							}					

						}
					}

					_outputMCP->Fill();
		    		}

                	}catch (lcio::DataNotAvailableException err) { };

		}
		int nHDLepton = HDLepton.size();
		int nHDTau = HDTau.size();
		int nHDBQuark = HDBQuark.size();
		int nHDCQuark = HDCQuark.size();
		int nHDGluon = HDGluon.size();
		int nHDGamma = HDGamma.size();
		int nHDW = HDW.size();
		int nHDZ = HDZ.size();
		
		_HDCat = 0;

		if(nHDLepton > 1) _HDCat = 1313;
		if(nHDTau > 1) _HDCat = 1515;
		if(nHDBQuark > 1) _HDCat = 505;
		if(nHDCQuark > 1) _HDCat = 404;
		if(nHDGluon > 1) _HDCat = 2121;
		if(nHDGamma > 1) _HDCat = 2222;
		if(nHDZ > 1) _HDCat = 2323;
		if(nHDW > 1) _HDCat = 2424;
		if(nHDZ == 1 && nHDGamma > 1) _HDCat = 2223;
		
		
		

		if(_flag/1000-_flag/10000*10 == 1)
		{
			try 	
			{  
                   
	            		LCCollection * Trk = evtP->getCollection( "MarlinTrkTracks" );
				int NTrk = Trk->getNumberOfElements();
				TVector3 EndPointPos, StartPointPos;
				
				for(int ii = 0; ii < 3; ii++)
				{
					_TstartP[ii] = 0;
					_TendP[ii] = 0;
					_TrkP[ii] = 0;
				}
				_NTrkHit = 0;
				_TPhi = 0;
				_TTheta = 0;
				_D0 = 0;
				_Z0 = 0;
				_Omega = 0;
				_TanLambda = 0;
				_TrackType = 0;

				for(int t0 =0;t0 < NTrk; t0++)
				{
					_nTrk++;
					Track* a_Trk = dynamic_cast<Track*>( Trk->getElementAt( t0 ) );
					_NTrkHit = a_Trk->getTrackerHits().size();		
					EndPointPos = (a_Trk->getTrackerHits()[_NTrkHit - 1])->getPosition();	
					StartPointPos = (a_Trk->getTrackerHits()[0])->getPosition();
					
					_TstartP[0] = StartPointPos.X();
					_TstartP[1] = StartPointPos.Y();
					_TstartP[2] = StartPointPos.Z();

					_TendP[0] = EndPointPos.X();
					_TendP[1] = EndPointPos.Y();
					_TendP[2] = EndPointPos.Z();
						
					_TPhi = a_Trk->getPhi();
					_D0 = a_Trk -> getD0();
					_Z0 = a_Trk -> getZ0();
					_Omega = a_Trk -> getOmega();
					_TanLambda = a_Trk->getTanLambda();
					HelixClass * TrkInit_Helix = new HelixClass();
					TrkInit_Helix->Initialize_Canonical(a_Trk->getPhi(), a_Trk -> getD0(), a_Trk -> getZ0(), a_Trk -> getOmega(), a_Trk->getTanLambda(), 3.5);

					TVector3 TrkMom(TrkInit_Helix->getMomentum()[0],TrkInit_Helix->getMomentum()[1],TrkInit_Helix->getMomentum()[2]);
					_TrkP[0] = TrkMom.X();
					_TrkP[1] = TrkMom.Y();
					_TrkP[2] = TrkMom.Z();
	
					_TTheta = TrkMom.Theta();
		
					delete TrkInit_Helix;					
					if( _NTrkHit > 9 || (fabs(EndPointPos.Z()) > LStar - 500 && EndPointPos.Perp() < TPCInnerRadius ) || fabs(EndPointPos.Z()) > ECALHalfZ - 200  )		// Min requirement for track quality
					{
						if( fabs(EndPointPos.Z()) > ECALHalfZ - 500 && EndPointPos.Perp() > TPCOuterRadius - 300  )	//Shoulder
						{
							_TrackType = 30;
						}
						else if( fabs(EndPointPos.Z()) > LStar - 500 && EndPointPos.Perp() < TPCInnerRadius )		//Forward
						{
							_TrackType = 40;
						}
						else if( EndPointPos.Perp() > TPCOuterRadius - 100 )		//Barrel
						{	
							_TrackType = 10;
						}
						else if( fabs(EndPointPos.Z()) > ECALHalfZ - 200 )		//Endcap
						{
							_TrackType = 20; 
						}

						if( fabs(_D0) < 1 && fabs(_Z0) < 1 )
						{
							_TrackType += 1;
							_nTrkHQ +=1;
						}
					
					}		
					_outputTrk->Fill();
				}

                	}catch (lcio::DataNotAvailableException err) { };

		}

		if(_flag/100-_flag/1000*10 == 1)
		{

			std::vector<std::string> CaloHitCollections;
			CaloHitCollections.clear();
			CaloHitCollections.push_back("ECALPSHitCollection");
			CaloHitCollections.push_back("ECALBarrel");
			CaloHitCollections.push_back("ECALEndcap");
			CaloHitCollections.push_back("ECALOther");
			CaloHitCollections.push_back("HCALBarrel");
			CaloHitCollections.push_back("HCALEndcap");
			CaloHitCollections.push_back("HCALOther");
			CaloHitCollections.push_back("LCAL");
			CaloHitCollections.push_back("LHCAL");
			
			int nCol = CaloHitCollections.size();
			
			_HitCat = -1;
			_HitTime = 0;
			_HitEn = 0;
			_HPos[0] = 0;
			_HPos[1] = 0;
			_HPos[2] = 0;
			_HLayer = -1;
			std::map<CalorimeterHit*, int> HitNNb;
			HitNNb.clear();
			for(int c0 = 1;c0 < 4;c0++)
			{
				try
                		{
					LCCollection * CaloHitColl = evtP ->getCollection(CaloHitCollections[c0].c_str());
					int NHitsCurrCol = CaloHitColl->getNumberOfElements();
					for(int h01 = 0; h01 < NHitsCurrCol; h01++)
                                	{
						CalorimeterHit * a_hit = dynamic_cast<CalorimeterHit*>(CaloHitColl->getElementAt(h01));
						TVector3 tmpPos1 = a_hit->getPosition();
						int L1 = idDecoder(a_hit)["K-1"];
						int tmpNNb = 0;
						for(int h02 = 0; h02 < NHitsCurrCol; h02++)
						{
							if(h02 != h01)
							{
								CalorimeterHit * b_hit = dynamic_cast<CalorimeterHit*>(CaloHitColl->getElementAt(h02));
								TVector3 tmpPos2 = b_hit->getPosition();
								int L2 = idDecoder(b_hit)["K-1"];
								if((tmpPos1-tmpPos2).Mag() < 5.1 && L1 == L2)
								{
									tmpNNb++;
								}
							}
						}
						HitNNb[a_hit] = tmpNNb;	
					}
				}catch (lcio::DataNotAvailableException err) { };
			}
			for(int c1 = 0;c1 < nCol; c1++)
			{
				try
                		{
					LCCollection * CaloHitColl = evtP ->getCollection(CaloHitCollections[c1].c_str());
					int NHitsCurrCol = CaloHitColl->getNumberOfElements();
					_nHit += NHitsCurrCol;
					TVector3 HitPos;

					for(int h0 = 0; h0 < NHitsCurrCol; h0++)
                                	{
						CalorimeterHit * a_hit = dynamic_cast<CalorimeterHit*>(CaloHitColl->getElementAt(h0));
						_HitCat = c1;
						_HitEn = a_hit->getEnergy();
						_THEn += _HitEn;
						HitPos = a_hit->getPosition();
						_NNb = -1;

						_HLayer = idDecoder(a_hit)["K-1"];						

						_HPos[0] = HitPos.X();
						_HPos[1] = HitPos.Y();
						_HPos[2] = HitPos.Z();
			
						// this time is always zero ?
						_HitTime = a_hit->getTime();
						if(HitNNb.find(a_hit) != HitNNb.end())
						{
							_NNb = HitNNb[a_hit];
						}

						_outputHit->Fill();
					}				

                		}catch (lcio::DataNotAvailableException err) { };
			}

		}

		if(_flag/10-_flag/100*10 == 1)
		{
			try 	
			{  
             
	           		LCCollection *CluColl = evtP ->getCollection("EHBushes");
				int NCluCurrCol = CluColl->getNumberOfElements();
				_nClu = NCluCurrCol;
				float LCEn = 0;
				float TCEn = 0;
				int LCFlag = -1; 					

				for(int ci = 0; ci < NCluCurrCol; ci++)
                                {
					Cluster * aa_clu = dynamic_cast<Cluster*>(CluColl->getElementAt(ci));
					float aa_CluE = aa_clu->getEnergy();
					if (aa_CluE > LCEn)
					{
						LCEn = aa_CluE;
						LCFlag = ci;
					}
					TCEn += aa_CluE;
				}
				_LCEn = LCEn;
				_TCEn = TCEn;
				
				_CluEn = 0;
				_NCalHit = 0;
				_CluType = 0;
				_CPos[0] = 0; 
				_CPos[1] = 0; 
				_CPos[2] = 0; 
				_CluDepth = 0;
				_DisTLC = -1;
		
				for(int c0 = 0; c0 < NCluCurrCol; c0++)
                                {
					Cluster * a_clu = dynamic_cast<Cluster*>(CluColl->getElementAt(c0));
					Cluster * l_clu = dynamic_cast<Cluster*>(CluColl->getElementAt(LCFlag));
					_CluEn = a_clu->getEnergy();
					TVector3 CluPos = a_clu->getPosition();
					_CluDepth = DisSeedSurface(CluPos);
					if(c0 != LCFlag)
					{
						_DisTLC = BushDis(a_clu,l_clu);
					}
							
					if(_CluDepth < _minCluDepth) _minCluDepth = _CluDepth;

					_CPos[0] = CluPos.X();
					_CPos[1] = CluPos.Y();
					_CPos[2] = CluPos.Z();
					_CluType = ClusterFlag1st(a_clu);
					_NCalHit = a_clu->getCalorimeterHits().size();
					for(int h2 = 0;h2 < _NCalHit;h2++)
					{
						CalorimeterHit * a_hit = a_clu->getCalorimeterHits()[h2];
						if(fabs(a_hit->getEnergy() - DHCALCalibrationConstant) < 1.0E-6)
						{
							_HitCat = 11;
						}
						else
						{
							_HitCat = 10;
						}
						_HitEn = a_hit->getEnergy();
						TVector3 HitPos = a_hit->getPosition();
						_NNb = -1;

						_HLayer = idDecoder(a_hit)["K-1"];						

						_HPos[0] = HitPos.X();
						_HPos[1] = HitPos.Y();
						_HPos[2] = HitPos.Z();
			
						_HitTime = a_hit->getTime();
						_outputHit->Fill();
					}

					_outputClu->Fill();
				}				


                	}catch (lcio::DataNotAvailableException err) { };
		}


		if(_flag-_flag/10*10 == 1)
		{
			try 	
			{  
                   
	            		LCCollection *PFO = evtP->getCollection( "ArborPFOs" );
				int NPFO = PFO->getNumberOfElements();
			
				_Charge_a = 0;
				_Type_a = 0;
				_PPFO_a[3] = 0;
				_PPFO_a[0] = 0;
				_PPFO_a[1] = 0;
				_PPFO_a[2] = 0;
				
				std::vector<ReconstructedParticle *> NePFO;
				NePFO.clear();

				for(int p00 =0;p00 < NPFO; p00++)
				{
					ReconstructedParticle *a_PFO=dynamic_cast<EVENT::ReconstructedParticle *>(PFO->getElementAt(p00)); 
					int charge = a_PFO->getCharge();
					if(charge == 0) NePFO.push_back(a_PFO);
				}

				std::map<ReconstructedParticle*, int> PFOTouchFlag;
				std::map<MCParticle*, int> MCTouchFlag;
				std::map<ReconstructedParticle*, int> PFOMCLink;
				PFOTouchFlag.clear();
				MCTouchFlag.clear();
				PFOMCLink.clear();

				for(int t1 = 0;t1 < 20;t1++)
				{
					std::map<MCParticle*, int> MCNearestPFOIndex;
					std::map<ReconstructedParticle*, int> PFONearestMCIndex;
					MCNearestPFOIndex.clear();
					PFONearestMCIndex.clear();
					int nNeMC = NeMCPB2.size();
					int nNePFO = NePFO.size();
					for(int mc1 = 0;mc1 < nNeMC;mc1++)
					{
						MCParticle *a_MCP = NeMCPB2[mc1];
						if(MCTouchFlag.find(a_MCP) == MCTouchFlag.end())
						{
							TVector3 tmpMCP_P = a_MCP->getMomentum();
							float DeltaR = 1.0e6;
							int tmpIndex = -1;
							for(int pfo1 = 0;pfo1 < nNePFO;pfo1++)
							{
								ReconstructedParticle *a_PFO = NePFO[pfo1];
								if(PFOTouchFlag.find(a_PFO) == PFOTouchFlag.end())
								{
									TVector3 tmpPFO_P = a_PFO->getMomentum();
									float tmpAng = tmpMCP_P.Angle(tmpPFO_P);
									if(tmpAng < DeltaR)
									{
										DeltaR = tmpAng;
										tmpIndex = pfo1;
									}
								}								
							}
							if(tmpIndex > -1 && DeltaR < 0.1)
							{
								MCNearestPFOIndex[a_MCP] = tmpIndex;
							}
						}
					}
					for(int pfo2 = 0;pfo2 < nNePFO;pfo2++)
					{
						ReconstructedParticle *a_PFO = NePFO[pfo2];
						if(PFOTouchFlag.find(a_PFO) == PFOTouchFlag.end())
						{
							TVector3 tmpPFO_P = a_PFO->getMomentum();
							float DeltaR = 1.0e6;
							int tmpIndex = -1;
							for(int mc2 = 0;mc2 < nNeMC;mc2++)
							{
								MCParticle *a_MCP = NeMCPB2[mc2];
								if(MCTouchFlag.find(a_MCP) == MCTouchFlag.end())
								{
									TVector3 tmpMCP_P = a_MCP->getMomentum();
									float tmpAng = tmpMCP_P.Angle(tmpPFO_P);
									if(tmpAng < DeltaR)
									{
										DeltaR = tmpAng;
										tmpIndex = mc2;
									}
								}
							}
							if(tmpIndex > -1 && DeltaR < 0.1)
							{
								PFONearestMCIndex[a_PFO] = tmpIndex;
								MCParticle *b_MCP = NeMCPB2[tmpIndex];
								if(MCNearestPFOIndex.find(b_MCP) != MCNearestPFOIndex.end() && MCNearestPFOIndex[b_MCP] == pfo2)
								{
									PFOTouchFlag[a_PFO] = 1;
									MCTouchFlag[b_MCP] = 1;
									PFOMCLink[a_PFO] = tmpIndex;
								}
							}
						}
					}

				}
				
				float maxEn = -1.0;
				float maxEnCh = -1.0;
				float maxEnNe = -1.0;				
				for(int p0 =0;p0 < NPFO; p0++)
				{
					_nPFO_a++;
					ReconstructedParticle *a_PFO=dynamic_cast<EVENT::ReconstructedParticle *>(PFO->getElementAt(p0)); 
					_Charge_a = a_PFO->getCharge();
					_Type_a = a_PFO->getType();
					_PFOCluEn_a = 0;
					
					TLorentzVector currP( a_PFO->getMomentum()[0], a_PFO->getMomentum()[1], a_PFO->getMomentum()[2], a_PFO->getEnergy());
					float PFOEn = a_PFO->getEnergy();
					if(PFOEn > maxEn) maxEn = PFOEn;			

					_trkD0 = -1;
					_trkZ0 = -1;
					_trkSP[0] = -1;
					_trkSP[1] = -1;
					_trkSP[2] = -1;
					_trkEP[0] = -1;
					_trkEP[1] = -1;
					_trkEP[2] = -1;
					_NHTrk = -1;
					_MCTType_a = 0;
					
					if(PFOEn<1) _nPFO_aLE++;
					else if(PFOEn<5) _nPFO_aME++;
					else _nPFO_aHE++;

                                	if(_Charge_a == 0)
					{
						if(_Type_a == 22)
						{ 
							_nNEMPFOs_a++;
							if(PFOEn<1) _nNEMPFOs_aLE++;
							else if(PFOEn<5) _nNEMPFOs_aME++;
							else _nNEMPFOs_aHE++;
						}
						else 
						{
							_nNHADPFOs_a++;
							if(PFOEn<1) _nNHADPFOs_aLE++;
							else if(PFOEn<5) _nNHADPFOs_aME++;
							else _nNHADPFOs_aHE++;
						}
					 	_nNEPFOs_a++;
						if(PFOEn<1) _nNEPFOs_aLE++;
						else if(PFOEn<5) _nNEPFOs_aME++;
						else _nNEPFOs_aHE++;
						_PFOCluEn_a = a_PFO->getEnergy();
						_TotNePFOEn_a += _PFOCluEn_a;
						if(PFOEn > maxEnNe)
						{
							maxEnNe = PFOEn;
							_leadingNePFOEn = PFOEn;
							int nClusters = a_PFO->getClusters().size();
							if(nClusters > 0)
							{
								Cluster *a_CluPFO = a_PFO->getClusters()[0];
								_LNePFOPos[0] = a_CluPFO->getPosition()[0];
								_LNePFOPos[1] = a_CluPFO->getPosition()[1];
								_LNePFOPos[2] = a_CluPFO->getPosition()[2];
								_LNePFOTheta = currP.Theta();
								_LNePFOPhi = currP.Phi();
							}

						}
					}
                                	else
					{
                                        	if(fabs(_Type_a) == 11)
						{ 
							_nCEMPFOs_a++;
							if(PFOEn<1) _nCEMPFOs_aLE++;
							else if(PFOEn<5) _nCEMPFOs_aME++;
							else _nCEMPFOs_aHE++;
						}
                                        	else if(fabs(_Type_a) == 13) 
						{
							_nCMIPPFOs_a++;
							if(PFOEn<1) _nCMIPPFOs_aLE++;
							else if(PFOEn<5) _nCMIPPFOs_aME++;
							else _nCMIPPFOs_aHE++;
						}
                                        	else 
						{
							_nCHADPFOs_a++;
							if(PFOEn<1) _nCHADPFOs_aLE++;
							else if(PFOEn<5) _nCHADPFOs_aME++;
							else _nCHADPFOs_aHE++;
						}
                                         	_nCHPFOs_a++;
						if(PFOEn<1) _nCHPFOs_aLE++;
						else if(PFOEn<5) _nCHPFOs_aME++;
						else _nCHPFOs_aHE++;

						int nPClu_a = a_PFO->getClusters().size();
						if(nPClu_a > 0)
						{
							for(int pc = 0; pc < nPClu_a;pc++)
							{
								Cluster* a_pclu = a_PFO->getClusters()[pc];
								_PFOCluEn_a += a_pclu->getEnergy();
							}
						}
						if(a_PFO->getTracks().size() > 0)
						{
							Track* a_trk = a_PFO->getTracks()[0];
							_trkD0 = a_trk->getD0();
							_trkZ0 = a_trk->getZ0();
							_NHTrk = a_trk->getTrackerHits().size();				
							TVector3 TrkEP = (a_trk->getTrackerHits()[_NHTrk - 1])->getPosition();	
							TVector3 TrkSP = (a_trk->getTrackerHits()[0])->getPosition();
					
							_trkSP[0] = TrkSP.X();
							_trkSP[1] = TrkSP.Y();
							_trkSP[2] = TrkSP.Z();

							_trkEP[0] = TrkEP.X();
							_trkEP[1] = TrkEP.Y();
							_trkEP[2] = TrkEP.Z();
							
						}
						_TotChPFOEn_a += _PFOCluEn_a;
						if(PFOEn > maxEnCh)
                                                {
                                                        maxEnCh = PFOEn;
                                                        _leadingChPFOEn = PFOEn;
                                                        int nClusters = a_PFO->getClusters().size();
                                                        if(nClusters > 0)
                                                        {
                                                                Cluster *a_CluPFO = a_PFO->getClusters()[0];
                                                                _LChPFOPos[0] = a_CluPFO->getPosition()[0];
                                                                _LChPFOPos[1] = a_CluPFO->getPosition()[1];
                                                                _LChPFOPos[2] = a_CluPFO->getPosition()[2];
								_LChPFOTheta = currP.Theta();
								_LChPFOPhi = currP.Phi();
                                                        }

                                                }
				
                                	}
					
					_P4MCTL[0] = 0;
					_P4MCTL[1] = 0;
					_P4MCTL[2] = 0;
					_P4MCTL[3] = 0;
					try 	
					{
						LCCollection * MCTL = evtP->getCollection("RecoMCTruthLink");
						
						LCRelationNavigator *navMCTL   = new LCRelationNavigator(MCTL);
                                               	LCObjectVec vecMCTL            = navMCTL->getRelatedToObjects(a_PFO);
                                               	int pdgid=0;
                                               	if (vecMCTL.size() > 0) {
                                                    
                                                        MCParticle* mcp = dynamic_cast<MCParticle *>(vecMCTL[0]);
                                                        pdgid=abs(mcp->getPDG());
							_P4MCTL[0] = mcp->getMomentum()[0];
							_P4MCTL[1] = mcp->getMomentum()[1];
							_P4MCTL[2] = mcp->getMomentum()[2];
							_P4MCTL[3] = mcp->getEnergy();
                                                }
						_MCTType_a = pdgid;					
						
					}catch (lcio::DataNotAvailableException err) { };

					
					
					if(_Charge_a == 0 && PFOMCLink.find(a_PFO) != PFOMCLink.end() && PFOMCLink[a_PFO] > -1)
					{
						
						MCParticle *a_MCP = NeMCPB2[PFOMCLink[a_PFO]];
						
						_MCTType_a = a_MCP->getPDG();
						_P4MCTL[0] = a_MCP->getMomentum()[0];
						_P4MCTL[1] = a_MCP->getMomentum()[1];
						_P4MCTL[2] = a_MCP->getMomentum()[2];
						_P4MCTL[3] = a_MCP->getEnergy();
						
					}
							
                                	
					_PPFO_a[3] = a_PFO->getEnergy();
					_PPFO_a[0] = a_PFO->getMomentum()[0];
					_PPFO_a[1] = a_PFO->getMomentum()[1];
					_PPFO_a[2] = a_PFO->getMomentum()[2];
	
                                	ArborTotalP += currP;
					_TotalRecoP4_a[0] += _PPFO_a[0];
					_TotalRecoP4_a[1] += _PPFO_a[1];
					_TotalRecoP4_a[2] += _PPFO_a[2];
					_TotalRecoP4_a[3] += _PPFO_a[3];		
					_PFOTheta = currP.Theta();
					_PFOPhi = currP.Phi();
					_outputPFO->Fill();
					
				} 

				_leadingPFOEn = maxEn;

                	}catch (lcio::DataNotAvailableException err) { };
			
			try 	
			{  
                   
	            		LCCollection *PFOp = evtP->getCollection( "PandoraPFOs" );
				int NPFOp = PFOp->getNumberOfElements();
				_Charge_p = 0;
				_Type_p = 0;
				_PPFO_p[3] = 0;
				_PPFO_p[0] = 0;
				_PPFO_p[1] = 0;
				_PPFO_p[2] = 0;
				
				for(int p1 =0;p1 < NPFOp; p1++)
				{
					_nPFO_p++;
					ReconstructedParticle *a_PFO=dynamic_cast<EVENT::ReconstructedParticle *>(PFOp->getElementAt(p1)); 
					_Charge_p = a_PFO->getCharge();
					_Type_p = a_PFO->getType();
					float PFOEn = a_PFO->getEnergy();
					if(PFOEn<1) _nPFO_pLE++;
					else if(PFOEn<5) _nPFO_pME++;
					else _nPFO_pHE++;
					
					
					_PFOCluEn_p = 0;
                                	if(_Charge_p == 0)
					{
						if(_Type_p == 22)
						{ 
							_nNEMPFOs_p++;
							if(PFOEn<1) _nNEMPFOs_pLE++;
							else if(PFOEn<5) _nNEMPFOs_pME++;
							else _nNEMPFOs_pHE++;
						}
						else 
						{
							_nNHADPFOs_p++;
							if(PFOEn<1) _nNHADPFOs_pLE++;
							else if(PFOEn<5) _nNHADPFOs_pME++;
							else _nNHADPFOs_pHE++;
						}
					 	_nNEPFOs_p++;
						if(PFOEn<1) _nNEPFOs_pLE++;
						else if(PFOEn<5) _nNEPFOs_pME++;
						else _nNEPFOs_pHE++;
						_PFOCluEn_p = a_PFO->getEnergy();
						_TotNePFOEn_p += _PFOCluEn_p;
					}
                                	else
					{
                                        	if(fabs(_Type_p) == 11)
						{ 
							_nCEMPFOs_p++;
							if(PFOEn<1) _nCEMPFOs_pLE++;
							else if(PFOEn<5) _nCEMPFOs_pME++;
							else _nCEMPFOs_pHE++;
						}
                                        	else if(fabs(_Type_p) == 13) 
						{
							_nCMIPPFOs_p++;
							if(PFOEn<1) _nCMIPPFOs_pLE++;
							else if(PFOEn<5) _nCMIPPFOs_pME++;
							else _nCMIPPFOs_pHE++;
						}
                                        	else 
						{
							_nCHADPFOs_p++;
							if(PFOEn<1) _nCHADPFOs_pLE++;
							else if(PFOEn<5) _nCHADPFOs_pME++;
							else _nCHADPFOs_pHE++;
						}
                                         	_nCHPFOs_p++;
						if(PFOEn<1) _nCHPFOs_pLE++;
						else if(PFOEn<5) _nCHPFOs_pME++;
						else _nCHPFOs_pHE++;
						int nPClu_p = a_PFO->getClusters().size();
						if(nPClu_p > 0)
						{
							for(int pc = 0; pc < nPClu_p;pc++)
							{
								Cluster* a_pclu = a_PFO->getClusters()[pc];
								_PFOCluEn_p += a_pclu->getEnergy();
							}
						}
						_TotChPFOEn_p += _PFOCluEn_p;
                                	}				
                                	TLorentzVector currP( a_PFO->getMomentum()[0], a_PFO->getMomentum()[1], a_PFO->getMomentum()[2], a_PFO->getEnergy());
					_PPFO_p[3] = a_PFO->getEnergy();
					_PPFO_p[0] = a_PFO->getMomentum()[0];
					_PPFO_p[1] = a_PFO->getMomentum()[1];
					_PPFO_p[2] = a_PFO->getMomentum()[2];
	
                                	PandoraTotalP += currP;
					_TotalRecoP4_p[0] += _PPFO_p[0];
					_TotalRecoP4_p[1] += _PPFO_p[1];
					_TotalRecoP4_p[2] += _PPFO_p[2];
					_TotalRecoP4_p[3] += _PPFO_p[3];
					_outputPFOp->Fill();
				} 

                	}catch (lcio::DataNotAvailableException err) { };
			

		}

		try{
			LCCollection *ISOH = evtP->getCollection("AllIsolatedHits");
			int nIso = ISOH->getNumberOfElements();
			for(int ih = 0; ih < nIso; ih++)
			{
				CalorimeterHit * a_hit = dynamic_cast<CalorimeterHit*>(ISOH->getElementAt(ih));
				_IsoHEn = a_hit->getEnergy();
				TVector3 IHPos = a_hit->getPosition();
				_IsoPos[0] = IHPos.X();
				_IsoPos[0] = IHPos.Y();
				_IsoPos[0] = IHPos.Z();
				_DisIHC = 1.0e6;				
				_PFOType = 0;
				_CEnIso = 0;
				_CDepthIso = 0;

				try{
					LCCollection *PFO = evtP->getCollection( "ArborPFOs" );
					int NPFO = PFO->getNumberOfElements();
					for(int p0 =0;p0 < NPFO; p0++)
					{
						ReconstructedParticle *a_PFO=dynamic_cast<EVENT::ReconstructedParticle *>(PFO->getElementAt(p0)); 
						
						if(a_PFO->getClusters().size()>0)
						{
							Cluster* a_clu = a_PFO->getClusters()[0];
							float tmpDis = DisPointToBush(IHPos, a_clu);
							if(tmpDis < _DisIHC)
							{ 
								_DisIHC = tmpDis;
								_PFOType = a_PFO->getType();
								_CEnIso = a_clu->getEnergy();
								_CDepthIso = DisSeedSurface(a_clu->getPosition());
							}
						}
					}					

				}catch (lcio::DataNotAvailableException err) { };

				_outputIsoH->Fill();
			}
		}catch (lcio::DataNotAvailableException err) { };

		_Mass_a = ArborTotalP.M();
		_Mass_p = PandoraTotalP.M();

                _outputEvt->Fill();
                _Num++;

         }
}

void AnaSinglePart::end()
{

	if (_outputEvt) {

		TFile *tree_file = _outputEvt->GetCurrentFile(); //just in case we switched to a new file
		tree_file->Write();
		delete tree_file;
	}

}
