#include <TauAna.h>
#include <EVENT/LCCollection.h>
#include <IMPL/LCCollectionVec.h>
#include <EVENT/LCFloatVec.h>
#include <EVENT/MCParticle.h>
#include <EVENT/ReconstructedParticle.h>
#include <IMPL/MCParticleImpl.h>
#include <string>
#include <iostream>
#include <EVENT/LCFloatVec.h>
#include <EVENT/LCParameters.h>
#include <stdexcept>
#include <TFile.h> 
#include <TTree.h>
#include <TVector3.h>
#include <TRandom.h>
#include <Rtypes.h> 
#include <sstream>		
#include <cmath>
#include <vector>
#include <TMath.h>
#include "TLorentzVector.h"

using namespace std;

TauAna a_TauAna_instance;

TauAna::TauAna()
	: Processor("TauAna"),
	_output(0)
{
	_description = "Print MC Truth" ;

	_treeFileName="MCTruth.root";
	registerProcessorParameter( "TreeOutputFile" , 
			"The name of the file to which the ROOT tree will be written" ,
			_treeFileName ,
			_treeFileName);

	_treeName="Tau";
	registerProcessorParameter( "TreeName" , 
			"The name of the ROOT tree" ,
			_treeName ,
			_treeName);

	_overwrite=0;
	registerProcessorParameter( "OverwriteFile" , 
			"If zero an already existing file will not be overwritten." ,
			_overwrite ,
			_overwrite);

}

void TauAna::init() {

	printParameters();

	TFile *tree_file=new TFile(_treeFileName.c_str(),(_overwrite ? "RECREATE" : "UPDATE"));

	if (!tree_file->IsOpen()) {
		delete tree_file;
		tree_file=new TFile(_treeFileName.c_str(),"NEW");
	}

	_outputTree = new TTree(_treeName.c_str(),_treeName.c_str());
	_outputTau = new TTree("Taujets","Taujets");
	_outputMCTau = new TTree("MCTaujets","MCTaujets");
	_outputEvt = new TTree("Evt","Evt");
	_outputEvt->Branch("EventNr", &_eventNr, "EventNr/I");

	_outputEvt->Branch("tauAngle", &_tauAngle, "tauAngle/F");
	_outputEvt->Branch("MCTauPMass", &_MCTauPMass, "MCTauPMass/F");
	_outputEvt->Branch("TauPMass", &_TauPMass, "TauPMass/F");
	_outputEvt->Branch("MCTauPEn", &_MCTauPEn, "MCTauPEn/F");
	_outputEvt->Branch("TauPEn", &_TauPEn, "TauPEn/F");
	_outputEvt->Branch("MCTauPP[3]", _MCTauPP, "MCTauPP[3]/F");
	_outputEvt->Branch("TauPP[3]", _TauPP, "TauPP[3]/F");

	_outputEvt->Branch("MCTauMMass", &_MCTauMMass, "MCTauMMass/F");
	_outputEvt->Branch("TauMMass", &_TauMMass, "TauMMass/F");
	_outputEvt->Branch("MCTauMEn", &_MCTauMEn, "MCTauMEn/F");
	_outputEvt->Branch("TauMEn", &_TauMEn, "TauMEn/F");
	_outputEvt->Branch("MCTauMP[3]", _MCTauMP, "MCTauMP[3]/F");
	_outputEvt->Branch("TauMP[3]", _TauMP, "TauMP[3]/F");
	_outputEvt->Branch("HDecay[999]", _HDecay, "HDecay[999]/I");
	_outputEvt->Branch("HDDecay[999]", _HDDecay, "HDDecay[999]/I");
	_outputEvt->Branch("LZ0", &_LZ0, "LZ0/F");
	_outputEvt->Branch("LD0", &_LD0, "LD0/F");

	_outputEvt->Branch("NLZ0", &_NLZ0, "NLZ0/F");
	_outputEvt->Branch("NLD0", &_NLD0, "NLD0/F");
	_outputEvt->Branch("Aimp", &_Aimp, "Aimp/F");
	_outputEvt->Branch("Aimp", &_Aimp, "Aimp/F");
	_outputEvt->Branch("Bimp", &_Bimp, "Bimp/F");
	_outputEvt->Branch("cosTh", &_cosTh, "cosTh/F");
	_outputEvt->Branch("cosThR", &_cosThR, "cosThR/F");
	
	_outputEvt->Branch("Ancharged", &_Ancharged, 	"Ancharged/I");
	_outputEvt->Branch("Anphoton", 	&_Anphoton, 	"Anphoton/I");
	_outputEvt->Branch("AvisEn", 	&_AvisEn, 	"AvisEn/F");
	_outputEvt->Branch("Acharged",  &_Acharged,       "Acharged/F");
	_outputEvt->Branch("Acone1", 	&_Acone1, 	"Acone1/F");
	_outputEvt->Branch("Acone2", 	&_Acone2, 	"Acone2/F");
	_outputEvt->Branch("Acone3", 	&_Acone3, 	"Acone3/F");
	_outputEvt->Branch("AMass", 	&_AMass, 	"AMass/F");
	_outputEvt->Branch("AvisP[3]", _AvisP, "AvisP[3]/F");
	_outputEvt->Branch("Ane", &_Ane,    "Ane/I");
	_outputEvt->Branch("Anmu", &_Anmu,    "Anmu/I");
	_outputEvt->Branch("Anpi", &_Anpi,    "Anpi/I");
	_outputEvt->Branch("Bne", &_Bne,    "Bne/I");
	_outputEvt->Branch("Bnmu", &_Bnmu,    "Bnmu/I");
	_outputEvt->Branch("Bnpi", &_Bnpi,    "Bnpi/I");

	_outputEvt->Branch("Bncharged", &_Bncharged, 	"Bncharged/I");
	_outputEvt->Branch("Bnphoton", 	&_Bnphoton, 	"Bnphoton/I");
	_outputEvt->Branch("BvisEn", 	&_BvisEn, 	"BvisEn/F");
	_outputEvt->Branch("Bcharged", 	&_Bcharged, 	"Bcharged/F");
	_outputEvt->Branch("Bcone1", 	&_Bcone1, 	"Bcone1/F");
	_outputEvt->Branch("Bcone2",	&_Bcone2, 	"Bcone2/F");
	_outputEvt->Branch("Bcone3", 	&_Bcone3, 	"Bcone3/F");
	_outputEvt->Branch("BMass", 	&_BMass, 	"BMass/F");
	_outputEvt->Branch("BvisP[3]", _BvisP, "BvisP[3]/F");
	_outputEvt->Branch("dimu",    &_dimu,       "dimu/I");
	_outputEvt->Branch("recoilM",    &_recoilM,       "recoilM/F");
	_outputEvt->Branch("evtN",    &_evtN,       "evtN/I");
	_outputEvt->Branch("ATauM",     &_ATauM,        "ATauM/F");
	_outputEvt->Branch("BTauM",     &_BTauM,        "BTauM/F");
	
	_outputEvt->Branch("ArecoNch", &_ArecoNch,    "ArecoNch/I");
	_outputEvt->Branch("BrecoNch", &_BrecoNch,    "BrecoNch/I");
	_outputEvt->Branch("ArecoNph", &_ArecoNph,    "ArecoNph/I");
	_outputEvt->Branch("BrecoNph", &_BrecoNph,    "BrecoNph/I");
	_outputEvt->Branch("Arecoe", &_Arecoe,    "Arecoe/I");
	_outputEvt->Branch("Arecomu", &_Arecomu,    "Arecomu/I");
	_outputEvt->Branch("Arecopi", &_Arecopi,    "Arecopi/I");
	_outputEvt->Branch("ArecoEn", &_ArecoEn,    "ArecoEn/F");
	_outputEvt->Branch("Brecoe", &_Brecoe,    "Brecoe/I");
	_outputEvt->Branch("Brecomu", &_Brecomu,    "Brecomu/I");
	_outputEvt->Branch("Brecopi", &_Brecopi,    "Brecopi/I");
	_outputEvt->Branch("BrecoEn", &_BrecoEn,    "BrecoEn/F");
	_outputEvt->Branch("ArecoP[3]", _ArecoP, "ArecoP[3]/F");
	_outputEvt->Branch("BrecoP[3]", _BrecoP, "BrecoP[3]/F");
	_outputEvt->Branch("ArecoM",     &_ArecoM,        "ArecoM/F");
	_outputEvt->Branch("BrecoM",     &_BrecoM,        "BrecoM/F");
	_outputEvt->Branch("Reco_recoilM",    &_Reco_recoilM,       "Reco_recoilM/F");



	_outputTree->SetAutoSave(32*1024*1024);  // autosave every 32MB
	_outputTree->Branch("EventNr", &_eventNr, "EventNr/I");
	_outputTree->Branch("Num", &_Num, "Num/I");

	_outputTree->Branch("Charge", &_Charge, "Charge/F");
	_outputTree->Branch("P[3]", _P, "P[3]/F");
	_outputTree->Branch("En", &_En, "En/F");
	_outputTree->Branch("VisP[3]", _VisP, "VisP[3]/F");
	_outputTree->Branch("VisEn", &_VisEn, "VisEn/F");
	_outputTree->Branch("VisEnCh", &_VisEnCh, "VisEnCh/F");
	_outputTree->Branch("cone", &_cone, "cone/F");
	_outputTree->Branch("En_tr_all", &_En_tr_all, "En_tr_all/F");

	_outputTree->Branch("NMuon", &_NMuon, "NMuon/I");
	_outputTree->Branch("NEle", &_NEle, "NEle/I");
	_outputTree->Branch("NPion", &_NPion, "NPion/I");
	_outputTree->Branch("NKaon", &_NKaon, "NKaon/I");
	_outputTree->Branch("NPhoton", &_NPhoton, "NPhoton/I");
	_outputTree->Branch("NUndef", &_NUndef, "NUndef/I");

	_outputTree->Branch("Reco_Charge", &_Reco_Charge, "Reco_Charge/F");
	_outputTree->Branch("Reco_VisP[3]", _Reco_VisP, "Reco_VisP[3]/F");
        _outputTree->Branch("Reco_VisEn", &_Reco_VisEn, "Reco_VisEn/F");
	_outputTree->Branch("Reco_VisEnCh", &_Reco_VisEnCh, "Reco_VisEnCh/F");

	_outputTree->Branch("Reco_NMuon", &_Reco_NMuon, "Reco_NMuon/I");
        _outputTree->Branch("Reco_NEle", &_Reco_NEle, "Reco_NEle/I");
        _outputTree->Branch("Reco_NPion", &_Reco_NPion, "Reco_NPion/I");
        _outputTree->Branch("Reco_NPhoton", &_Reco_NPhoton, "Reco_NPhoton/I");
        _outputTree->Branch("Reco_NUndef", &_Reco_NUndef, "Reco_NUndef/I");	


	_outputTree->Branch("Mass", &_Mass, "Mass/F");
	_outputEvt->Branch("TauNumber", &_TauNumber, "TauNumber/I");
	_outputTau->Branch("TauEnergy", &_TauEnergy, "TauEnergy/F");
	_outputTau->Branch("Num", &_eventNr, "Num/I");
	_outputTau->Branch("MCTauM", &_MCTauM, "MCTauM/I");
	_outputTau->Branch("MCTauP", &_MCTauP, "MCTauP/I");
	_outputTau->Branch("TauCharge", &_TauCharge, "TauCharge/F");
	_outputTau->Branch("TauMass", &_TauMass, "TauMass/F");
	_outputTau->Branch("TauP[3]", _TauP, "TauP[3]/F");
	_outputEvt->Branch("MCTauNumber", &_MCTauNumber, "MCTauNumber/I");
	_outputMCTau->Branch("TauReco", &_TauReco, "TauReco/I");
	_outputMCTau->Branch("TauRecoE", &_TauRecoE, "TauRecoE/I");
	_outputMCTau->Branch("TauEnergy", &_MCTauEnergy, "TauEnergy/F");
	_outputMCTau->Branch("Num", 	  &_eventNr, "Num/I");
	_outputMCTau->Branch("TauCharge", &_MCTauCharge, "TauCharge/F");
	_outputMCTau->Branch("TauMass",   &_MCTauMass, "TauMass/F");
	_outputMCTau->Branch("TauP[3]",    _MC_TauP, "TauP[3]/F");
	_outputMCTau->Branch("Tag_e",    &_Tag_e, "Tag_e/I");
	_outputMCTau->Branch("Tag_mu",       &_Tag_mu, "Tag_mu/I");
	_outputMCTau->Branch("Tag_pi",       &_Tag_pi, "Tag_pi/I");
	_outputMCTau->Branch("Tag_photon",    &_Tag_photon, "Tag_photon/I");


	_outputTree->Branch("DeltaTheta", &_DeltaTheta, "DeltaTheta/F");
	_outputTree->Branch("DeltaPhi", &_DeltaPhi, "DeltaPhi/F");
	_outputTree->Branch("DeltaR", &_DeltaR, "DeltaR/F");

	_outputTree->Branch("DeltaPhiVis", &_DeltaPhiVis, "DeltaPhiVis/F");
	_outputTree->Branch("DeltaThetaVis", &_DeltaThetaVis, "DeltaThetaVis/F");
	

	_photonmass = new TH1F("Mgg", "Mgg", 400, 0, 0.5);
	_photonmass2 = new TH1F("Mgg2", "Mgg2", 400, 0, 0.5);
	_photonmass3 = new TH1F("Mgg3", "Mgg, expected error", 400, -0.25, 0.25);

	_Num = 0;
}

void TauAna::processEvent( LCEvent * evtP ) 
{		

	if (evtP) 								
	{		
		
		try 	
		{    

			_MCTauP=0;
			_MCTauM=0;
			_ATauM=100;
			_BTauM=100;
			_TauNumber=0;
			_dimu=0;
			_cosTh=0;
			_cosThR=0;

			
	//For each Tau Pair, write it

			LCCollection* col_MCP = evtP->getCollection( "MCParticle" ) ;			

			int _nMCP = col_MCP->getNumberOfElements();
			_eventNr=evtP->getEventNumber();
			cout<<"MCP "<<_evtN<<"th event"<<endl;
			
			TVector3 MCP_P, MCVisP, RecoP_P, TotalReco_P, ADir, BDir;

			// cout<<_nMCP<<" : "<<_nRecoP<<endl; 

			std::vector<MCParticle* > pos_tau_daughter; 
			std::vector<MCParticle* > neg_tau_daughter; 
			std::vector<MCParticle* > h_daughter; 
			std::vector<MCParticle* > MCtau; 
			std::vector<MCParticle* > fs_particle;
			std::vector<MCParticle* > curr_daughter;
			std::vector<MCParticle* > remain_par;
			std::vector<MCParticle* > Ajet_par;
			std::vector<MCParticle* > Bjet_par;
			std::vector<TVector3 > photon_p;
			pos_tau_daughter.clear();
			neg_tau_daughter.clear();
			remain_par.clear();
			MCtau.clear();
			fs_particle.clear();
			curr_daughter.clear();
			photon_p.clear();

			MCParticle * TauPlu(0);
			MCParticle * TauMin(0); 
			MCParticle * CurrTau(0); 

			TVector3 VTX, EndP, _p_tr, _p_ph;
			TLorentzVector P_T(0,0,0,250.0);
			std::vector<TLorentzVector> P_M;
			std::vector<TLorentzVector> P_P;
			TLorentzVector P_muM;
			TLorentzVector P_muP;
			TVector3 leadDir(1,1,1);
			float leadEn=0;
			float leadD1=0;
			float leadD2=0;
			float leadD3=0;
			float Zmassc=10.0;
			float invM=0;
			_recoilM=0;
			for(int i=0;i<_nMCP;i++)
                        {
                                MCParticle * a_MCP = dynamic_cast<MCParticle*>(col_MCP->getElementAt(i));
				MCParticle *a_parent = a_MCP;
				if(a_MCP->getParents().size()>0)
				{
					a_parent=a_MCP->getParents()[0];
					if(a_parent->getPDG()==25){
						if(abs(a_MCP -> getPDG()) == 15){
							if(a_MCP -> getPDG()>0){
							ADir=a_MCP ->getMomentum();
							}
							else{
								BDir=a_MCP->getMomentum();
							}
						}
						
					}
				}

			}

			for(int i=0;i<_nMCP;i++)
                        {
                                MCParticle * a_MCP = dynamic_cast<MCParticle*>(col_MCP->getElementAt(i));
				if(a_MCP->getPDG()!=25&&a_MCP->getDaughters().size()<2)continue;
				if(a_MCP->getPDG()==25&&a_MCP->getDaughters().size()>1){
					int nD=a_MCP->getDaughters().size();
					for(int id=0;id<nD;id++){
						_HDecay[id]=a_MCP->getDaughters()[id]->getPDG();
			
					}
					if(abs(_HDecay[0])==24||abs(_HDecay[0])==23){
						int nDD=a_MCP->getDaughters()[0]->getDaughters().size();
						for(int idd=0;idd<nDD;idd++){
						
							_HDDecay[idd]=a_MCP->getDaughters()[0]->getDaughters()[idd]->getPDG();
						}
					}
				}
			
				VTX = a_MCP->getVertex();
				EndP = a_MCP->getEndpoint();
				
			}
			for(int i=0;i<_nMCP;i++)
			{
				MCParticle * a_MCP = dynamic_cast<MCParticle*>(col_MCP->getElementAt(i));
				MCParticle * a_par=a_MCP;
				VTX = a_MCP->getVertex();
                                EndP = a_MCP->getEndpoint();
				
				if((a_MCP->getDaughters().size()==0 )&&(abs(a_MCP->getPDG())==13||abs(a_MCP->getPDG())==11)){	
				//if((a_MCP->getDaughters().size()==0|| (fabs(VTX.Z()) < 1000 && fabs(VTX.Perp()) < 1600 ) && ( fabs(EndP.Z()) > 2000 || fabs(EndP.Perp()) > 1600  ) )&&(abs(a_MCP->getPDG())==13||abs(a_MCP->getPDG())==11))
					//cout<<a_par->getPDG()<<endl;	
					if(abs(a_MCP->getPDG())==11){
						if(a_MCP->getParents().size()==0)continue;
						do{
							a_par=a_par->getParents()[0];
						}while(a_par->getParents().size()>0&&abs(a_par->getPDG())!=13);
						if(abs(a_par->getPDG())!=13)continue;
					}
					float muE=a_par->getEnergy();
					float muP0=a_par->getMomentum()[0];
					float muP1=a_par->getMomentum()[1];
					float muP2=a_par->getMomentum()[2];		
					TLorentzVector cur_P(muP0,muP1,muP2,muE);
					//cout<<muE<<"  "<<a_MCP->getPDG()<<"   "<<a_par->getPDG()<<endl;	
					if(a_par->getPDG()==13)
					{
						P_P.push_back(cur_P);	
					}else{
						P_M.push_back(cur_P);
					}
				}
			}
				int nm=P_M.size();
				int np=P_P.size();
				for(int im=0;im<nm;im++){
					for(int ip=0;ip<np;ip++){
						invM=(P_P[ip]+P_M[im]).M();	
						if(abs(invM-91.2)<Zmassc){
							_dimu=1;
							Zmassc=abs(invM-91.2);
							P_muP=P_P[ip];
							P_muM=P_M[im];
						}
					}
				}
				_recoilM=(P_T-P_muP-P_muM).M();
			for(int i=0;i<_nMCP;i++)
			{
				MCParticle * a_MCP = dynamic_cast<MCParticle*>(col_MCP->getElementAt(i));
				VTX = a_MCP->getVertex();
                                EndP = a_MCP->getEndpoint();
				
                                if( (fabs(VTX.Z()) < 1000 && fabs(VTX.Perp()) < 1600 ) && ( fabs(EndP.Z()) > 2000 || fabs(EndP.Perp()) > 1600  ) ){
					//cout<<a_MCP->getPDG()<<" "<<abs(a_MCP->getEnergy()-P_muP[3])<<" "<<abs(a_MCP->getMomentum()[0]-P_muP[0])<<endl;	
					if(abs(a_MCP->getPDG())==13&&((abs(a_MCP->getEnergy()-P_muP[3])<1.0e-4&&abs(a_MCP->getMomentum()[0]-P_muP[0])<1.0e-4)||(abs(a_MCP->getEnergy()-P_muM[3])<1.0e-4&&abs(a_MCP->getMomentum()[0]-P_muM[0])<1.0e-4))){
					continue;
					}
					if(a_MCP->getEnergy()>leadEn&&a_MCP->getCharge()!=0){
						leadEn=a_MCP->getEnergy();
						leadD1=a_MCP->getMomentum()[0];
						leadD2=a_MCP->getMomentum()[1];
						leadD3=a_MCP->getMomentum()[2];
					}	
					leadDir.SetXYZ(leadD1,leadD2,leadD3);
					remain_par.push_back(a_MCP);
				}
			}	
			int n_loop= remain_par.size();
			cout<<"remain MC"<<n_loop<<endl;
			_Ancharged=0;_Bncharged=0;
			_Anphoton=0;_Bnphoton=0;
			_Ane=0;_Anmu=0;_Anpi=0;
			_Bne=0;_Bnmu=0;_Bnpi=0;
			_AvisEn=0;_BvisEn=0;
			_Acharged=0;_Bcharged=0;
			_AMass=0;_BMass=0;
			_Acone1=0;_Acone2=0;_Acone3=0;
			_Bcone1=0;_Bcone2=0;_Bcone3=0;
			_AvisP[0]=0;
			_AvisP[1]=0;
			_AvisP[2]=0;
			_BvisP[0]=0;
			_BvisP[1]=0;
			_BvisP[2]=0;
			
			float lastpiA=0;
			float lastpiB=0;
			TVector3 Apidir;
			TVector3 Bpidir;
			TVector3 Apdir;
                        TVector3 Bpdir;
			for(int i=0;i<n_loop;i++){
				MCParticle * a_MCP = remain_par[i];
				int pid = a_MCP->getPDG();
				TVector3 curdir=a_MCP->getMomentum();
				float energy=a_MCP->getEnergy();
				int charge=a_MCP->getCharge();
				if(abs(pid)!=12&&abs(pid)!=14&&abs(pid)!=16){
				//cout<<"check cone for "<<pid<<"  angle:"<<curdir.Angle(leadDir)<<endl;
				if(curdir.Angle(leadDir)<1.){
					Ajet_par.push_back(a_MCP);
					_AvisP[0]+=a_MCP->getMomentum()[0];
					_AvisP[1]+=a_MCP->getMomentum()[1];
					_AvisP[2]+=a_MCP->getMomentum()[2];
					if(curdir.Angle(ADir)<1.5){
						_cosTh=curdir.Angle(ADir);
					}
					else
						_cosTh=curdir.Angle(BDir);
					
					if(charge!=0){
						_Ancharged++;
						if(abs(pid)==11){
							_Ane++;
						}
						else if(abs(pid)==13){
							_Anmu++;
						}
						else if(abs(pid)==211){
							_Anpi++;
							if(a_MCP->getEnergy()>lastpiA){
								Apidir=a_MCP->getMomentum();
								lastpiA=a_MCP->getEnergy();
							}
						}
						_AvisEn += energy;
						_Acharged += energy;
						
					}
					else if(pid==22){
						_Anphoton++;
						_AvisEn += energy;
					}
					
					//cout<<"A"<<_Ancharged<<endl;
				}
				else{
					Bjet_par.push_back(a_MCP);
					_BvisP[0]+=a_MCP->getMomentum()[0];
                                        _BvisP[1]+=a_MCP->getMomentum()[1];
                                        _BvisP[2]+=a_MCP->getMomentum()[2];
					if(charge!=0){
						_Bncharged++;
						_BvisEn += energy;
						_Bcharged +=energy;
						if(abs(pid)==11){
							_Bne++;
						}
						else if(abs(pid)==13){
							_Bnmu++;
						}
						else if(abs(pid)==211){
							_Bnpi++;
							if(a_MCP->getEnergy()>lastpiB){
                                                                Bpidir=a_MCP->getMomentum();
                                                                lastpiB=a_MCP->getEnergy();
                                                        }
						}
					}
					if(pid==22){
						_Bnphoton++;
						_BvisEn += energy;
					}
					//cout<<"B"<<_Bncharged<<endl;

				}
				}	

			}	
			int n_loopA=Ajet_par.size();
			if(_Ancharged&&_Anphoton){
			for(int i=0;i<n_loopA;i++){
				MCParticle * a_MCP = Ajet_par[i];
                                int pid = a_MCP->getPDG();
				float energy = a_MCP->getEnergy();
                                TVector3 curdir=a_MCP->getMomentum();
                                int charge=a_MCP->getCharge();
				if(energy<1.)continue;
				if(pid==22&&curdir.Angle(leadDir)>_Acone1){
					_Acone1=curdir.Angle(leadDir);
					Apdir=a_MCP->getMomentum();
					//cout<<"Acone1"<<_Acone1<<endl;
				}
				else if(charge!=0&&curdir.Angle(leadDir)>_Acone2){
					_Acone2=curdir.Angle(leadDir);
				}

			}
			for(int i=0;i<n_loopA;i++){
                                MCParticle * a_MCP = Ajet_par[i];
                                int pid = a_MCP->getPDG();
                                TVector3 curdir=a_MCP->getMomentum();
				float energy = a_MCP->getEnergy();
		
                                if(pid==22&&curdir.Angle(Apdir)>_Acone3&&energy>1.){
                                        _Acone3=curdir.Angle(Apdir);
                                }
			}	
			}
			int n_loopB=Bjet_par.size();
			float BleadE=0;
			TVector3 Bleaddir;
			if(_Bncharged&&_Bnphoton){
			for(int j=0;j<n_loopB;j++){
				MCParticle * a_MCP = Bjet_par[j];
				float energy=a_MCP->getEnergy();
				if(energy<BleadE)continue;
				BleadE=energy;
				Bleaddir=a_MCP->getMomentum();
			}
			for(int i=0;i<n_loopB;i++){
				MCParticle * a_MCP = Bjet_par[i];
                                int pid = a_MCP->getPDG();
                                TVector3 curdir=a_MCP->getMomentum();
				float energy=a_MCP->getEnergy();
                                int charge=a_MCP->getCharge();
				//cout<<"pho energy"<<energy<<endl;
				if(energy<1.)continue;
				if(pid==22&&curdir.Angle(Bleaddir)>_Bcone1){
					_Bcone1=curdir.Angle(Bleaddir);
					Bpdir=a_MCP->getMomentum();
					//cout<<"Bcone1"<<_Bcone1<<endl;
				}
				else if(charge!=0&&curdir.Angle(Bpidir)>_Bcone2){
					_Bcone2=curdir.Angle(Bleaddir);
				}

			}
			for(int i=0;i<n_loopB;i++){
                                MCParticle * a_MCP = Bjet_par[i];
                                int pid = a_MCP->getPDG();
                                TVector3 curdir=a_MCP->getMomentum();
				float energy=a_MCP->getEnergy();
				if(energy>1.&&pid==22&&curdir.Angle(Bpdir)>_Bcone3){
                                        _Bcone3=curdir.Angle(Bpdir);
                                }
			}	
			}
			//cout<<"cone1 "<<_Acone1<<"  "<<_Bcone1<<endl;
			TLorentzVector AP(_AvisP[0],_AvisP[1],_AvisP[2],_AvisEn);
			TLorentzVector BP(_BvisP[0],_BvisP[1],_BvisP[2],_BvisEn);
			//_AMass = sqrt((_AvisEn+_BvisEn)*(_AvisEn+_BvisEn)-(_AvisP[0]+_BvisP[0])*(_AvisP[0]+_BvisP[0])-(_AvisP[1]+_BvisP[1])*(_AvisP[1]+_BvisP[1])-(_AvisP[2]+_BvisP[2])*(_AvisP[2]+_BvisP[2]));//higgs mass
			
//			_BMass = (P_T-AP-BP).M();
			_AMass=sqrt(_AvisEn*_AvisEn-_AvisP[0]*_AvisP[0]-_AvisP[1]*_AvisP[1]-_AvisP[2]*_AvisP[2]);
			_BMass=sqrt(_BvisEn*_BvisEn-_BvisP[0]*_BvisP[0]-_BvisP[1]*_BvisP[1]-_BvisP[2]*_BvisP[2]);
			



			LCCollection* col_RecoP = evtP->getCollection( "ArborPFOs" );
			int _nRecoP=col_RecoP->getNumberOfElements();	
			TLorentzVector recoP_muM;
			TLorentzVector recoP_muP;
			TVector3 LDir(1,1,1);
			float LEn=0;
			float mcone=100.0;
			_Reco_recoilM=0;

			_ArecoNch=0;_BrecoNch=0;
			_ArecoNph=0;_BrecoNph=0;
			_Arecoe=0;_Arecomu=0;_Arecopi=0;
			_Brecoe=0;_Brecomu=0;_Brecopi=0;
			_ArecoEn=0;_BrecoEn=0;
			//_Acharged=0;_Bcharged=0;
			_ArecoM=0;_BrecoM=0;
			//_Acone1=2;_Acone2=2;_Acone3=2;
			//_Bcone1=2;_Bcone2=2;_Bcone3=2;
			_ArecoP[0]=0;
			_ArecoP[1]=0;
			_ArecoP[2]=0;
			_BrecoP[0]=0;
			_BrecoP[1]=0;
			_BrecoP[2]=0;
			


			std::vector<TLorentzVector> FourM_MuP;
			std::vector<TLorentzVector> FourM_MuM;
			std::vector<ReconstructedParticle* > candi_par;
			candi_par.clear();

			for(int k = 0; k < _nRecoP; k++)
			{
				ReconstructedParticle * a_RecoP = dynamic_cast<ReconstructedParticle*>(col_RecoP->getElementAt(k));
				float RecoPID = a_RecoP->getType();
				float RecoE = a_RecoP->getEnergy();
				float muP0=a_RecoP->getMomentum()[0];
                                float muP1=a_RecoP->getMomentum()[1];
                                float muP2=a_RecoP->getMomentum()[2];
				TLorentzVector cur_P(muP0,muP1,muP2,RecoE);
				for(int l=0;l<_nRecoP;l++)
				{
					if(RecoPID==13){
						FourM_MuM.push_back(cur_P);	
					}
					if(RecoPID==-13){
						FourM_MuP.push_back(cur_P);
					}
				}	
			}
			int rnm = FourM_MuM.size();
			int rnp = FourM_MuP.size();
			for(int im=0;im<rnm;im++){
                                for(int ip=0;ip<rnp;ip++){
                                        invM=(FourM_MuP[ip]+FourM_MuM[im]).M();
                                        if(abs(invM-91.2)<mcone){
                                                mcone=abs(invM-91.2);
                                                recoP_muP=FourM_MuP[ip];
                                                recoP_muM=FourM_MuM[im];
                                        }
                                }
                        }
			for(int i=0;i<_nRecoP;i++){
				ReconstructedParticle * a_RecoP = dynamic_cast<ReconstructedParticle*>(col_RecoP->getElementAt(i));
				if(abs(a_RecoP->getType())==13&&((abs(a_RecoP->getEnergy()-recoP_muP[3])<1.0e-3&&abs(a_RecoP->getMomentum()[0]-recoP_muP[0])<1.0e-3)||(abs(a_RecoP->getEnergy()-recoP_muM[3])<1.0e-3&&abs(a_RecoP->getMomentum()[0]-recoP_muM[0])<1.0e-3)))continue;
				if(a_RecoP->getEnergy()>LEn&&a_RecoP->getCharge()!=0){
					LEn=a_RecoP->getEnergy();
					LDir=a_RecoP->getMomentum();
					Track *Ltrk=a_RecoP->getTracks()[0];
					_LD0=Ltrk->getD0();
					_LZ0=Ltrk->getZ0();
				}
				candi_par.push_back(a_RecoP);
			}
			int n_candi=candi_par.size();
			float BLen=0;
			for(int i=0;i<n_candi;i++){
				ReconstructedParticle * a_RecoP = candi_par[i];
				int pid=a_RecoP->getType();
				TVector3 curdir=a_RecoP->getMomentum();
				float energy=a_RecoP->getEnergy();
				if(energy<0.5)continue;
				int charge=a_RecoP->getCharge();
				if(curdir.Angle(LDir)<1.){
					if(curdir.Angle(ADir)<1.5){
						_cosThR=curdir.Angle(ADir);
					}
					else
						_cosThR=curdir.Angle(BDir);
					

					_ArecoP[0]+=a_RecoP->getMomentum()[0];
					_ArecoP[1]+=a_RecoP->getMomentum()[1];
					_ArecoP[2]+=a_RecoP->getMomentum()[2];
					_ArecoEn+=energy;
					//_ArechEch+=energy;
					if(charge!=0){
						_ArecoNch++;
						if(abs(pid)==11){
							_Arecoe++;
						}
						if(abs(pid)==13){
                                                        _Arecomu++;
                                                }
						if(abs(pid)==211){
                                                        _Arecopi++;
                                                }
					}
					if(pid==22){
						_ArecoNph++;
						_ArecoEn+=energy;
					}
				}
				else{
					_BrecoP[0]+=a_RecoP->getMomentum()[0];
                                        _BrecoP[1]+=a_RecoP->getMomentum()[1];
                                        _BrecoP[2]+=a_RecoP->getMomentum()[2];
                                        _BrecoEn+=energy;
					if(energy>BLen){
						BLen=energy;
						_NLD0=0;
						_NLZ0=0;
						if(a_RecoP->getTracks().size()>0){
							Track *Ltrk=a_RecoP->getTracks()[0];
							_NLD0=Ltrk->getD0();
							_NLZ0=Ltrk->getZ0();
						}
					}
                                        //_BrechEch+=energy;
                                        if(charge!=0){
                                                _BrecoNch++;
                                                if(abs(pid)==11){
                                                        _Brecoe++;
                                                }
                                                if(abs(pid)==13){
                                                        _Brecomu++;
                                                }
                                                if(abs(pid)==211){
                                                        _Brecopi++;
                                                }
                                        }
					if(pid==22){
						_BrecoNph++;
						_BrecoEn+=energy;
					}
				}
			}
			TLorentzVector ArecoFourP(_ArecoP[0],_ArecoP[1],_ArecoP[2],_ArecoEn);
			TLorentzVector BrecoFourP(_BrecoP[0],_BrecoP[1],_BrecoP[2],_BrecoEn);
			_Aimp=LEn/_ArecoEn;
			_Bimp=BLen/_BrecoEn;
			_ArecoM=sqrt(_ArecoEn*_ArecoEn-_ArecoP[0]*_ArecoP[0]-_ArecoP[1]*_ArecoP[1]-_ArecoP[2]*_ArecoP[2]);
			_BrecoM=sqrt(_BrecoEn*_BrecoEn-_BrecoP[0]*_BrecoP[0]-_BrecoP[1]*_BrecoP[1]-_BrecoP[2]*_BrecoP[2]);
                        _Reco_recoilM=(P_T-recoP_muP-recoP_muM).M();
			
			for(int i = 0; i < _nMCP; i++)
                        {
                                MCParticle * a_MCP = dynamic_cast<MCParticle*>(col_MCP->getElementAt(i));
				//	if(a_MCP->getDaughters().size() == 0 && a_MCP->getParents().size() > 0)
				VTX = a_MCP->getVertex();
				EndP = a_MCP->getEndpoint();
//				if( VTX.Mag() < 10 && EndP.Mag() > 10 )
//				if( (fabs(VTX.Z()) < 2000 && fabs(EndP.Z()) > 2000 ) || (fabs(VTX.Perp()) < 1600 && fabs(EndP.Perp()) > 1600) )
//				if( (fabs(VTX.Z()) < 1000 && fabs(VTX.Perp()) < 1600 ) && ( fabs(EndP.Z()) > 2000 || fabs(EndP.Perp()) > 1600  ) )
				if(VTX.Mag() < 10 && EndP.Mag() > 10)

				{
					fs_particle.push_back(a_MCP);
				}
				MCParticle *a_parent = a_MCP;
				if(a_MCP->getParents().size()>0)
				{
					//do{
					//	a_parent = a_parent->getParents()[0];
					//}while( a_parent ->getParents().size() > 0&& abs(a_parent->getPDG()) != 25);
					//cout<<"parent PDG"<<a_parent->getPDG()<<endl;	
					a_parent=a_MCP->getParents()[0];
					if(a_parent->getPDG()==25){
						//cout<<"a higgs"<<endl;
						int curpdg =a_MCP -> getPDG();
						//cout<<"current pdg"<<a_MCP -> getPDG()<<endl;
						if( a_MCP -> getPDG() == 15 )
						{
							TauMin = a_MCP; 
							MCtau.push_back(a_MCP);
							_MCTauM++;
							}
						else if( a_MCP -> getPDG() == -15 )
						{
							TauPlu = a_MCP;
							MCtau.push_back(a_MCP);
							_MCTauP++;
						}
					}
				}
			}
			
			for(int j = 0; j < int(fs_particle.size()); j++)
			{
				MCParticle * b_p = fs_particle[j];
 
				MCParticle * p = b_p;
				do{
					p = p->getParents()[0];
				}
				while( p->getParents().size() > 0 && abs(p->getPDG()) != 15 );

				if(p->getPDG() == 15)
				{
					neg_tau_daughter.push_back(b_p);
					

				
				}
				else if(p->getPDG() == -15)
				{
					pos_tau_daughter.push_back(b_p);
				}
			}
			cout<<"tau+:tau-"<<_MCTauP<<" : "<<_MCTauM<<endl;

			// cout<<"sss "<<pos_tau_daughter.size()<<" : "<<neg_tau_daughter.size()<<endl;
			if(_MCTauP>0&&_MCTauM>0){
			for(int i = 0; i < 2; i++)
			{
				if(i == 0)
				{ 
					CurrTau = TauPlu;
					curr_daughter = pos_tau_daughter; 
				} 
				else 
				{
					CurrTau = TauMin; 
					curr_daughter = neg_tau_daughter; 
				}

				photon_p.clear();
				_Charge = CurrTau->getCharge(); 
				_En = CurrTau->getEnergy(); 
				_VisEn = 0; 
				_VisEnCh = 0;
				MCP_P = CurrTau->getMomentum();
				MCVisP.SetXYZ(0, 0, 0);
				TotalReco_P.SetXYZ(0, 0, 0);
				_P[0] = CurrTau->getMomentum()[0];
				_P[1] = CurrTau->getMomentum()[1];
				_P[2] = CurrTau->getMomentum()[2];
				_VisP[0] = 0; _VisP[1] = 0; _VisP[2] = 0;
				_Reco_VisP[0] = 0; _Reco_VisP[1] = 0; _Reco_VisP[2] = 0;
				_p_tr[0]=0;_p_tr[1]=0;_p_tr[2]=0;
				_p_ph[0]=0;_p_ph[1]=0;_p_ph[2]=0;
				_TauP[0]=0;_TauP[1]=0;_TauP[2]=0;
				_NMuon = 0; _NEle = 0; _NPion = 0; _NKaon = 0; _NPhoton = 0; _NUndef = 0; 
				_Reco_NMuon = 0; _Reco_NEle = 0; _Reco_NPion = 0; _Reco_NPhoton = 0; _Reco_NUndef = 0; 
				_TauMass = 0; _TauEnergy = 0; _TauCharge = 0;
				float _En_tr = 0; float _En_ph = 0;
				_cone = 0; _En_tr_all = 0;

				for(int j = 0; j < int(curr_daughter.size()); j++)
				{
					MCParticle * a_DauP = curr_daughter[j];
					if(abs(a_DauP->getPDG()) != 12 && abs(a_DauP->getPDG()) != 14 && abs(a_DauP->getPDG()) != 16 )
					{
						_VisEn += a_DauP->getEnergy();
						MCVisP += a_DauP->getMomentum();
						// cout<<j<<" : "<<_VisEn<<endl; 

						_VisP[0] = a_DauP->getMomentum()[0];
						_VisP[1] = a_DauP->getMomentum()[1];
						_VisP[2] = a_DauP->getMomentum()[2];

						if( a_DauP->getCharge() != 0 )
							_VisEnCh += a_DauP->getEnergy();

						if(a_DauP->getEnergy() > 1.0)
						{
							if( abs(a_DauP->getPDG()) == 13 ){
								_NMuon++;
								_p_tr = a_DauP->getMomentum ();
								_En_tr = a_DauP->getEnergy();
							}
							else if( abs(a_DauP->getPDG()) == 11 ){
								_p_tr = a_DauP->getMomentum ();
								_En_tr = a_DauP->getEnergy();
								_NEle++;							
							}
							else if( abs(a_DauP->getPDG()) == 211 ){
								_p_tr = a_DauP->getMomentum ();
								_En_tr = a_DauP->getEnergy();
								
								_NPion++;
							}
							else if( abs(a_DauP->getPDG()) == 321 )
								_NKaon++;
						}
						if( a_DauP->getEnergy() > 0.2 )
						{	
							if( a_DauP->getPDG() == 22 ){
								_p_ph = a_DauP->getMomentum ();
								_En_ph = a_DauP->getEnergy();	
								_NPhoton++;
							}
							else 
								_NUndef++;
						}
					}
					_cone = _p_ph.Angle(_p_tr);
					_En_tr_all = _En_tr/(_En_tr+_En_ph);
				}
				if(i==0){
					_ATauM=sqrt(_VisEn*_VisEn-MCVisP[0]*MCVisP[0]-MCVisP[1]*MCVisP[1]-MCVisP[2]*MCVisP[2]);
				}
				else{
					_BTauM=sqrt(_VisEn*_VisEn-MCVisP[0]*MCVisP[0]-MCVisP[1]*MCVisP[1]-MCVisP[2]*MCVisP[2]);
				}
				

				_Reco_Charge = 0;
				_Reco_VisEn = 0;
				_Reco_VisEnCh = 0;
				


							// cout<<"Gausss"<<endl;
				_outputTree->Fill();
				_Num++;
			}
			}
						_MCTauNumber=MCtau.size();
			_MCTauPEn=0;
				_MCTauMEn=0;
				_MCTauMP[0]=0;
				_MCTauMP[1]=0;
				_MCTauMP[2]=0;
				_MCTauPP[0]=0;
				_MCTauPP[1]=0;
				_MCTauPP[2]=0;

			
			for(int j=0; j< _MCTauNumber; j++)
                        {
                                MCParticle * a_MCP = MCtau[j];
				_TauReco=0;
					
				_MC_TauP[0] = 0;
				_MC_TauP[1] = 0;
				_MC_TauP[2] = 0;
								_MCTauEnergy = 0;
                                //_MC_TauP[0] = a_MCP->getMomentum()[0];
                                //_MC_TauP[1] = a_MCP->getMomentum()[1];
                                //_MC_TauP[2] = a_MCP->getMomentum()[2];
                                //_MCTauEnergy= a_MCP->getEnergy();
                                _MCTauCharge= a_MCP->getCharge();
                                _MCTauMass =  a_MCP->getMass();
				float win_en=1;
				float win_angle=1;
				if(_MCTauCharge>0)
                                {
                                        curr_daughter = pos_tau_daughter;
					_MCTauPMass=a_MCP->getMass();
					
					for(int id = 0; id < int(curr_daughter.size()); id++)
                                	{
					MCParticle * a_DauP = curr_daughter[id];
			
                                        if(abs(a_DauP->getPDG()) != 12 && abs(a_DauP->getPDG()) != 14 && abs(a_DauP->getPDG()) != 16){
                                                _MCTauPEn += a_DauP->getEnergy();
                                                _MCTauPP[0] += a_DauP->getMomentum()[0];
                                                _MCTauPP[1] += a_DauP->getMomentum()[1];
                                                _MCTauPP[2] += a_DauP->getMomentum()[2];
                                        }
					}
                                }
                                else
                                {
                                        curr_daughter = neg_tau_daughter;
					_MCTauMMass=a_MCP->getMass();

                                        for(int id = 0; id < int(curr_daughter.size()); id++)
                                        {
                                        MCParticle * a_DauP = curr_daughter[id];

                                        if(abs(a_DauP->getPDG()) != 12 && abs(a_DauP->getPDG()) != 14 && abs(a_DauP->getPDG()) != 16){
                                                _MCTauMEn += a_DauP->getEnergy();
                                                _MCTauMP[0] += a_DauP->getMomentum()[0];
						_MCTauMP[1] += a_DauP->getMomentum()[1];
                                                _MCTauMP[2] += a_DauP->getMomentum()[2];
						if(abs(a_DauP->getPDG())==11){
							_Tag_e++;
						}
						else if(abs(a_DauP->getPDG())==13){
                                                        _Tag_mu++;
                                                }
						else if(abs(a_DauP->getPDG())==211){
                                                        _Tag_pi++;
                                                }
						else if(abs(a_DauP->getPDG())==22){
                                                        _Tag_photon++;
                                                }
						
                                        }
					}
                                }
				TVector3 tauP(_MCTauMP[0],_MCTauMP[1],_MCTauMP[2]);
				TVector3 tauM(_MCTauPP[0],_MCTauPP[1],_MCTauPP[2]);
				_tauAngle=tauP.Angle(tauM);
				for(int k = 0; k < int(curr_daughter.size()); k++)
                                {
					MCParticle * a_DauP = curr_daughter[k];
					if(abs(a_DauP->getPDG()) != 12 && abs(a_DauP->getPDG()) != 14 && abs(a_DauP->getPDG()) != 16){
						_MCTauEnergy += a_DauP->getEnergy();
						_MC_TauP[0] += a_DauP->getMomentum()[0];
                                		_MC_TauP[1] += a_DauP->getMomentum()[1];
                                		_MC_TauP[2] += a_DauP->getMomentum()[2];
					}
				}
				
                                _outputMCTau->Fill();
                        }
			_outputEvt->Fill();
			_evtN++;
		}		
		catch (lcio::DataNotAvailableException err) { }

	}  	  

}	

void TauAna::end()
{

	if (_outputTree) {

		TFile *tree_file = _outputTree->GetCurrentFile(); //just in case we switched to a new file
		//tree_file->cd();
		tree_file->Write();
		delete tree_file;
		//tree_file->Close();
	}

}



