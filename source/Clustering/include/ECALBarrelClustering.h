#ifndef _ECALBARRELCLUSTERING_
#define _ECALBARRELCLUSTERING_

#include "ArborClustering.h"
#include "TVector3.h"

typedef std::pair<std::vector<ArborCaloHit>::iterator, std::vector<ArborCaloHit>::iterator> ArborCaloHitRange;

class ECALBarrelClustering : public ArborClustering
{
public:
	ECALBarrelClustering(std::vector<ArborCaloHit>& arborCaloHitVec);

	void Clustering();

	void print();
private:
	void SortHits();

	std::vector<ArborCaloHit>* m_CaloHitVec;

};

#endif
