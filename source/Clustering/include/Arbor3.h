#ifndef _Arbor3_h_
#define _Arbor3_h_

#include "ArborHit3.h"
#include <string>
#include <iostream>
#include <TVector3.h>
#include "ArborTool.h"
#include <unordered_map>
#include <map>

typedef std::unordered_multimap<unsigned,unsigned> HitLinkMap;

class Arbor3 {

public:
	   //Arbor3();
       void Clustering( std::vector<ArborHit3> );

	   // getters
       inline linkcoll& getCleanedLinks() { return CleanedLinks; }
       inline linkcoll& getIterLinks_1() { return IterLinks_1; }
       inline linkcoll& getIterLinks() { return IterLinks; }
       inline linkcoll& getlinks_debug() { return links_debug; }

       inline branchcoll& getTrees() { return Trees; } 
       inline std::vector<int>& getIsoHitsIndex() { return IsoHitsIndex; }
	   inline std::vector<ArborHit3>& getPreClusteredHits() { return preClusteredHits; }

       void init();

private:
	   void IsoHitsClassification();
	   void printParameters();

	   void Clear();

       void HitsCleaning( std::vector<ArborHit3> inputHits );
	   void PreClustering();
       
       void BuildInitLink();
	   void LinkClean(linkcoll alllinks);
       void LinkIteration(int time);
       
       void BranchBuilding();
	   void BranchRebuiding();

private:
       HitLinkMap IterBackLinks;
       
       std::vector<ArborHit3> cleanedHits;
       std::vector<ArborHit3> preClusteredHits;

       std::vector<int> IsoHitsIndex;
       
       linkcoll Links;
       linkcoll CleanedLinks;
       linkcoll IterInputLinks; 
       linkcoll alliterlinks;
       linkcoll links_debug; 
       linkcoll IterLinks; 
       linkcoll IterLinks_1; 

       branchcoll Trees; 

	   // ----
	   bool  mergeHit;

	   float hitClusteringDistanceECAL;
	   float hitClusteringDistanceHCAL;

	   float initSearchRangeECAL1;
	   float initSearchRangeECAL2;
	   float initSearchRangeHCAL;
	   float initSearchRangeMUON;

	   float iterationSearchRangeECAL1;
	   float iterationSearchRangeECAL2;
	   float iterationSearchRangeHCAL1;
	   float iterationSearchRangeHCAL2;
	   float iterationSearchRangeMUON1;
	   float iterationSearchRangeMUON2;

	   float linkAngleCutECAL;
	   float smallAngleWeightECAL;
	   float linkLengthCutAtLargeAngleECAL;
	   float largeAngleWeightECAL;
	   float shortLinkCutECAL1;
	   float shortLinkCutECAL2;
	   float shortLinkFactorECAL;
	   float longLinkCutECAL;

	   float linkAngleCutHCAL;
	   float smallAngleWeightHCAL;
	   float linkLengthCutAtLargeAngleHCAL;
	   float largeAngleWeightHCAL;
	   float shortLinkCutHCAL;
	   float shortLinkFactorHCAL;
	   float longLinkCutHCAL;

	   float linkAngleCutMUON;               
	   float smallAngleWeightMUON;          
	   float linkLengthCutAtLargeAngleMUON;
	   float largeAngleWeightMUON;
	   float shortLinkCutMUON;           
	   float shortLinkFactorMUON;         
	   float longLinkCutMUON;            

	   int   ecalFirstPartLayer;

	   bool  useTime;
	   float hitTimeCut;
	   float hitTimeResolution;
	   int   hitTimeDiffInLinking;
	   int   hitTimeDiffInClustering;
};
#endif
