#ifndef _ARBORCLUSTERING_
#define _ARBORCLUSTERING_

#include "ArborCaloHit.h"

class ArborClustering
{
public:
	ArborClustering();

	virtual void print() = 0;
	virtual void Clustering() = 0;

private:
};

#endif
