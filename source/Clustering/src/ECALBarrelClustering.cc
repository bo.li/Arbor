#include "ECALBarrelClustering.h"
#include "MarlinArbor2.h"

#include <iostream>

using namespace std;

ECALBarrelClustering::ECALBarrelClustering(std::vector<ArborCaloHit>& arborCaloHitVec)
	                : m_CaloHitVec(&arborCaloHitVec)
{
	cout << "In ECALBarrelClustering. ECALBarrel hit: " << arborCaloHitVec.size() << endl;
}

void ECALBarrelClustering::Clustering()
{
	cout << "In ECALBarrelClustering::Clustering" << endl;
	SortHits();

	int test = MarlinArbor2::getParameters()->getIntVal("Test");

	cout << "Test ---- " << test << endl;
}

void ECALBarrelClustering::SortHits()
{
	cout << "In ECALBarrelClustering::SortHits" << endl;

	std::vector<ArborCaloHit>& arborCaloHitVec = *m_CaloHitVec;

	// sort the ArborCaloHit in the vector by stave, module and layer
	std::sort(arborCaloHitVec.begin(), arborCaloHitVec.end(), arborCaloHitCompare());

	auto it = arborCaloHitVec.begin();
  
	// the range will constain two iterators for the first hit and last hit
	// with the same (stave, module, layer) by using equal_range()
	vector<ArborCaloHitRange> Ranges;

	while (it!=arborCaloHitVec.end()) {
		ArborCaloHit& referenceHit = *it;

		ArborCaloHitRange bounds = 
		    std::equal_range(it, arborCaloHitVec.end(), referenceHit, arborCaloHitCompare());

		std::cout << "stave: " << referenceHit.GetStave() << ", module: " << referenceHit.GetModule()
			      << ", layer: " << referenceHit.GetLayer() << std::endl;

#if 0		
		//////////////////////////// check if the hit is close to the module boundary
		//  SetNearSideOfHit(ArborCaloHit hit);
		int stave = referenceHit.GetStave();
		int module = referenceHit.GetModule();

		for(auto it=bounds.first; it<bounds.second; ++it) { 
			TVector3 hitPos = it->GetPosition();

			std::cout << "stave: " << stave << ", module: " << module 
				      << "  hit position: " << hitPos.X() << ", " << hitPos.Y() << ", " << hitPos.Z() 
					  << std::endl;

			std::vector< std::pair<ModuleSide, double> > sides = 
	                  m_ecalBarrelModules[stave][module-1].getNearSides(hitPos);// hit module starts from 1 
			
			std::vector< std::pair<ModuleSide, double> > validSides;

			for(auto sideIt=sides.begin(); sideIt!=sides.end(); ++sideIt) {
				auto& side = (*sideIt).first;
				auto& distance = (*sideIt).second;
			   	//std::cout << "------------close side: " << side << std::endl;

				// the condition of valid side based on ILD geometry
				if(side==LEFT||side==TOP||side==BOTTOM||side==DOWN) {
					it->SetNearModuleBoundary();
					validSides.emplace_back(std::pair<ModuleSide, double>(side, distance));
					m_ecalBarrelModules[stave][module-1].PutHit2Side(it, side);
				}
		   	}

			it->SetNearSides(validSides);
		}
		////////////////////////////
#endif

		it += bounds.second - bounds.first;

		Ranges.push_back(bounds);
	}
}

void ECALBarrelClustering::print()
{
}
