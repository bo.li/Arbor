#include <Arbor3.h>
#include <TTree.h>
#include <algorithm>
#include <TMath.h>
#include "KDTreeLinkerAlgoT.h"
#include <iomanip>

#include <marlin/Processor.h>
#include "gear/LayerLayout.h"
#include "gear/CalorimeterParameters.h"

#include <MarlinArbor3.h>

using namespace std; 
using namespace gear;

void Arbor3::init() 
{
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// get parameters from steering file
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	mergeHit = (bool)MarlinArbor3::getParameters()->getIntVal("HitMergeInPreclustering");

	hitClusteringDistanceECAL = MarlinArbor3::getParameters()->getFloatVal("HitClusteringDistanceECAL");
	hitClusteringDistanceHCAL = MarlinArbor3::getParameters()->getFloatVal("HitClusteringDistanceHCAL");

	initSearchRangeECAL1 = MarlinArbor3::getParameters()->getFloatVal("InitSearchRangeECAL1");
	initSearchRangeECAL2 = MarlinArbor3::getParameters()->getFloatVal("InitSearchRangeECAL2");
	initSearchRangeHCAL  = MarlinArbor3::getParameters()->getFloatVal("InitSearchRangeHCAL");
	initSearchRangeMUON  = MarlinArbor3::getParameters()->getFloatVal("InitSearchRangeMUON");

	iterationSearchRangeECAL1 = MarlinArbor3::getParameters()->getFloatVal("IterationSearchRangeECAL1");
	iterationSearchRangeECAL2 = MarlinArbor3::getParameters()->getFloatVal("IterationSearchRangeECAL2");
	iterationSearchRangeHCAL1 = MarlinArbor3::getParameters()->getFloatVal("IterationSearchRangeHCAL1");
	iterationSearchRangeHCAL2 = MarlinArbor3::getParameters()->getFloatVal("IterationSearchRangeHCAL2");
	iterationSearchRangeMUON1 = MarlinArbor3::getParameters()->getFloatVal("IterationSearchRangeMUON1");
	iterationSearchRangeMUON2 = MarlinArbor3::getParameters()->getFloatVal("IterationSearchRangeMUON2");

	linkAngleCutECAL                 = MarlinArbor3::getParameters()->getFloatVal("LinkAngleCutECAL");
	smallAngleWeightECAL             = MarlinArbor3::getParameters()->getFloatVal("SmallAngleWeightECAL");
	linkLengthCutAtLargeAngleECAL    = MarlinArbor3::getParameters()->getFloatVal("LinkLengthCutAtLargeAngleECAL");
	largeAngleWeightECAL             = MarlinArbor3::getParameters()->getFloatVal("LargeAngleWeightECAL");
	shortLinkCutECAL1                = MarlinArbor3::getParameters()->getFloatVal("ShortLinkCutECAL1");
	shortLinkCutECAL2                = MarlinArbor3::getParameters()->getFloatVal("ShortLinkCutECAL2");
	shortLinkFactorECAL              = MarlinArbor3::getParameters()->getFloatVal("ShortLinkFactorECAL");
	longLinkCutECAL                  = MarlinArbor3::getParameters()->getFloatVal("LongLinkCutECAL");

	linkAngleCutHCAL                 = MarlinArbor3::getParameters()->getFloatVal("LinkAngleCutHCAL");
	smallAngleWeightHCAL             = MarlinArbor3::getParameters()->getFloatVal("SmallAngleWeightHCAL");
	linkLengthCutAtLargeAngleHCAL    = MarlinArbor3::getParameters()->getFloatVal("LinkLengthCutAtLargeAngleHCAL");
	largeAngleWeightHCAL             = MarlinArbor3::getParameters()->getFloatVal("LargeAngleWeightHCAL");
	shortLinkCutHCAL                 = MarlinArbor3::getParameters()->getFloatVal("ShortLinkCutHCAL");
	shortLinkFactorHCAL              = MarlinArbor3::getParameters()->getFloatVal("ShortLinkFactorHCAL");
	longLinkCutHCAL                  = MarlinArbor3::getParameters()->getFloatVal("LongLinkCutHCAL");

	linkAngleCutMUON                 = MarlinArbor3::getParameters()->getFloatVal("LinkAngleCutMUON");
	smallAngleWeightMUON             = MarlinArbor3::getParameters()->getFloatVal("SmallAngleWeightMUON");
	linkLengthCutAtLargeAngleMUON    = MarlinArbor3::getParameters()->getFloatVal("LinkLengthCutAtLargeAngleMUON");
	largeAngleWeightMUON             = MarlinArbor3::getParameters()->getFloatVal("LargeAngleWeightMUON");
	shortLinkCutMUON                 = MarlinArbor3::getParameters()->getFloatVal("ShortLinkCutMUON");
	shortLinkFactorMUON              = MarlinArbor3::getParameters()->getFloatVal("ShortLinkFactorMUON");
	longLinkCutMUON                  = MarlinArbor3::getParameters()->getFloatVal("LongLinkCutMUON");

	// time
	useTime                          = (bool)MarlinArbor3::getParameters()->getIntVal("UseTime");

	hitTimeCut                       = MarlinArbor3::getParameters()->getFloatVal("HitTimeCut");
	
	//FIXME: ECAL and HCAL have the same time resolution ?
	hitTimeResolution                = MarlinArbor3::getParameters()->getFloatVal("HitTimeResolution");

	hitTimeDiffInLinking             = MarlinArbor3::getParameters()->getFloatVal("HitTimeDiffInLinking");
	hitTimeDiffInClustering          = MarlinArbor3::getParameters()->getFloatVal("HitTimeDiffInClustering");

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// get parameters from GEAR file
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	const CalorimeterParameters& ecalBarrel = MarlinArbor3::getGearMgr()->getEcalBarrelParameters();
    const LayerLayout& ecalBarrelLayerLayout = ecalBarrel.getLayerLayout();

	int iLayer;

	// decide the layer of the first part of ECAL by looping the layers
	for(iLayer=1; iLayer<ecalBarrelLayerLayout.getNLayers(); ++iLayer) {
		double deltaThickness = 
			   ecalBarrelLayerLayout.getThickness(iLayer) - ecalBarrelLayerLayout.getThickness(iLayer-1);
		if(deltaThickness>0.) break;
	}

	ecalFirstPartLayer = iLayer;

	// print all the parameters
	printParameters();
}

void Arbor3::printParameters() 
{
	cout << "HitMergeInPreclustering        " << mergeHit                      << endl
		 << "HitClusteringDistanceECAL      " << hitClusteringDistanceECAL     << endl
		 << "HitClusteringDistanceHCAL      " << hitClusteringDistanceHCAL     << endl
		 << "InitSearchRangeECAL1           " << initSearchRangeECAL1          << endl
		 << "InitSearchRangeECAL2           " << initSearchRangeECAL2          << endl
		 << "InitSearchRangeHCAL            " << initSearchRangeHCAL           << endl
		 << "InitSearchRangeMUON            " << initSearchRangeMUON           << endl
		 << "IterationSearchRangeECAL1      " << iterationSearchRangeECAL1     << endl
		 << "IterationSearchRangeECAL2      " << iterationSearchRangeECAL2     << endl
		 << "IterationSearchRangeHCAL1      " << iterationSearchRangeHCAL1     << endl
		 << "IterationSearchRangeHCAL2      " << iterationSearchRangeHCAL2     << endl
		 << "IterationSearchRangeMUON1      " << iterationSearchRangeMUON1     << endl
		 << "IterationSearchRangeMUON2      " << iterationSearchRangeMUON2     << endl
         << "LinkAngleCutECAL               " << linkAngleCutECAL              << endl 
         << "SmallAngleWeightECAL           " << smallAngleWeightECAL          << endl    
         << "LinkLengthCutAtLargeAngleECAL  " << linkLengthCutAtLargeAngleECAL << endl 
         << "LargeAngleWeightECAL           " << largeAngleWeightECAL          << endl  
         << "ShortLinkCutECAL1              " << shortLinkCutECAL1             << endl  
         << "ShortLinkCutECAL2              " << shortLinkCutECAL2             << endl   
         << "ShortLinkFactorECAL            " << shortLinkFactorECAL           << endl  
         << "LongLinkCutECAL                " << longLinkCutECAL               << endl 
         << "LinkAngleCutHCAL               " << linkAngleCutHCAL              << endl   
         << "SmallAngleWeightHCAL           " << smallAngleWeightHCAL          << endl  
         << "LinkLengthCutAtLargeAngleHCAL  " << linkLengthCutAtLargeAngleHCAL << endl  
         << "LargeAngleWeightHCAL           " << largeAngleWeightHCAL          << endl  
         << "ShortLinkCutHCAL               " << shortLinkCutHCAL              << endl  
         << "ShortLinkFactorHCAL            " << shortLinkFactorHCAL           << endl   
         << "LongLinkCutHCAL                " << longLinkCutHCAL               << endl   
         << "LinkAngleCutMUON               " << linkAngleCutMUON              << endl   
         << "SmallAngleWeightMUON           " << smallAngleWeightMUON          << endl  
         << "LinkLengthCutAtLargeAngleMUON  " << linkLengthCutAtLargeAngleMUON << endl  
         << "LargeAngleWeightMUON           " << largeAngleWeightMUON          << endl  
         << "ShortLinkCutMUON               " << shortLinkCutMUON              << endl  
         << "ShortLinkFactorMUON            " << shortLinkFactorMUON           << endl   
         << "LongLinkCutMUON                " << longLinkCutMUON               << endl   
         << "UseTime                        " << useTime                       << endl   
         << "HitTimeCut                     " << hitTimeCut                    << endl   
         << "HitTimeResolution              " << hitTimeResolution             << endl   
         << "HitTimeDiffInLinking           " << hitTimeDiffInLinking          << endl   
         << "HitTimeDiffInClustering        " << hitTimeDiffInClustering       << endl
         << "EcalFirstPartLayer             " << ecalFirstPartLayer            << endl; 
	     
}

void Arbor3::Clear() 
{
	cleanedHits.clear();
	preClusteredHits.clear();

	IsoHitsIndex.clear();

	Links.clear();
	CleanedLinks.clear();
	IterLinks_1.clear();
	IterLinks.clear();
	IterInputLinks.clear();
	IterBackLinks.clear();
	alliterlinks.clear();
	links_debug.clear();

	Trees.clear();
}

void Arbor3::HitsCleaning( std::vector<ArborHit3> inputHits )
{
	//Currently we cannot really do much things here. Mapping before calling
    cleanedHits = inputHits;        

	cout << "the size of input hits: " << inputHits.size() << endl;
}

// PreClustering:
// make a clustering of hits in each layer before linking
void Arbor3::PreClustering()
{
	preClusteredHits = cleanedHits;

	// sort the hit by stave, module and layer
	std::sort(preClusteredHits.begin(), preClusteredHits.end(), arborCaloHitCompare());

#if 0
	cout << "PreClusteredHits size (after sorting): " << preClusteredHits.size() << endl;

	for(int iHit=0; iHit<preClusteredHits.size(); ++iHit) {
		cout << "hit: " << iHit << endl;
		preClusteredHits[iHit].print();
	}
#endif

	if(mergeHit) {
		auto it = preClusteredHits.begin();
  
	    while (it!=preClusteredHits.end()) {
	   		ArborHit3& referenceHit = *it;
	   
	   		auto bounds = std::equal_range(it, preClusteredHits.end(), referenceHit, arborCaloHitCompare());
	   
	   		//std::cout << "stave: " << referenceHit.GetStave() << ", module: " << referenceHit.GetModule()
	   		//	      << ", layer: " << referenceHit.GetLayer() << std::endl;
	   
	   		// Make preclustering in each layer
	   
	   		// Create link for close hits
	   		// We don't use k-d tree here, since the hit number in a layer with the same stave and
	   		// module will not be too large.
	   		for(auto hitIter1=bounds.first; hitIter1<bounds.second; ++hitIter1) {
	   			//(*hitIter1).print();
	   
	   			for(auto hitIter2=hitIter1+1; hitIter2<bounds.second; ++hitIter2) {

	   				CaloHitType hitType = hitIter2->GetHitType();

	   				float hitDistanceCut = 0.;

	   				if(hitType<=ECALOther) {
	   					// ECAL: 7.5
	   					hitDistanceCut = hitClusteringDistanceECAL;
	   				}
	   				else if(hitType<=HCALOther) {
	   					// HCAL: 45.0
	   					hitDistanceCut = hitClusteringDistanceHCAL;
	   				}
					else if(hitType==MUON) {
						// FIXME
						hitDistanceCut = hitClusteringDistanceHCAL;
					}


	   				double hitsDist = hitIter1->getDistanceWith((*hitIter2));
	   				//cout << "hits dist: " << hitsDist << endl;

					if(hitsDist>hitDistanceCut) continue;

					// time option
					if(useTime) {
				    	double timeHit1 = hitIter1->GetTime();
	   			        double timeHit2 = hitIter2->GetTime();
	   			    	double timeDiff = fabs(timeHit1-timeHit2);

				    	timeDiff = int(timeDiff/hitTimeResolution);
	   
	   			    	if(timeDiff>hitTimeDiffInClustering) continue;
					}
	   			
					hitIter1->addLinkedHitInLayer(hitIter2);
					hitIter2->addLinkedHitInLayer(hitIter1);
	   				//cout << "a link is created" << endl;
	   			}
	   		}
	   
	   		// merge hits according to the links
	   		for(auto hitIter1=bounds.first; hitIter1<bounds.second; ++hitIter1) {
	   
	   			// In principle, we don't need to check status of merging, this is only for safty.
	   			if(hitIter1->IsDropped() || hitIter1->IsMerged()) continue;
	   			hitIter1->MakeMergedHit();
	   		}
	   
	   		//cout << "--" << endl;
	   
	   		it += bounds.second - bounds.first;
	   	}
	} // if mergeHit


	//////////////////////
	cleanedHits.clear();

	for(int iHit=0; iHit<preClusteredHits.size(); ++iHit) {
		if(mergeHit && preClusteredHits[iHit].IsDropped() ) continue;
		if(useTime && preClusteredHits[iHit].GetTime()>hitTimeCut) continue;
		cleanedHits.emplace_back(preClusteredHits[iHit]);
	}

	cout << "******** cleanedHits: " << cleanedHits.size() << endl;

#if 0
    cout << "PreClusteredHits (after clustering): " << cleanedHits.size() << endl;
#endif

#if 0
    cout << "PreClusteredHits (after clustering): " << endl;

	for(int iHit=0; iHit<cleanedHits.size(); ++iHit) {
		cout << "hit: " << iHit << endl;
		cleanedHits[iHit].print();
	}
#endif
}

void Arbor3::BuildInitLink()
{
	KDTreeLinkerAlgo<unsigned,3> kdtree;
    typedef KDTreeNodeInfoT<unsigned,3> KDTreeNodeInfo;
	std::array<float,3> minpos{ {0.0f,0.0f,0.0f} }, maxpos{ {0.0f,0.0f,0.0f} };

	// found -> nearByHitIndex;
    std::vector<KDTreeNodeInfo> nodes, found;

	for(int i0=0; i0<cleanedHits.size(); ++i0)
    { 
		const auto& hit = cleanedHits[i0].GetPosition();
        nodes.emplace_back(i0,(float)hit.X(),(float)hit.Y(),(float)hit.Z());

        if(i0==0)
        {
		   	minpos[0] = hit.X(); minpos[1] = hit.Y(); minpos[2] = hit.Z();
            maxpos[0] = hit.X(); maxpos[1] = hit.Y(); maxpos[2] = hit.Z();
        }
        else
        { 
			minpos[0] = std::min((float)hit.X(),minpos[0]);
            minpos[1] = std::min((float)hit.Y(),minpos[1]);
            minpos[2] = std::min((float)hit.Z(),minpos[2]);
            maxpos[0] = std::max((float)hit.X(),maxpos[0]);
            maxpos[1] = std::max((float)hit.Y(),maxpos[1]);
            maxpos[2] = std::max((float)hit.Z(),maxpos[2]);
        }
    }

    KDTreeCube kdXYZCube(minpos[0],maxpos[0], minpos[1],maxpos[1], minpos[2],maxpos[2]);

    kdtree.build(nodes,kdXYZCube);
    nodes.clear();

	Links.clear();	//all initial links

	for(int i0=0; i0<cleanedHits.size(); ++i0)
	{
		found.clear();

		TVector3 PosA = cleanedHits[i0].GetPosition();

		//cout << endl;
		//cleanedHits[i0].print();

		// the search range
		float side = 0.;	
		
		CaloHitType hitType = cleanedHits[i0].GetHitType();

		//if(hitType==ECALBarrel || hitType==ECALEndcap || hitType==ECALOther) {
		if(hitType<=ECALOther) {
		    //ECAL default: 20
        
			side = cleanedHits[i0].GetLayer()<ecalFirstPartLayer ? initSearchRangeECAL1: initSearchRangeECAL2;

#if 0
			cout << "hit type: " << hitType << ", initSearchRangeECAL: " << initSearchRangeECAL << endl;
#endif
		} 
		else if(hitType<=HCALOther) {
		    //HCAL default: 50
			side = initSearchRangeHCAL;	
#if 0
			cout << "hit type: " << hitType << ", initSearchRangeHCAL: " << initSearchRangeHCAL << endl;
#endif
		}
		else if(hitType==MUON) {
		    side = initSearchRangeMUON;
		}

		float xmin(PosA.X() - side);
        float xmax(PosA.X() + side);

		float ymin(PosA.Y() - side);
        float ymax(PosA.Y() + side); 
		
		float zmin(PosA.Z() - side);
        float zmax(PosA.Z() + side); 

        KDTreeCube searchcube(xmin, xmax, ymin, ymax, zmin, zmax);

        kdtree.search(searchcube,found);

#if 0
		cout << endl;
		cleanedHits[i0].print();
		cout << "found hits by k-d tree in building init. link: " << found.size() << endl;
#endif

		// the best link determined by distance
		std::pair<int, int> bestLink(0, 0);

		double minDistance = 1.e6;

		//cout << "found.size: " << found.size() << endl;

		// TODO:
		// Can k-d tree get the nearest hit first ?
		for(unsigned int j0 = 0; j0 < found.size(); ++j0)
		{
			int indexOfNearHit = found[j0].data;

			// make sure that the combination of two hits used only once
			if(indexOfNearHit>=(unsigned)i0) continue;
			
			int layerHitA = cleanedHits[i0].GetLayer();
			int layerHitB = cleanedHits[indexOfNearHit].GetLayer();

			// the hit combination should not have the same layer number.
			bool isSameLayer = layerHitA == layerHitB;
			if(isSameLayer) continue;

			// require the two hits are in the same type
			bool isDiffHitType = cleanedHits[i0].GetHitType() != cleanedHits[indexOfNearHit].GetHitType();
			if(isDiffHitType) continue;
		    
		    TVector3 PosB = cleanedHits[indexOfNearHit].GetPosition();
		
			//cleanedHits[indexOfNearHit].print();

			double hitsDistance = (PosA - PosB).Mag();

			//// test...
			//TVector3 testPos(1567.093, -1094.386, -1098.742);
			//if((testPos-PosB).Mag()<1) cout << "--------> the test hit" << endl;

			if(hitsDistance<minDistance)
				minDistance = hitsDistance;
			else 
				continue;

			// create a link with proper direction

			bool timeDetermined = false;

			if(useTime) {
				float timeA = cleanedHits[i0].GetTime();
			    float timeB = cleanedHits[indexOfNearHit].GetTime();

			    float timeDiff = timeA - timeB;
				int timeDiffInt= int(timeDiff/hitTimeResolution);

			    if(fabs(timeDiffInt)<hitTimeDiffInLinking && fabs(timeDiffInt)>0) {
					bestLink.first  = timeDiffInt>0 ? indexOfNearHit : i0;
					bestLink.second = timeDiffInt>0 ? i0: indexOfNearHit;

					timeDetermined = true;
				}
				else if(fabs(timeDiffInt) > hitTimeDiffInLinking) {
					continue;
				}
			}

			// if we don't use time or even time can not decide the direction
			// then use layer number
			if(!useTime || !timeDetermined) {
			    // if it is not pratical to use the time information,
		        // the direction of link is determined by the layer number 
				bestLink.first  = layerHitA > layerHitB ? indexOfNearHit : i0;
				bestLink.second = layerHitA > layerHitB ? i0 : indexOfNearHit;
			}
		}

		if(bestLink.first!=bestLink.second) Links.push_back(bestLink);
	}

	links_debug = Links; 

#if 1
	cout << "init link size: " << Links.size() << endl;
#endif
}

////////////////////////////////////////////////////////////////////////////////
void Arbor3::LinkClean(linkcoll alllinks )
{
	//creat a link collection (direction of going in) for each hit
	std::vector< std::vector<int> > linksIn;

	linksIn.resize(cleanedHits.size());

    for(int iLink = 0; iLink < alllinks.size(); ++iLink) {
		int hitIn  = alllinks[iLink].first;
	    int hitOut = alllinks[iLink].second;

        linksIn[hitOut].push_back(hitIn);
    }
	
	// For each hit, get the best link we have already
	//
	// Since the hits are sorted by layer, actually 
	// *** WE ARE LOOPING HITS LAYER BY LAYER ***
#if 1
	linkcoll cleanedLinks; 

    for(int iHit=0; iHit<cleanedHits.size(); ++iHit) {

        int bestHitIndex = -1;
        float minDistance = 1.e6;

		//these are all the hits linked (i.e. pointing) to the i-th hit
        std::vector<int>& hitLinks = linksIn[iHit];

		if(hitLinks.size()==0) continue;

		// for each link (or linked hit), decide a hit which have minimal "distance" to the i-th hit
        for(int iLink=0; iLink<hitLinks.size(); ++iLink) {
            TVector3 PosA = cleanedHits[ hitLinks[iLink] ].GetPosition();
			TVector3 PosB = cleanedHits[iHit].GetPosition();

            float hitDistance = (PosB - PosA).Mag();

			// TODO: should we take angle into account ?
            if(hitDistance<minDistance) {
                bestHitIndex = hitLinks[iLink];
                minDistance  = hitDistance;
            }
			// -------------
        }

        std::pair<int, int> bestLink;
		bestLink.first  = bestHitIndex;
        bestLink.second = iHit;

		// test
		//int layerA = cleanedHits[bestLink.first ].GetLayer();
		//int layerB = cleanedHits[bestLink.second].GetLayer();
		//if(layerA>=layerB) cleanedLinks.push_back(bestLink);

		cleanedLinks.push_back(bestLink);
    }
#endif

#if 0
    cout << "Size of cleanedLinks: " << cleanedLinks.size() << endl;
#endif

	CleanedLinks = cleanedLinks;
}

void Arbor3::LinkIteration(int time)
{
	// The current algorithm of link iteration:
	// 1) Initialization of k-d tree, reference directions of hits and other stuff by the hits 
	//    and links we got before.
	// 2) For each hit, using k-d tree, which is defined by the center and its distance to the side, 
	//    to get the nearby hits and then decide if it is the proper one to link with: 
	//    conditions should be fulfilled, i.e., the distance and angle between two hits are less than
	//    the prescribled cut, and the two possible hits are not linked yet.
	// 3) Clean links(code is almost the same with LinkClean)
	//
	//
	// TODO:
	// a. initial link: short and local link
	// b. iteration 1: longer link (ineffeciency, gap between sub-detectors ...)
	// c. iteration 2: longest link for the not connected hit; only for the not well connected hit ?
	
#if 0
	cout << "LinkIteration: time " << time << endl;
#endif

	KDTreeLinkerAlgo<unsigned,3> kdtree;
    typedef KDTreeNodeInfoT<unsigned,3> KDTreeNodeInfo;

	std::array<float,3> minpos{ {0.0f,0.0f,0.0f} }, maxpos{ {0.0f,0.0f,0.0f} };
    std::vector<KDTreeNodeInfo> nodes, found;

	// the reference direction of each hit
	std::vector< TVector3 > referenceDir;
	referenceDir.resize(cleanedHits.size());

	// number of link starting (or going in) with a hit
    int Nin_hit[cleanedHits.size()];

	// number of link ending (or going out) with a hit
    int Nout_hit[cleanedHits.size()];

	for(int i0=0; i0<cleanedHits.size(); ++i0)
    { 
		const auto& hit = cleanedHits[i0].GetPosition();
		
		// initialization for the number of linked hit, in and out
        Nin_hit[i0] = 0;
        Nout_hit[i0] = 0;

        nodes.emplace_back(i0,(float)hit.X(),(float)hit.Y(),(float)hit.Z());

		// find the min/max position
        if( i0 == 0 )
        {
            minpos[0] = hit.X(); minpos[1] = hit.Y(); minpos[2] = hit.Z();
            maxpos[0] = hit.X(); maxpos[1] = hit.Y(); maxpos[2] = hit.Z();
        }
        else
        {
            minpos[0] = std::min((float)hit.X(),minpos[0]);
            minpos[1] = std::min((float)hit.Y(),minpos[1]);
            minpos[2] = std::min((float)hit.Z(),minpos[2]);
            maxpos[0] = std::max((float)hit.X(),maxpos[0]);
            maxpos[1] = std::max((float)hit.Y(),maxpos[1]);
            maxpos[2] = std::max((float)hit.Z(),maxpos[2]);
        }
    }

	// a k-d tree cube with parametrized by the max and min position
    KDTreeCube kdXYZCube(minpos[0], maxpos[0], minpos[1], maxpos[1], minpos[2], maxpos[2]);
    kdtree.build(nodes,kdXYZCube);
    nodes.clear();

	if(time == 1)
	{
		IterLinks_1.clear();
		alliterlinks = CleanedLinks;
	}
	else
	{
		IterLinks.clear();
		alliterlinks = IterLinks_1;
	}


#if 1
	// 1-to-1 hit link: the direction of link going in
	std::vector<int> hitInLinks;
	hitInLinks.resize(cleanedHits.size());
	
	// 1-to-muti hit link: the direction of link going out
	std::vector< std::vector<int> > hitOutLinks;
	hitOutLinks.resize(cleanedHits.size());

	// initialization
	for(int j=0; j<cleanedHits.size(); ++j) {
		hitInLinks[j] = -1;
	}

	//
	for(int j=0; j<alliterlinks.size(); ++j) {
		std::pair<int, int>& currlink = alliterlinks[j];

		hitInLinks[currlink.second] = currlink.first;

		Nin_hit[currlink.first]++;
		Nout_hit[currlink.second]++;

		hitOutLinks[currlink.first].push_back(currlink.second);
	}

	// TODO:
	// since the hits are looped LAYER BY LAYER,
	// if we store the reference direction of each hit in a layer,
	// then they can be used in the next layer.
	for(int i=0; i<cleanedHits.size(); ++i) {
		TVector3 refDir = cleanedHits[i].GetPosition();

		//cleanedHits[i].print();

		if(hitInLinks[i]!=-1) { 
			refDir = cleanedHits[i].GetPosition() - cleanedHits[hitInLinks[i]].GetPosition();
		
			//the 
	     	if(hitInLinks[ hitInLinks[i] ]!=-1) { 
	     		refDir += cleanedHits[hitInLinks[i]].GetPosition() - cleanedHits[ hitInLinks[ hitInLinks[i] ] ].GetPosition();
	     	}
		}

		refDir = refDir.Unit();
		referenceDir[i] = refDir;
	}
#endif

// debug output
#if 0
	cout << "--------------------------------------------" << endl;
	cout << "size of alliterlinks: " << alliterlinks.size() << endl;
	for(int iLink=0; iLink<alliterlinks.size(); ++iLink) {
		auto link = alliterlinks[iLink];
		cout << "Link " << iLink << ": hit " << link.first << "-hit " << link.second << endl;
		cleanedHits[link.first].print();
		cleanedHits[link.second].print();
	}

	cout << ">>>>>>>>>>>>>>>>>>>>>>>>>" << endl << endl;

	for(int iHit=0; iHit<cleanedHits.size(); ++iHit) {
		cout << "Hit " << iHit << " Link --- Nin: " << Nin_hit[iHit] << " Nout: " << Nout_hit[iHit] << endl;
	}
	cout << "--------------------------------------------" << endl << endl;
#endif


	// for each hit, try to find nearby hits 
	for(int i1=0; i1<cleanedHits.size(); ++i1)
	{
		found.clear();

		TVector3 PosA = cleanedHits[i1].GetPosition();
		
		int NLayer_A = cleanedHits[i1].GetLayer();
		int NStave_A = cleanedHits[i1].GetStave();
		int SubD_A = cleanedHits[i1].GetSubD();

		//cout << "SubD_A: " << SubD_A <<  endl;


		// FIXME: do we really need this large region?
		// need specified region for each subdetector
		
		// the search range
		float side = 0.;	
		
		CaloHitType hitType = cleanedHits[i1].GetHitType();

		if(hitType<=ECALOther) {
			//ECALBarrel: 
			//const float side = 20 + 10*time;
			//ECALEndcap:
		    //const float side = 20 + 1*time;

			if(time==1) 
				side = iterationSearchRangeECAL1;	
			else
				side = iterationSearchRangeECAL2;	

#if 0
			cout << "time: " << time << ", hit type: " << hitType << ", side: " << side << endl;
#endif
		} 
		else if(hitType<=HCALOther){
		    //HCAL default: 50
		    //const float side = 50 + 50*time;

			if(time==1) 
				side = iterationSearchRangeHCAL1;	
			else
				side = iterationSearchRangeHCAL2;	

#if 0
			cout << "time: " << time << ", hit type: " << hitType << ", side: " << side << endl;
#endif
		}
		else if(hitType==MUON) {
			if(time==1) 
				side = iterationSearchRangeMUON1;
			else
				side = iterationSearchRangeMUON2;
		}

        const float xplus(PosA.X() + side), xminus(PosA.X() - side);
        const float yplus(PosA.Y() + side), yminus(PosA.Y() - side);
        const float zplus(PosA.Z() + side), zminus(PosA.Z() - side);
        const float xmin(std::min(xplus,xminus)), xmax(std::max(xplus,xminus));
        const float ymin(std::min(yplus,yminus)), ymax(std::max(yplus,yminus));
        const float zmin(std::min(zplus,zminus)), zmax(std::max(zplus,zminus));

        KDTreeCube searchcube(xmin, xmax, ymin, ymax, zmin, zmax);
        kdtree.search(searchcube,found);

		// TODO: 
		// maybe we can reduce the number of hits found by k-d tree
#if 0
		// this seems does not work ...
		if(time==2 && found.size()>200) {
			//cout << "the found hits size: " << found.size() << endl;
		    continue;
		}
#endif

		// for each nearby hit found by the k-d tree ...
		for(unsigned int j1 = 0; j1 < found.size(); ++j1)
		{
#if 0
			int SubD_B1 = cleanedHits[found[j1].data].GetSubD();

			if(SubD_A*SubD_B1==2) { 
				if( found[j1].data >= (unsigned)i1 )
				cout << "SubD_A: " << SubD_A << ", SubD_B: " << SubD_B1 << endl;
			}
#endif
			// check the combination of two hits only once
			if( found[j1].data >= (unsigned)i1 ) continue;

#if 0
			cleanedHits[i1].print();
			cout << "k-d tree found hits: " << found.size() << ", current hit: " << j1 
				 << ", the possible link: " << i1 << "-" << found[j1].data << endl;
			cleanedHits[found[j1].data].print();

#endif
			// TODO:: it is better to use a distance cut here to reduce the CPU time
	
			// Flag of at least one hit is not isolated
			int FlagNoIso = Nout_hit[found[j1].data] + Nin_hit[i1] + Nout_hit[i1] + Nin_hit[found[j1].data];
			
			if(FlagNoIso==0) continue;
			
			// Flag of both hits are jointed 
			// if one is zero, the flag will be zero
			//int FlagJoint = Nout_hit[found[j1].data] * Nin_hit[i1] * Nout_hit[i1] * Nin_hit[found[j1].data];
			
			//if(FlagJoint || FlagNoIso==0) continue;

			bool isConnected = false;

			std::vector<int>& linksOfHitI = hitOutLinks[i1];
			std::vector<int>& linksOfHitJ = hitOutLinks[found[j1].data];

			for(auto it=linksOfHitI.begin(); it!=linksOfHitI.end(); ++it) {
				int hitConnected = *it;

				if(hitConnected==found[j1].data) {
					isConnected = true;
					break;
				}
			}

			// a piece of code copied from upper lines :(
			if(!isConnected) {
			    for(auto it=linksOfHitJ.begin(); it!=linksOfHitJ.end(); ++it) {
			    	int hitConnected = *it;

			    	if(hitConnected==i1) {
			    		isConnected = true;
			    		break;
			    	}
			    }
			}

			TVector3 PosB = cleanedHits[found[j1].data].GetPosition();

			int NLayer_B = cleanedHits[found[j1].data].GetLayer();
			int NStave_B = cleanedHits[found[j1].data].GetStave();
			int SubD_B = cleanedHits[found[j1].data].GetSubD();

#if 0
			cout << "FlagJoint: " << FlagJoint << ", FlagNoIso: " << FlagNoIso << ", connected: " << isConnected << endl;
			cout << endl;
#endif

				if(!isConnected)
				{	
					bool FlagEE = false; 
					bool FlagHH = false; 
					bool FlagEH = false; 

					if(SubD_A == 1 && SubD_B == 1 )
						FlagEE = true;

					// FIXME:: it's very important to check this...
					//if(SubD_A > 2 && SubD_B == 2 )
					if(SubD_A == 2 && SubD_B == 2 )
						FlagHH = true;

					if(SubD_A * SubD_B == 2)
						FlagEH = true;

					//TODO: connection of MSTC ?

#if 0
					if(FlagEH) {
				    	cout << "----------------" << endl;
				    	cleanedHits[i1].print();
				    	cout << "N_i: " << Nin_hit[i1] 
				    		 << ", N_o: " << Nout_hit[i1] << endl; 

				    	int foundj1 = found[j1].data;

				    	cleanedHits[foundj1].print();
			            cout << "N_i: " << Nin_hit[foundj1] 
				    		 << ", N_o: " << Nout_hit[foundj1] << endl;

						TVector3 PosDiffAB = PosB - PosA; 

			            float DisAB = PosDiffAB.Mag();			
				    	cout << "DisAB: " << DisAB << 
				    	 ", FlagEE: " << FlagEE << ", FlagHH: " << FlagHH << endl;
					}
#endif

					if(FlagEE || FlagHH || FlagEH)
					{
						std::pair<int, int> a_Link; 

						if( NStave_A != NStave_B || ( NLayer_A == 0 && NLayer_B != 0 ) || ( NLayer_B == 0 && NLayer_A != 0 ) 
							|| (NLayer_A != NLayer_B && NStave_A == NStave_B) )
						{
#if 0
							cout << "NStave_A: " << NStave_A << ", " << "NStave_B: " << NStave_B << endl;
#endif
							/////FIXME: copying code
		                	bool timeDetermined = false;

		                	if(useTime) {
								float timeA = cleanedHits[i1].GetTime();
								float timeB = cleanedHits[found[j1].data].GetTime();

		                	    float timeDiff = timeA - timeB;
		                		int timeDiffInt= int(timeDiff/hitTimeResolution);

		                	    if(fabs(timeDiffInt)<hitTimeDiffInLinking && fabs(timeDiffInt)>0) {
		                			a_Link.first  = timeDiffInt>0 ? found[j1].data : i1;
		                			a_Link.second = timeDiffInt>0 ? i1: found[j1].data;

		                			timeDetermined = true;
		                		}
								else if(fabs(timeDiffInt)>hitTimeDiffInLinking) {
									continue;
								}
		                	}

		                	// if we don't use time or even time can not decide the direction
		                	// then use layer number
		                	if(!useTime || !timeDetermined) {
		                	    // if it not pratical to use the time information,
		                        // the direction of link is determined by the layer number or hit magnitude
								bool isHitBFirst = false;

								if(cleanedHits[i1].GetStave()==cleanedHits[found[j1].data].GetStave()) {
									if(NLayer_A > NLayer_B) isHitBFirst = true;
								}
								else {
									double magA = cleanedHits[i1].GetPosition().Mag();
								    double magB = cleanedHits[found[j1].data].GetPosition().Mag();

									if(magA > magB) isHitBFirst = true;
								}

		                		a_Link.first  = isHitBFirst ? found[j1].data : i1;
		                		a_Link.second = isHitBFirst ? i1 : found[j1].data;
		                	}
		                	/////
#if 0
							cout << "FlagJoint: " << FlagJoint << ", FlagNoIso: " << FlagNoIso << endl;
							
							cout << "layer1: " << cleanedHits[a_Link.first].GetLayer() << endl;
							cleanedHits[a_Link.first].GetPosition().Print();
							cout << "layer2: " << cleanedHits[a_Link.second].GetLayer() << endl;
							cleanedHits[a_Link.second].GetPosition().Print();

							cout << endl;
#endif

#if 0
							if(FlagEH) {
								continue;
								cout << "EH link: " << endl;
								cleanedHits[a_Link.first].GetPosition().Print();
								cleanedHits[a_Link.second].GetPosition().Print();
							}
#endif

#if 0
							cout << "create link: " << a_Link.first << ":" << a_Link.second << endl;
#endif

							if(a_Link.first==a_Link.second) continue;

							alliterlinks.push_back(a_Link);
							hitOutLinks[a_Link.first].push_back(a_Link.second);
						}
						else {
#if 0
							cout << "no addition link" << endl;
#endif
						}
					} // if(FlagEE || FlagHH)
				} //if(!isConnected)
		} // for each nearby hit
	} // for each hit


#if 1
	//This is more or less the same code with function Linkclean
	
	int MinAngleIndex = -10;
	float MinAngle = 1.e6; 
	float tmpOrder = 0;
	//float DirAngle = 0; 
	std::pair<int, int> SelectedPair; 

	// link collections (direction of going in) of each hit
	std::vector< std::vector<int> > linksIn;

	linksIn.resize(cleanedHits.size());

    for(int iLink = 0; iLink < alliterlinks.size(); ++iLink) {
		int hitIn  = alliterlinks[iLink].first;
	    int hitOut = alliterlinks[iLink].second;

        linksIn[hitOut].push_back(hitIn);
    }

	// maybe we don't need it. The backlink can be obtained from the link very easily.
	IterBackLinks.clear();

#if 0
	cout << "Cleaning links..." << endl;
#endif

	for(int i2=0; i2<cleanedHits.size(); ++i2)
	{
		TVector3 PosB = cleanedHits[i2].GetPosition();
		MinAngleIndex = -10;
		MinAngle = 1.e6;

		std::vector<int>& linksInOfHit = linksIn[i2];
#if 0
		cout << "hit " << i2 << ", linked hit size:  " << linksInOfHit.size() << endl;

		for(int iHit=0; iHit<linksInOfHit.size(); ++iHit) {
			cout << "--Link: " << linksInOfHit[iHit] << ":" << i2 << endl;
		}
#endif

		for(int j2=0; j2<linksInOfHit.size(); j2++)
		{
			int hitIndex = linksInOfHit[j2];
			TVector3 PosA = cleanedHits[hitIndex].GetPosition();
			
			TVector3 aLink = PosB - PosA;

			double tmpAngle = referenceDir[hitIndex].Angle(aLink);
			double tmpLen = aLink.Mag();

#if 1 // no free parameters
			float linkAngleCut = 0.;
			float smallAngleWeight;
			float linkLengthCutAtLargeAngle;
			float largeAngleWeight;
			float shortLinkCut;
			float shortLinkFactor;
			float longLinkCut;

			CaloHitType hitType = cleanedHits[i2].GetHitType();
			//bool isECALHit = hitType==ECALBarrel || hitType==ECALEndcap || hitType==ECALOther;

			//if(isECALHit) { 
			if(hitType<=ECALOther) {
			    int hitLayer = cleanedHits[i2].GetLayer();

				linkAngleCut                = linkAngleCutECAL ;
			    smallAngleWeight            = smallAngleWeightECAL;
			    linkLengthCutAtLargeAngle   = linkLengthCutAtLargeAngleECAL;
			    largeAngleWeight            = largeAngleWeightECAL;
			    shortLinkCut                = hitLayer>ecalFirstPartLayer ? shortLinkCutECAL1 : shortLinkCutECAL2;
			    shortLinkFactor             = shortLinkFactorECAL;
			    longLinkCut                 = longLinkCutECAL;
			} 
			else if(hitType<=HCALOther) {
				linkAngleCut                = linkAngleCutHCAL;
			    smallAngleWeight            = smallAngleWeightHCAL;
			    linkLengthCutAtLargeAngle   = linkLengthCutAtLargeAngleHCAL;
			    largeAngleWeight            = largeAngleWeightHCAL;
			    shortLinkCut                = shortLinkCutHCAL;
			    shortLinkFactor             = shortLinkFactorHCAL;
			    longLinkCut                 = longLinkCutHCAL;
			}
			else if(hitType==MUON) {
				// FIXME
				linkAngleCut                = linkAngleCutMUON;
			    smallAngleWeight            = smallAngleWeightMUON;
			    linkLengthCutAtLargeAngle   = linkLengthCutAtLargeAngleMUON;
			    largeAngleWeight            = largeAngleWeightMUON;
			    shortLinkCut                = shortLinkCutMUON;
			    shortLinkFactor             = shortLinkFactorMUON;
			    longLinkCut                 = longLinkCutMUON;
			}

			if(tmpAngle<linkAngleCut)
			   	tmpAngle = smallAngleWeight;
			else {
				if(tmpLen>linkLengthCutAtLargeAngle) continue;
				tmpAngle = largeAngleWeight;          
			}

			double lenCut = shortLinkCut;

			if(tmpLen<lenCut) tmpLen *= shortLinkFactor;
			tmpOrder = tmpAngle * tmpLen;

			double dist = (PosB - PosA).Mag();
			if(dist>longLinkCut) continue;     
#endif

			// FIXME
			if(tmpOrder < MinAngle)
			{
				MinAngleIndex = linksInOfHit[j2];
				MinAngle = tmpOrder;
			}
			else {
#if 0
				cout << "removed link: " << linksInOfHit[j2] << ":" << i2 << endl;

				if(cleanedHits[i2].GetSubD() * cleanedHits[linksInOfHit[j2]].GetSubD() == 2) {
				    cleanedHits[linksInOfHit[j2]].print();
					cleanedHits[i2].print();
				}
#endif
			}
		}

		//if(MinAngle>cut) continue;

		if(MinAngleIndex>-0.5)
		{
			SelectedPair.first = MinAngleIndex;
			SelectedPair.second = i2;

			if(SelectedPair.first == SelectedPair.second)
			{
#if 0
				cout << "SelectedPair.first: " << SelectedPair.first 
					 << ", SelectedPair.second: " << SelectedPair.second << endl;
#endif
				continue; 
			}

#if 0
			cout << "Kept link: " << SelectedPair.first << ":" << SelectedPair.second << endl;
#endif

			if(time == 1)
			{
				IterLinks_1.push_back(SelectedPair);

#if 0
				cout << "push1: SelectedPair.first: " << SelectedPair.first 
					 << ", SelectedPair.second: " << SelectedPair.second << endl;
#endif
			}
			else
			{
				IterLinks.push_back(SelectedPair);
				IterBackLinks.emplace(SelectedPair.second,SelectedPair.first);
#if 0
				cout << "push2: SelectedPair.first: " << SelectedPair.first 
					 << ", SelectedPair.second: " << SelectedPair.second << endl;
#endif
			}
		}
	}	
#endif

#if 0
	cout << "IterLinks: " << IterLinks.size() << endl;
#endif 
}

void Arbor3::BranchBuilding()
{
#if 0
	cout << "BranchBuilding" << endl;
#endif

    int NBranches = 0;

    std::map <int, int> HitBeginIndex;
    std::map <int, int> HitEndIndex;

    std::vector< std::vector<int> > InitBranchCollection;

#if 0
	cout << "cleaned hit size: " << cleanedHits.size() << endl;
#endif

    for(int i1=0; i1<cleanedHits.size(); ++i1)
    {
        HitBeginIndex[i1] = 0;
        HitEndIndex[i1] = 0;
#if 0
		cleanedHits[i1].print();
#endif
    }

    for(int j1=0; j1<IterLinks.size(); ++j1)
    {
        HitBeginIndex[ (IterLinks[j1].first) ]++;
        HitEndIndex[ (IterLinks[j1].second) ]++;
    }

	for(int i2=0; i2<cleanedHits.size(); ++i2)
	{
		if(HitEndIndex[i2]>1) {
			cout << "WARNING: INTERNAL LOOP with more than 1 link stopped at the same Hit" << endl;
		}

		// if this is a endpoint
		if(HitBeginIndex[i2]==0 && HitEndIndex[i2]==1)       
		{
			// FIXME:: need also to check the number of hit in the branch
			++NBranches;

			std::vector<int> branchHits;      //array of indexes 
			branchHits.push_back(i2);

            int hitIndex = i2;

	 		// length ?
			int LL = 0; 

			while(HitEndIndex[hitIndex]!=0 && LL<300)
			{
				auto iterlink_range = IterBackLinks.equal_range(hitIndex);

				// FIXME:: it's better to use a 'if' statement
				assert( std::distance(iterlink_range.first, iterlink_range.second)==1 );
				
				hitIndex = iterlink_range.first->second;
				branchHits.push_back(hitIndex);
				
				++LL;
			}
			// and what if LL is greater than 300 ?
	
			// TODO::
			// if the branch size if less than 5
			// put them to fragments

#if 0
			cout << "put branch hits: " << branchHits.size() << endl;
#endif
			InitBranchCollection.push_back( std::move(branchHits) );
		}
	}


	//
	KDTreeLinkerAlgo<unsigned,3> kdtree;

	std::array<float,3> minpos{ {0.0f,0.0f,0.0f} }, maxpos{ {0.0f,0.0f,0.0f} };

	typedef KDTreeNodeInfoT<unsigned,3> KDTreeNodeInfo;
	std::vector<KDTreeNodeInfo> nodes, found;
	bool needInitPosMinMax = true;

	// why float ?
	std::vector<float> BranchSize;

	for(int i3=0; i3<NBranches; ++i3)
	{
	    std::vector<int> currBranch = InitBranchCollection[i3];
		BranchSize.push_back( float(currBranch.size()) );
	}

	// need move ?
	std::vector<int> SortedBranchIndex = std::move( SortMeasure(BranchSize, 1) );

#if 0
	for(int i=0; i<SortedBranchIndex.size(); ++i) {
		cout << "sorted branchindex: " << SortedBranchIndex[i] << endl;
	}
#endif

	std::vector<float> cutBranchSize;

	std::vector<bool> touchedHitsMap(cleanedHits.size(), false);
    std::vector< std::vector<int> > TmpBranchCollection;

	std::unordered_map<int,int> branchToSeed;
	HitLinkMap seedToBranchesMap;

    std::vector< std::vector<int> > PrunedInitBranchCollection;
	PrunedInitBranchCollection.resize(InitBranchCollection.size());

	std::vector<bool> seedHitsMap(cleanedHits.size(),false);

	for(int i4=0; i4<NBranches; ++i4)
	{
	    std::vector<int> currBranch = InitBranchCollection[SortedBranchIndex[i4]];

		int currBranchSize = currBranch.size();

#if 0
		cout << "the sorted branch " << i4 << ": size " << currBranchSize << endl;
#endif
	
		std::vector<int> iterBranch;

		for(int j4 = 0; j4<currBranchSize; ++j4)
		{
			int currHit = currBranch[j4];

			if( !touchedHitsMap[currHit] )
			{
				iterBranch.push_back(currHit);
				touchedHitsMap[currHit] = true;
			}
		}
		
		// the seed of branch
		auto theseed = currBranch[currBranchSize - 1];

#if 0
		cout << "seed: " << theseed << endl;
		cleanedHits[theseed].print();
#endif

		// Oh~
		branchToSeed.emplace(SortedBranchIndex[i4],theseed);
		seedToBranchesMap.emplace(theseed,SortedBranchIndex[i4]); // map seed to branches

		if(!seedHitsMap[theseed])
		{
			seedHitsMap[theseed] = true;

			const auto& hit = cleanedHits[theseed].GetPosition();

			nodes.emplace_back(theseed, (float)hit.X(), (float)hit.Y(), (float)hit.Z());

			if(needInitPosMinMax) {
				needInitPosMinMax = false;
				minpos[0] = hit.X(); minpos[1] = hit.Y(); minpos[2] = hit.Z();
				maxpos[0] = hit.X(); maxpos[1] = hit.Y(); maxpos[2] = hit.Z();
			} else {
				minpos[0] = std::min((float)hit.X(),minpos[0]);
				minpos[1] = std::min((float)hit.Y(),minpos[1]);
				minpos[2] = std::min((float)hit.Z(),minpos[2]);
				maxpos[0] = std::max((float)hit.X(),maxpos[0]);
				maxpos[1] = std::max((float)hit.Y(),maxpos[1]);
				maxpos[2] = std::max((float)hit.Z(),maxpos[2]);
			}
		}

		TmpBranchCollection.push_back(iterBranch);
		cutBranchSize.push_back( float(iterBranch.size()) );
		PrunedInitBranchCollection[SortedBranchIndex[i4]] = std::move(iterBranch);
	}

	std::vector<int> SortedcutBranchIndex = std::move( SortMeasure(cutBranchSize, 1) );

#if 0
	for(int i=0; i<SortedcutBranchIndex.size(); ++i) {
		cout << "SortedcutBranchIndex: " << SortedcutBranchIndex[i] << endl;
	}
#endif

	KDTreeCube kdXYZCube(minpos[0],maxpos[0], minpos[1], maxpos[1], minpos[2], maxpos[2]);
	kdtree.build(nodes,kdXYZCube);
	nodes.clear();


	// QuickUnion:
	// what's QuickUnion
	QuickUnion qu(NBranches);

	for(int i7=0; i7<NBranches; ++i7)
	{
		auto SeedIndex_A = branchToSeed[i7];

		// all the branches belong to the same seed
		auto shared_branches = seedToBranchesMap.equal_range(SeedIndex_A);

		for( auto itr = shared_branches.first; itr != shared_branches.second; ++itr ) {
			const auto foundSortedIdx = itr->second;

			if( foundSortedIdx <= (unsigned)i7 ) continue;
#if 0
		cout << "shared_branches indices: " << foundSortedIdx << endl;
#endif

			if( !qu.connected(i7,foundSortedIdx) ) {
				qu.unite(i7,foundSortedIdx);
			}
		}
	}

	Trees.clear();
	std::unordered_map<unsigned, branch> merged_branches(qu.count());
	Trees.reserve(qu.count());

	cout << "branch: " << qu.count() << endl;

	for( unsigned i = 0; i < (unsigned)NBranches; ++i ) {
		unsigned root = qu.find(i);
		const auto& branch = PrunedInitBranchCollection[i];

		auto& merged_branch = merged_branches[root];
		merged_branch.insert(merged_branch.end(), branch.begin(), branch.end());
	}

	for( auto& final_branch : merged_branches ) {
		Trees.push_back(std::move(final_branch.second));
	}
}

// copy from the original Arbor class
void Arbor3::IsoHitsClassification()
{
	IsoHitsIndex.clear();

	int NLinks = IterLinks.size();
	int NHits = cleanedHits.size();

    int BeginIndex[NHits];
    int EndIndex[NHits];

    for(int i0 = 0; i0 < NHits; i0++) {
        BeginIndex[i0] = 0;
        EndIndex[i0] = 0;
		//cleanedHits[i0].print();
    }

    for(int j0 = 0; j0 < NLinks; j0++) {
        BeginIndex[ (IterLinks[j0].first) ] ++;
        EndIndex[ (IterLinks[j0].second) ] ++;
#if 0
		cout << "Link: " << IterLinks[j0].first << " ---- " << IterLinks[j0].second << endl;
		cleanedHits[IterLinks[j0].first].print();
		cleanedHits[IterLinks[j0].second].print();
		cout << endl;
#endif
    }

    for(int i1 = 0; i1 < NHits; i1++) {
	    if(BeginIndex[i1] == 0 && EndIndex[i1] == 0) {
	    	IsoHitsIndex.push_back(i1);
			cleanedHits[i1].SetIsoHit();
			//cout << "iso index: " << i1 << endl;
	    }
    }

#if 1
	streamlog_out(MESSAGE)<<"IsoHits: "<<IsoHitsIndex.size()<<endl; 
#endif
}

#if 0
double HitBranchDistance(const ArborHit3& hit, const vector<int>& branch)
{
	TVector3 hitPos = hit.GetPosition();

	const double HITSDISTANCELIMIT = 300; 
	double dist = 1.e10;

	for(int iHit=0; iHit<branch.size(); ++iHit) {
		ArborHit3& branchHit = cleanedHits[branch[iHit]];
	
		TVector3 branchHitPos = branchHit.GetPosition();

		double hitsDist = (hitPos-branchHitPos).Mag();

		if(hitsDist>HITSDISTANCELIMIT) {
			dist = -1.; // convention: large distance between hit and branch
		}
	}
}
#endif

void Arbor3::BranchRebuiding()
{
//  Branch absorbs the isolated hits.
    cout << "BranchRebuiding---> current branch size: " << Trees.size() << endl;

	//////////////////////////////////////////////////////////////////////////////
	KDTreeLinkerAlgo<unsigned,3> kdtree;
    typedef KDTreeNodeInfoT<unsigned,3> KDTreeNodeInfo;
	std::array<float,3> minpos{ {0.0f,0.0f,0.0f} }, maxpos{ {0.0f,0.0f,0.0f} };

	// found -> nearByHitIndex;
    std::vector<KDTreeNodeInfo> nodes, found;

	for(int i0=0; i0<cleanedHits.size(); ++i0)
    { 
		const auto& hit = cleanedHits[i0].GetPosition();
        nodes.emplace_back(i0,(float)hit.X(),(float)hit.Y(),(float)hit.Z());

        if(i0==0)
        {
		   	minpos[0] = hit.X(); minpos[1] = hit.Y(); minpos[2] = hit.Z();
            maxpos[0] = hit.X(); maxpos[1] = hit.Y(); maxpos[2] = hit.Z();
        }
        else
        { 
			minpos[0] = std::min((float)hit.X(),minpos[0]);
            minpos[1] = std::min((float)hit.Y(),minpos[1]);
            minpos[2] = std::min((float)hit.Z(),minpos[2]);
            maxpos[0] = std::max((float)hit.X(),maxpos[0]);
            maxpos[1] = std::max((float)hit.Y(),maxpos[1]);
            maxpos[2] = std::max((float)hit.Z(),maxpos[2]);
        }
    }

    KDTreeCube kdXYZCube(minpos[0],maxpos[0], minpos[1],maxpos[1], minpos[2],maxpos[2]);

    kdtree.build(nodes,kdXYZCube);
	//////////////////////////////////////////////////////////////////////////////
    
	nodes.clear();
	linkcoll newLinks; 
    

	// loop each branch
	//
	//
	//
	//

	for(int iIsoHit=0; iIsoHit<IsoHitsIndex.size(); ++iIsoHit) {
		found.clear();

	    int i0 = IsoHitsIndex[iIsoHit];
		TVector3 PosA = cleanedHits[i0].GetPosition();

		// the search range
		float side = 0.;	
		
		CaloHitType hitType = cleanedHits[i0].GetHitType();

		// FIXME
		if(hitType<=ECALOther) {
			side = 200.;
		} 
		else if(hitType<=HCALOther) {
			side = 300.;	
		}
		else if(hitType==MUON) {
		    side = 500.;
		}

		float xmin(PosA.X() - side);
        float xmax(PosA.X() + side);

		float ymin(PosA.Y() - side);
        float ymax(PosA.Y() + side); 
		
		float zmin(PosA.Z() - side);
        float zmax(PosA.Z() + side); 

        KDTreeCube searchcube(xmin, xmax, ymin, ymax, zmin, zmax);

        kdtree.search(searchcube,found);

#if 0
		cout << endl;
		cleanedHits[i0].print();
		cout << "found hits by k-d tree in building init. link: " << found.size() << endl;
#endif

		cout << "Iso hit: " << iIsoHit << "hit index: " << i0 << endl;
		cleanedHits[i0].print();
		cout << "---------> the close hits: " << endl;

		double minDistance = 1.e6;
		int minDistHitIndex = -1;

		for(unsigned int j0 = 0; j0 < found.size(); ++j0) {
			int indexOfNearHit = found[j0].data;
			if(indexOfNearHit!=i0) cleanedHits[indexOfNearHit].print();

			double dist = (cleanedHits[indexOfNearHit].GetPosition() - cleanedHits[i0].GetPosition()).Mag();
			if(dist<minDistance) {
				minDistance = dist;
				minDistHitIndex = indexOfNearHit;
			}
		}

		const double DISTLIMIT = 30;
		if(minDistance<DISTLIMIT && cleanedHits[minDistHitIndex].IsIsoHit()==false) {
			// TODO
			cout << "" << endl;
        
			std::pair<int, int> bestLink;
		    //bestLink.first  = bestHitIndex;
            //bestLink.second = iHit;

			newLinks.push_back(bestLink);


			// link the two hits
		}

		cout << endl;
	}

#if 0
   vector<TVector3> branchCenter;

   for(int iBranch=0; iBranch<Trees.size(); ++iBranch) {
	   vector<int>& branch = Trees[iBranch]; 

	   TVector3 branchPos(0., 0., 0.);

	   double posWeight = 1./branch.size();

	   for(int iHit=0; iHit<branch.size(); ++iHit) {
		   ArborHit3& branchHit = cleanedHits[branch[iHit]];
		   TVector3 branchHitPos = branchHit.GetPosition();

		   branchPos += posWeight * branchHitPos;
	   }

	   branchCenter.push_back(branchPos);
	   branchPos.Print();
   }
#endif

#if 0
   for(int iIsoHit=0; iIsoHit<IsoHitsIndex.size(); ++iIsoHit) { 
	   int isoHitIndex = IsoHitsIndex[iIsoHit];

	   ArborHit3& hit = cleanedHits[isoHitIndex]; 
	   TVector3 hitPos = hit.GetPosition();

	   // loop all the branches
	   for(int iBranch=0; iBranch<Trees.size(); ++iBranch) {
		   vector<int>& branch = Trees[iBranch]; 

		   TVector3 bc = branchCenter[iBranch];
		   
		   double hitBranchCenterDist = (hitPos - bc).Mag();
		   if()hitBranchCenterDist>
		   
	   }
   }
#endif
}

void Arbor3::Clustering( std::vector<ArborHit3> inputHits )
{
	Clear();

	HitsCleaning(inputHits);

	PreClustering();

	BuildInitLink();
	LinkClean(Links);

	LinkIteration(1);
	LinkIteration(2);

	BranchBuilding();	
	IsoHitsClassification();
	cout << "iso hits size: " << IsoHitsIndex.size() << endl;

}
