#ifndef _ARBORGEOMETRY_H_
#define _ARBORGEOMETRY_H_

#include <TVector3.h>
#include <TMath.h>

#include "ArborDef.h"
#include "ArborCaloCluster.h"

// TODO: this class maybe extended to be the ArborCaloHit 
// (or something like that) container, and this class can 
// even has member that is the object of layer class.
// And also the consider the implementation of geometry for
// tracking in calorimeter.
//
// Now this is just a simple geometry implementation to
// determine whether a hit is close to the boundary of a module:
//
//                 Up
//         -----------------              |
//        -                 -             |
// Left  --------L_phi--------   Right   L_R
//      -                     -           |
//     -------------------------          |
//                Down 
//
// and the angle between left and bottom side is called alpha

class EcalBarrel;

// FIXME
const int STAVENUMBER  = 8;
const int MODULENUMBER = 5;

class EcalBarrelModule
{
public:
	// These parameters are from the geometry of single module in ILD_o1_v05
	EcalBarrelModule(int stave, int module, TVector3 center = TVector3(0,0,0));

	inline void SetCenter   (TVector3 center) { m_Center    = center; }
	inline void SetLengthR  (double len_r)    { m_LengthR   = len_r;  }
	inline void SetLengthZ  (double len_z)    { m_LengthZ   = len_z;  }
	inline void SetLengthPhi(double len_p)    { m_LengthPhi = len_p;  }

	std::vector< std::pair<ModuleSide, double> > getNearSides(TVector3 hit, double nearCut = 0.);

	//std::pair<EcalBarrelModule&, ModuleSide> getNearModuleSide(ModuleSide side);
    std::pair<EcalBarrelModule*, ModuleSide> getNearModuleSide(
		                                 //std::vector< std::vector<EcalBarrelModule> >& ecalModules, 
										 ModuleSide         side, 
										 bool&              isExsiting);

	inline int GetModule() { return m_module; }
	inline int GetStave () { return m_stave;  }

	void ClearNearSideHits();

	void PutHit2Side(ARBORCALOHIT hit, ModuleSide side);
	std::vector<ARBORCALOHIT>& GetHitNearSide(ModuleSide side);

private:
	TVector3 m_Center;

	double   m_LengthR;
	double   m_LengthZ;
	double   m_LengthPhi;
	double   m_alpha;
	double   m_nearCut;

	int      m_stave;
	int      m_module;

	double   m_sides[NSIDES];  // the position array of sides to the center
	std::vector<ARBORCALOHIT> nearSideHits[NSIDES];
};

#endif
