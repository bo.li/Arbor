#ifndef _ECALBARREL_H_
#define _ECALBARREL_H_

#include "ArborGeometry.h"

class EcalBarrel
{
public:
	EcalBarrelModule& GetModule(int stave, int module);

	static EcalBarrel* GetEcalBarrel();

	static void Clear();

private:
	EcalBarrel(){};

	std::vector< std::vector<EcalBarrelModule> > m_ecalBarrel;

	static EcalBarrel* m_instance;
};

#endif
