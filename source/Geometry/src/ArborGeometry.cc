#include <iostream>

#include "ArborGeometry.h"
#include "EcalBarrel.h"

// The parameters are ralated to the ECAL geometry of ILD_o1_v05
EcalBarrelModule::EcalBarrelModule(int stave, int module, TVector3 center)
	:m_Center(center), 
	 m_LengthR(185.), 
	 m_LengthZ(940.), 
	 m_LengthPhi(1603.3),
	 m_alpha(0.78567),
	 m_nearCut(30.),
	 m_stave(stave),
	 m_module(module)
{ 
	m_sides[RIGHT]  =  m_LengthPhi/2. * sin(m_alpha);
	m_sides[LEFT]   =  m_LengthPhi/2. * sin(m_alpha);
	m_sides[UP]     =  m_LengthR/2.;
	m_sides[DOWN]   =  m_LengthR/2.;
	m_sides[TOP]    =  m_LengthZ/2.;
	m_sides[BOTTOM] =  m_LengthZ/2.;

	std::cout << "stave: " << m_stave << ", module: " <<  m_module << std::endl;
}

// get one side of the near module
std::pair<EcalBarrelModule*, ModuleSide> EcalBarrelModule::getNearModuleSide(
										 ModuleSide         side, 
										 bool&              isExsiting)
{
	int nearModule = m_module; // index of ecal module in ILD, from 1 to 5
	int nearStave  = m_stave;

	EcalBarrelModule* module = 0;
	ModuleSide nearSide = NSIDES;
	isExsiting = true;

	switch(side) {
		case DOWN:
			--nearStave;
			if(nearStave<0) nearStave += STAVENUMBER;
			nearSide = LEFT;
			break;
		case LEFT:
			++nearStave;
			if(nearStave>=STAVENUMBER) nearStave -= STAVENUMBER;
			nearSide = DOWN;
		    break;
		case TOP:
			++nearModule;
			nearSide = BOTTOM;
			if(nearModule>MODULENUMBER) isExsiting = false;
			break;
		case BOTTOM:
			--nearModule;
			nearSide = TOP;
			if(nearModule<=0) isExsiting = false;
			break;
		default:
			isExsiting =  false;
	}

	// check nearModule: nearModule or nearModule-1 ?
	// since the module index of this array starts from 0...
	// module = &ecalModules[nearStave][nearModule-1];
	module = &(EcalBarrel::GetEcalBarrel()->GetModule(nearStave, nearModule-1));

	return std::pair<EcalBarrelModule*, ModuleSide>(module, nearSide);
}

std::vector< std::pair<ModuleSide, double> > EcalBarrelModule::getNearSides(TVector3 hit, double nearCut) 
{
	//std::cout << "getNearSides..."  << std::endl;
	double distanceCut = nearCut > 1.e-2 ? nearCut : m_nearCut;

	std::vector< std::pair<ModuleSide, double> > nearSideVec;

	TVector3 localHitPos = hit - m_Center;  // the hit position in the local coordinate system of module, center as the origin
	localHitPos.RotateZ(-m_stave*TMath::Pi()/4);

	ModuleSide side;
	
	side = localHitPos.X() > 0. ? RIGHT : LEFT;
	TVector3 projVec(1,0,0); // this is a local vector in the module coordinate system

	if(side==RIGHT) {
		projVec.RotateZ(TMath::Pi()/2. - m_alpha);
	} else {
		projVec.RotateZ(TMath::Pi()/2. + m_alpha);
	}

	double distanceX = m_sides[side] - localHitPos * projVec;
#if 1
	std::cout << "module center: " << m_Center.X() << ", " << m_Center.Y() << ", " << m_Center.Z() << std::endl; 
	std::cout << "local hit : " << localHitPos.X() << ", " << localHitPos.Y() << ", " << localHitPos.Z() << std::endl; 
	std::cout << "side dist: " << m_sides[side] << ", proj: "  << localHitPos * projVec << std::endl;
	std::cout << "side: " << side << ", dist: " << distanceX << std::endl;
#endif
	if(distanceX<distanceCut) nearSideVec.emplace_back( std::pair<ModuleSide, double>(side, fabs(distanceX)) );

	////////////////////
	side = localHitPos.Y() > 0. ? UP : DOWN;
	double distanceY = m_sides[side] - fabs(localHitPos.Y());
	if(side==DOWN) {
		bool isInProperXRegion = localHitPos.X() > 600.; // only this region is close to the left side of another module 
		if(distanceY<distanceCut&&isInProperXRegion) 
			nearSideVec.emplace_back( std::pair<ModuleSide, double>(side, fabs(distanceY)) );
	} else { 
		if(distanceY<distanceCut) nearSideVec.emplace_back( std::pair<ModuleSide, double>(side, fabs(distanceY)) );
	}
#if 1
	std::cout << "side: " << side << ", dist: " << distanceY << std::endl;
#endif

	//////////////
	side = localHitPos.Z() > 0. ? TOP : BOTTOM;
	double distanceZ = m_sides[side] - fabs(localHitPos.Z());
#if 1
	std::cout << "side: " << side << ", dist: " << distanceZ << std::endl;
#endif
	if(distanceZ<distanceCut) nearSideVec.emplace_back( std::pair<ModuleSide, double>(side, fabs(distanceZ)) );


	// here is a convention of return value if the hit is out of the module: side = NSIDES
	if(distanceX<0.||distanceY<0.||distanceZ<0.) {
		nearSideVec.clear();
		side = NSIDES;
		nearSideVec.emplace_back( std::pair<ModuleSide, double>(side, -1.) );
	}

	return nearSideVec;
}

void EcalBarrelModule::PutHit2Side(ARBORCALOHIT hit, ModuleSide side) 
{
	nearSideHits[side].push_back(hit);
}

std::vector<ARBORCALOHIT>& EcalBarrelModule::GetHitNearSide(ModuleSide side)
{
	return nearSideHits[side];
}

void EcalBarrelModule::ClearNearSideHits()
{
	for(int i=0; i<NSIDES; ++i) { 
		nearSideHits[i].clear();
	}
}
