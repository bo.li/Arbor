#include "EcalBarrel.h"

EcalBarrel* EcalBarrel::m_instance = 0;

EcalBarrelModule& EcalBarrel::GetModule(int stave, int module)
{
   	return m_ecalBarrel[stave][module];
}

EcalBarrel* EcalBarrel::GetEcalBarrel()
{
	if(m_instance==NULL) {
		m_instance = new EcalBarrel();
		std::vector< std::vector<EcalBarrelModule> >& ecalBarrel =
			m_instance->m_ecalBarrel;

		// initialization of stave and module
		for(int iStave=0; iStave<8; ++iStave) {
			ecalBarrel.emplace_back(std::vector<EcalBarrelModule>());
	
			for(int iModule=0; iModule<5; ++iModule) {
				//TVector3 center(130.8, 1935.5, -1880.); // stave 0, module 0
				TVector3 center(130.8, 1935.5, -1880.+iModule*470*2); 
				center.RotateZ(iStave*TMath::Pi()/4);
				center.Print();
				ecalBarrel[iStave].emplace_back(EcalBarrelModule(iStave, iModule+1, center));// module index 
			}
		}
	}

	return m_instance;
}

void EcalBarrel::Clear()
{ 
	delete m_instance;
}
