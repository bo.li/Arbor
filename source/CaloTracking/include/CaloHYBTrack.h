#ifndef __CALOHYBTRACK__
#define __CALOHYBTRACK__
//*************************************************************************
//* =================
//*  CaloHYBTrack Class
//* =================
//*
//* (Description)
//*   Hybrid track class for Kalman filter
//* (Requires)
//*     TKalTrack
//* (Provides)
//*     class CaloHYBTrack
//* (Update Recored)
//*   2005/08/26  K.Fujii       Original version.
//*
//*************************************************************************
                                                                                
#include "TKalTrack.h"         // from KalTrackLib
#include "TAttDrawable.h"      // from Utils

//_________________________________________________________________________
//  ------------------------------
//   CaloHYBTrack: Kalman Track class
//  ------------------------------
                                                                                
class CaloHYBTrack : public TKalTrack, public TAttDrawable {
public:
   CaloHYBTrack(Int_t n = 1) : TKalTrack(n) {}
   ~CaloHYBTrack() {}

   using TAttDrawable::Draw;
   virtual void Draw(Int_t color, const Char_t *opt);

};

#endif
