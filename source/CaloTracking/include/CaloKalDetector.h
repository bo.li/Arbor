#ifndef __CALODETECTOR__
#define __CALODETECTOR__

#include "CaloVKalDetector.h"
#include "TMath.h"

#include "marlin/Global.h"
#include "gearxml/GearXML.h"
#include "gear/LayerLayout.h"
#include "gear/CalorimeterParameters.h"
#include "gear/GearMgr.h"


class CaloKalDetector : public CaloVKalDetector {
  
 public:
  CaloKalDetector(gear::GearMgr* gearMgr, Int_t m = 100);
  ~CaloKalDetector();
    
  static const Int_t _nLayer = 6;
};

#endif
