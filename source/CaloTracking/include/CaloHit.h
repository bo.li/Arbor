#ifndef __CALOHIT__
#define __CALOHIT__

#include "KalTrackDim.h"
#include "TVTrackHit.h"
#include "CaloMeasLayer.h"

//

class CaloHit : public TVTrackHit {
public:
   CaloHit(Int_t m = kMdim);

   CaloHit(const CaloMeasLayer &ms,
                  Double_t       *x,
                  Double_t       *dx,
            const TVector3       &xx,
                  Double_t        b,
                  Int_t           m = kMdim);

   virtual ~CaloHit();

   virtual TKalMatrix XvToMv (const TVector3 &xv, Double_t t0) const;

   virtual void       DebugPrint(Option_t *opt = "")           const;
        
   inline  const TVector3 GetExactX() const { return fXX;     }

private:
   TVector3 fXX;   // exact hit position

};

#endif
