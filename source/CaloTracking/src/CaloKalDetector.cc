#include "CaloKalDetector.h"
#include "CaloMeasLayer.h"
#include "CaloHit.h"
#include "TRandom.h"
#include "TMath.h"
#include <sstream>

using namespace gear;

//ClassImp(CaloKalDetector)

CaloKalDetector::CaloKalDetector(GearMgr* gearMgr, Int_t m)
                : CaloVKalDetector(m)
{
   Double_t A, Z, density, radlen;
   A       = 14.00674 * 0.7 + 15.9994 * 0.3;
   Z       = 7.3;
   density = 1.205e-3;
   radlen  = 3.42e4;
   TMaterial &air = *new TMaterial("CALOAir", "", A, Z, density, radlen, 0.);

   A       = 28.0855;
   Z       = 14.;
   density = 2.33;   
   radlen  = 9.36;
   TMaterial &si = *new TMaterial("CALOSi", "", A, Z, density, radlen, 0.);
   
   //Add a material: Tungsten
   A	   = 183.84;
   Z	   = 74;
   density = 19.3;
   radlen =  0.3504; 
   TMaterial &w = *new TMaterial("CALOW", "", A, Z, density, radlen, 0.);

   Bool_t active = CaloMeasLayer::kActive;
   Bool_t dummy  = CaloMeasLayer::kDummy;
   Bool_t activeness[2] = { active, dummy};


   if(gearMgr==NULL) {
	    //FIXME:: hard coded building of layers for ECAL barrel
        const Double_t coff = .15;
        static const Double_t sigmaxi   = 1 * coff;
        static const Double_t sigmazeta = 1 * coff;
       
        const double LAYERSTARTY = 1850.450; //mm
       
        // Two step values
        const double LAYERDELTAY11 = 7.4;     //mm
        const double LAYERDELTAY12 = 3.1;     //mm
       
        // Delta_y changes after layer 19
        const double LAYERDELTAY21 = 5.2;     //mm
        const double LAYERDELTAY22 = 9.5;     //mm
       
        double layerY = LAYERSTARTY;
       
        for(int l=0; l<29; ++l) {
       	 //Add(new CaloMeasLayer(air, si, xc, normal, rmin + 2*ladder*eps, plyhwidth - sximin, 
       	  //		              2*hlength, (plyhwidth + sximin)/2, sigmaxi, sigmazeta, inner,ss.str().data()));       
       	
       	 if(l>0&&l<=19) {
       	     layerY += l%2==1 ? LAYERDELTAY11 : LAYERDELTAY12;
       	 } 
       	 else if (l>19) {
       	     layerY += l%2==0 ? LAYERDELTAY21 : LAYERDELTAY22;
       	 }
       	     
            TVector3 normal(0, 1, 0);
            TVector3 xc(0, layerY, 0);
       
       	 Add(new CaloMeasLayer(w, w, xc, normal, xc.Perp(), 5000., 5000., 0.,
       	  		              sigmaxi, sigmazeta));       
        }
   }
   else {
       std::cout << "gearMgr: " << gearMgr << std::endl;
       const CalorimeterParameters& ecalBarrel = gearMgr->getEcalBarrelParameters();
       const LayerLayout& ecalBarrelLayerLayout = ecalBarrel.getLayerLayout();
       const std::vector<double_t>& ecalBarrelExtent = ecalBarrel.getExtent();
     
       int nLayer = ecalBarrelLayerLayout.getNLayers();
       int nStave = ecalBarrel.getSymmetryOrder();
     
       std::cout << "stave ---: " << nStave << std::endl;
       
       Double_t inner_R = ecalBarrelExtent[0];
       Double_t outer_z = ecalBarrelExtent[3];
     
       // point resolution
       static const Double_t sigmaxi   = 0.15;
       static const Double_t sigmazeta = 0.15;
       //static const Double_t sigmaxi   = 1.5;
       //static const Double_t sigmazeta = 1.5;
       
       Double_t yDepth = inner_R;
       Double_t xiWidth = 2 * yDepth * tan(TMath::Pi()/nStave);
       Double_t zetaWidth = 2 * outer_z; // full length
     
       for(int iLayer=0; iLayer<nLayer; ++iLayer) {
          for (int iStave=0; iStave<nStave; ++iStave) {
       		 Double_t angle=2*TMath::Pi()*iStave/nStave;
       		 TVector3 normal(sin(angle), cos(angle), 0);
       		 TVector3 xc(yDepth*sin(angle), yDepth*cos(angle), 0);
       		
       		 Add(new CaloMeasLayer(w, w, xc, normal, xc.Perp(), xiWidth, zetaWidth, 0., sigmaxi, sigmazeta));
          }
     
	      yDepth += ecalBarrelLayerLayout.getThickness(iLayer);
	      xiWidth = 2 * yDepth * tan(TMath::Pi()/nStave);
	   }
   }

   SetOwner();			
}

CaloKalDetector::~CaloKalDetector()
{
}
