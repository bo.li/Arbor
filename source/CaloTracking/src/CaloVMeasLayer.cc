//*************************************************************************
//* ===================
//*  CaloVMeasLayer Class
//* ===================
//*
//* (Description)
//*   Sample measurement layer class used by TVTrackHit.
//* (Requires)
//* (Provides)
//*     class CaloVMeasLayer
//* (Update Recored)
//*   2003/09/30  Y.Nakashima       Original version.
//*
//*************************************************************************
//

#include "CaloVMeasLayer.h"

Bool_t   CaloVMeasLayer::kActive = kTRUE;
Bool_t   CaloVMeasLayer::kDummy = kFALSE;

                                                                                
CaloVMeasLayer::CaloVMeasLayer(TMaterial &min,
                           TMaterial &mout,
                           Bool_t     isactive,
                     const Char_t    *name)  
            : TVMeasLayer(min, mout, isactive),
	      fName(name),
	      fNodePtr(0)
{
}

CaloVMeasLayer::~CaloVMeasLayer()
{
}
