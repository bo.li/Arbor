#include "CaloHit.h"
#include "CaloMeasLayer.h"
#include "TMath.h"

#include <iostream>
#include <iomanip>

using namespace std;


//_____________________________________________________________________
//  ----------------------------------
//  Ctors and Dtor
//  ----------------------------------

CaloHit::CaloHit(Int_t m)
        : TVTrackHit(m)
{
}
      
CaloHit::CaloHit(const CaloMeasLayer &ms,
                         Double_t       *x,
                         Double_t       *dx, 
                   const TVector3       &xx,
                         Double_t        b,
                         Int_t           m)
        : TVTrackHit(ms, x, dx, b, m),
          fXX(xx)
{
}

CaloHit::~CaloHit()
{
}

//_____________________________________________________________________
//  ----------------------------------
//  Implementation of public methods  
//  ----------------------------------

TKalMatrix CaloHit::XvToMv(const TVector3 &xv, Double_t /*t0*/) const
{
   return dynamic_cast<const CaloMeasLayer &>(GetMeasLayer()).XvToMv(xv);
}

void CaloHit::DebugPrint(Option_t *) const
{
   cerr << "------------------- Site Info -------------------------" << endl;

   for (Int_t i=0; i<GetDimension(); i++) {
      Double_t x  = (*this)(i,0);
      Double_t dx = (*this)(i,1);
      cerr << " x[" << i << "] = " << setw(8) << setprecision(5) << x
           << "    "
           << "dx[" << i << "] = " << setw(6) << setprecision(2) << dx
           << setprecision(7)
           << resetiosflags(ios::showpoint)
           << endl;
   }
   cerr << "-------------------------------------------------------" << endl;
}
