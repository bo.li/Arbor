#include "CaloVKalDetector.h"
#include "CaloVMeasLayer.h"
#include "TVKalDetector.h"
#include "TTUBE.h"
#include "TNode.h"
#include "TRotMatrix.h"
#include "TVirtualPad.h"

Double_t CaloVKalDetector::fgBfield  = 3.5;
TNode   *CaloVKalDetector::fgNodePtr = 0;


CaloVKalDetector::CaloVKalDetector(Int_t m)
             : TVKalDetector(m),
               fIsPowerOn(kTRUE)
{
}

CaloVKalDetector::~CaloVKalDetector()
{
}

TNode *CaloVKalDetector::GetNodePtr()
{
   if (!fgNodePtr) {
      new TRotMatrix("rotm","rotm", 10.,80.,10.,80.,10.,80.);

      TTUBE* tube = new TTUBE("Det","Det","void",70.,70.,100.);
	  tube->SetLineColor(kWhite);

      fgNodePtr = new TNode("World","World","Det",0.,0.,0.,"rotm");
   }
   return fgNodePtr;
}

void CaloVKalDetector::Draw(Int_t color, const Char_t *opt)
{
   if (!gPad) return;
   TNode *nodep = GetNodePtr();
   nodep->cd();
   TIter next(this);
   TObject *objp;
   while ((objp = next())) {
      TAttDrawable *dp = dynamic_cast<TAttDrawable *>(objp);
      if (dp) dp->Draw(color, opt); 
   }
   nodep->Draw("pad same");
}
