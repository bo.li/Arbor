#include <cmath>

#include "ArborCaloHit.h"

// The encode string for ILD_o1_v05
std::string ArborCaloHit::hitString[CALOHITTYPES][STRINGTYPES] = {
	{"M:3,S-1:3,I:9,J:9,K-1:6", "",    "",   ""  }, 
	{"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	{"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	{"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	{"M:3,S-1:4,I:9,J:9,K-1:7", "K-1", "M", "S-1"},
	{"M:3,S-1:4,I:9,J:9,K-1:7", "K-1", "M", "S-1"},
	{"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	{"I:10,J:10,K:10,S-1:2"   , "K"  , "" , "S-1"},
	{"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	{"S-1:4,M:3,K-1:6,I:16,GRZone:3,J:32:16", "K-1", "M", "S-1"}
};

// If we have a decoder for each ArborCaloHit in the constructor,
// that will spend a relatively significant time on CPU.
// By this static array of decoder we can save some CPU time.
// But it seems that we have to initialize the array like this...
CellIDDecoder<CalorimeterHit> ArborCaloHit::m_decoders[CALOHITTYPES] = 
{
	CellIDDecoder<CalorimeterHit>(hitString[UnknownType][DecoderString]), // This is invalid, keep it for convenience of accessing array
	CellIDDecoder<CalorimeterHit>(hitString[ECALBarrel ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[ECALEndcap ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[ECALOther  ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[HCALBarrel ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[HCALEndcap ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[HCALOther  ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[LCAL       ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[LHCAL      ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[MUON       ][DecoderString]),
};

ArborCaloHit::ArborCaloHit(CalorimeterHit* hit, CaloHitType hitType)
	         : m_Layer(-1), m_Module(-1), m_Stave(-1), 
			   m_Type(hitType), m_LCIOCaloHit(hit),
			   m_IsClustered(false)
{ 
	if(hitType==UnknownType) return;

	std::string& cellIDLayerString  = hitString[hitType][LayerString];
	std::string& cellIDModuleString = hitString[hitType][ModuleString];
	std::string& cellIDStaveString  = hitString[hitType][StaveString];

	CellIDDecoder<CalorimeterHit>& decoder = m_decoders[hitType];

	if(cellIDLayerString.length() ) { m_Layer  = decoder(m_LCIOCaloHit)[cellIDLayerString];  }
	if(cellIDModuleString.length()) { m_Module = decoder(m_LCIOCaloHit)[cellIDModuleString]; }
	if(cellIDStaveString.length() ) { m_Stave  = decoder(m_LCIOCaloHit)[cellIDStaveString];  }

}

double ArborCaloHit::getDistanceWith(ArborCaloHit& hit)
{
	const float* posA = GetPosition();
	const float* posB = hit.GetPosition();

	return sqrt( (posA[0] - posB[0]) * (posA[0] - posB[0]) + 
	             (posA[1] - posB[1]) * (posA[1] - posB[1]) +
	             (posA[2] - posB[2]) * (posA[2] - posB[2]) );
}

void ArborCaloHit::print()
{
   	std::cout << "Stave: "  << GetStave()       << ", " 
		      << "Module: " << GetModule()      << ", "
			  << "Layer: "  << GetLayer()       << ", "
			  << "X: "      << GetPosition()[0] << ", "
			  << "Y: "      << GetPosition()[1] << ", "
			  << "Z: "      << GetPosition()[2] << ", "
		      << "Type: "   << GetHitType()     << ", "
			  << "E: "      << GetEnergy()      
	          << std::endl;
}

void ArborCaloHit::printHitVector(std::vector<ArborCaloHit>& hitVec)
{
	auto it = hitVec.begin();

	while(it != hitVec.end()) {
		ArborCaloHit& caloHit = *it;
		caloHit.print();
		++it;
	}
}
