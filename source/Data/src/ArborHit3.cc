#include <cmath>

#include "ArborHit3.h"

#include <EVENT/SimCalorimeterHit.h>

std::string ArborHit3::hitString[2][CALOHITTYPES][STRINGTYPES] = { 
	{
       // The encode string for ILD_o1_v05
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "",    "",   ""  }, 
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"M:3,S-1:4,I:9,J:9,K-1:7", "K-1", "M", "S-1"},
	   {"M:3,S-1:4,I:9,J:9,K-1:7", "K-1", "M", "S-1"},
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"I:10,J:10,K:10,S-1:2"   , "K"  , "" , "S-1"},
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"S-1:4,M:3,K-1:6,I:16,GRZone:3,J:32:16", "K-1", "M", "S-1"}
	},
	{
	   // The encode string for ILD_o2_v05
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "",    "",   ""  }, 
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"M:3,S-1:4,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"M:3,S-1:4,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"I:10,J:10,K:10,S-1:2"   , "K"  , "" , "S-1"},
	   {"M:3,S-1:3,I:9,J:9,K-1:6", "K-1", "M", "S-1"},
	   {"S-1:4,M:3,K-1:6,I:16,GRZone:3,J:32:16", "K-1", "M", "S-1"}
	}
};

// If we have a decoder for each ArborHit3 in the constructor,
// that will spend a relatively significant time on CPU.
// By this static array of decoder we can save some CPU time.
// But it seems that we have to initialize the array like this...
CellIDDecoder<CalorimeterHit> ArborHit3::m_decoders[CALOHITTYPES] = 
{
	CellIDDecoder<CalorimeterHit>(hitString[1][UnknownType][DecoderString]), // This is invalid, keep it for convenience of accessing array
	CellIDDecoder<CalorimeterHit>(hitString[1][ECALBarrel ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[1][ECALEndcap ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[1][ECALOther  ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[1][HCALBarrel ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[1][HCALEndcap ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[1][HCALOther  ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[1][LCAL       ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[1][LHCAL      ][DecoderString]),
	CellIDDecoder<CalorimeterHit>(hitString[1][MUON       ][DecoderString]),
};

ArborHit3::ArborHit3(CalorimeterHit* hit, CaloHitType hitType, int staveOffset, int subID)
	         : m_Layer(-1), m_Module(-1), m_Stave(-1), 
			   m_Type(hitType), m_LCIOCaloHit(hit), m_IsClustered(false),
	           m_hitPos(m_LCIOCaloHit->getPosition()),
			   m_hitTime(m_LCIOCaloHit->getTime()),
			   m_hitEnergy(m_LCIOCaloHit->getTime()),
			   m_subD(subID),
			   m_IsMerged(false), m_IsDropped(false), m_IsStartingHit(false)

{ 
	if(hitType==UnknownType) return;

	std::string& cellIDLayerString  = hitString[1][hitType][LayerString];
	std::string& cellIDModuleString = hitString[1][hitType][ModuleString];
	std::string& cellIDStaveString  = hitString[1][hitType][StaveString];

	CellIDDecoder<CalorimeterHit>& decoder = m_decoders[hitType];

	if(cellIDLayerString.length() ) { m_Layer  = decoder(m_LCIOCaloHit)[cellIDLayerString];  }
	if(cellIDModuleString.length()) { m_Module = decoder(m_LCIOCaloHit)[cellIDModuleString]; }
	if(cellIDStaveString.length() ) { m_Stave  = decoder(m_LCIOCaloHit)[cellIDStaveString];  }
	
	m_Stave = m_Stave + staveOffset;

	//m_hitPos = m_LCIOCaloHit->getPosition();

	// The time information in SimCalorimeterHit is used.
    const LCObject* rawHit = m_LCIOCaloHit->getRawHit();
	const SimCalorimeterHit *simHit = dynamic_cast<const SimCalorimeterHit*>(rawHit);

	float hitTime = 0.;

    try{ 
		//std::cout << "cont: " << simHit->getNMCContributions() << std::endl;

	    float hitEnergy = 0.;

		for(int iCont=0; iCont<simHit->getNMCContributions(); ++iCont) {
			float aHitE = simHit->getEnergyCont(iCont);
			float aHitT = simHit->getTimeCont(iCont);

			hitTime = hitTime*hitEnergy + aHitT*aHitE;
			hitEnergy += aHitE;

			hitTime = hitTime/hitEnergy;
		}

		m_hitTime = hitTime;

		//std::cout << "time: " << hitTime << std::endl;

    } catch(std::exception& e){}
}

double ArborHit3::getDistanceWith(ArborHit3& hit)
{
	TVector3 posA = GetPosition();
	TVector3 posB = hit.GetPosition();

	return (posA - posB).Mag();
}

void ArborHit3::mergeHit(ArborHit3& hit)
{
	TVector3 posA = GetPosition();
	TVector3 posB = hit.GetPosition();

	double energyA = GetEnergy();
	double energyB = hit.GetEnergy();

	double totalEn = energyA + energyB;

	SetPosition( (posA*energyA + posB*energyB)*(1./totalEn) );

	//std::cout << "setpos: " << m_hitPos.X() << ", " << m_hitPos.Y() << ", " << m_hitPos.Z() << ", en: " << totalEn << std::endl;
	SetEnergy(totalEn);

	double timeA = GetTime();
	double timeB = hit.GetTime();
	double hitTime = (timeA*energyA + timeB*energyB)/totalEn;

	SetTime(hitTime);

	SetMerged();
	hit.SetDropped();
}

void ArborHit3::MakeMergedHit()
{
    // this function is to make a hit for the close hits in a layer
    // there are five patterns of hits which can be merged into a single hit

	int linkedHitsSize = linkedHitsInLayer.size();

	switch (linkedHitsSize) {
		case 0: {
			SetMerged();
			break;
		}
		//-----------------------------------------------
		case 1: {
			auto& hit = linkedHitsInLayer[0];
			
			if(hit->getLinkedHitsInLayer().size()==1) {
				mergeHit(*hit);
			}

			break;
		}
		//-----------------------------------------------
		case 2: {
			auto& hit1 = linkedHitsInLayer[0];
			auto& hit2 = linkedHitsInLayer[1];

			if(hit1->getLinkedHitsInLayer().size()==2 &&
			   hit2->getLinkedHitsInLayer().size()==2) {
			   
			   auto& links = hit1->getLinkedHitsInLayer();

			   if(links[0]==hit2 || links[1]==hit2) {
				   mergeHit(*hit1);
				   mergeHit(*hit2);
#if 0
				   std::cout << "Merged 3 hits..." << std::endl;
#endif
			   }
			}

			break;
		}
		//-----------------------------------------------
		case 3: {
			auto& hit1 = linkedHitsInLayer[0];
			auto& hit2 = linkedHitsInLayer[1];
			auto& hit3 = linkedHitsInLayer[2];

			if(hit1->getLinkedHitsInLayer().size()==3 &&
			   hit2->getLinkedHitsInLayer().size()==3 &&
			   hit3->getLinkedHitsInLayer().size()==3 ) {
			
			  auto& links1 = hit1->getLinkedHitsInLayer();
			  auto& links2 = hit2->getLinkedHitsInLayer();

			  // check link of hit1 and hit2
			  bool link12 = links1[0]==hit2 || links1[1]==hit2 || links1[2]==hit2;
			  
			  // check link of hit1 and hit3
			  bool link13 = links1[0]==hit3 || links1[1]==hit3 || links1[2]==hit3;

			  // check link of hit2 and hit3
			  bool link23 = links2[0]==hit3 || links2[1]==hit3 || links2[2]==hit3;

			  if(link12 && link13 && link23) {
				  mergeHit(*hit1);
				  mergeHit(*hit2);
				  mergeHit(*hit3);
#if 0
				  std::cout << "Merged 4 hits..." << std::endl;
#endif
			  }
			}

			break;
		}
		//-----------------------------------------------
		default: {
			SetMerged();
		}
	}
}

void ArborHit3::print()
{
	if(IsDropped()) return;

   	std::cout << "Stave: "  << GetStave()       << ", " 
		      << "Module: " << GetModule()      << ", "
			  << "Layer: "  << GetLayer()       << ", "
			  << "X: "      << GetPosition().X() << ", "
			  << "Y: "      << GetPosition().Y() << ", "
			  << "Z: "      << GetPosition().Z() << ", "
			  << "Time: "   << GetTime()
	          << std::endl;
}

void ArborHit3::printHitVector(std::vector<ArborHit3>& hitVec)
{
	auto it = hitVec.begin();

	while(it != hitVec.end()) {
		ArborHit3& caloHit = *it;
		caloHit.print();
		++it;
	}
}
