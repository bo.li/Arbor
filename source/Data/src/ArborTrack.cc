#include "ArborTrack.h"

#include "HelixClass.h"	

// FIXME: should from GEAR
#include <DetectorPos.h>

void ArborTrack::EvaluateMomentum() 
{
	const double mass = 0.139;
	const double BField = 3.5;

	HelixClass helix;
	helix.Initialize_Canonical(mLCIOTrack->getPhi(),   mLCIOTrack->getD0(),        mLCIOTrack->getZ0(), 
			                   mLCIOTrack->getOmega(), mLCIOTrack->getTanLambda(), BField);

	// FIXME
	mTrackEnergy = mass*mass;

	TVector3 trackMomentum(helix.getMomentum()[0], helix.getMomentum()[1], helix.getMomentum()[2]);

	mTrackMomentum = trackMomentum.Mag();
	mTrackEnergy   = sqrt(mTrackEnergy + mTrackMomentum*mTrackMomentum);

	// FIXME
	mTrackMomentum = mTrackEnergy;

	mTrackTheta = trackMomentum.Theta();
	mTrackPhi   = trackMomentum.Phi();

	mTrackHit  = mLCIOTrack->getTrackerHits().size();

	mTrackEndPoint      = (mLCIOTrack->getTrackerHits()[mTrackHit-1])->getPosition();
	mTrackStartingPoint = (mLCIOTrack->getTrackerHits()[0])->getPosition();
}

void ArborTrack::EvaluateQuality()
{
	CheckPreInteraction();

	double trackD0 = mLCIOTrack->getD0();
	double trackZ0 = mLCIOTrack->getZ0();

	if( mTrackHit > 9 || 
		(fabs(mTrackEndPoint.Z()) > LStar - 500 && mTrackEndPoint.Perp() < TPCInnerRadius ) || 
		fabs(mTrackEndPoint.Z()) > ECALHalfZ - 200  )		// Min requirement for track quality
	{	// LStar - 500, suppose to be the last Disk Position
		if(mIsPreInteractionTrack) {
			//streamlog_out(MESSAGE) << "a track with preinteraction, so we drop it! " << mLCIOTrack << endl; 
			mTrackQuality = PreInteraction;
			return ; 
		}

		bool isHighQuality =  fabs(trackD0)<1 && fabs(trackZ0)<1;

		// region
		if((mTrackEnergy < 1.0 && fabs(mTrackTheta-1.57)< 0.4) || 
		   (fabs(mTrackTheta-1.57) >= 0.4 && log10(mTrackEnergy) < -(fabs(mTrackTheta-1.57)-0.4)*0.2/0.3 )) {
			mTrackQuality = isHighQuality ? LE : ILL; //type: 101, 100
		}
		else if( fabs(mTrackEndPoint.Z()) > ECALHalfZ - 500 && mTrackEndPoint.Perp() > TPCOuterRadius - 300  ) { //Shoulder
			mTrackQuality = isHighQuality ? HQ_Shoulder : MQ_Shoulder; //31,30
		}
		else if( fabs(mTrackEndPoint.Z()) > LStar - 500 && mTrackEndPoint.Perp() < TPCInnerRadius )	{	//Forward
			mTrackQuality = isHighQuality ? HQ_Forward : MQ_Forward; //41,40
		}
		else if( mTrackEndPoint.Perp() > TPCOuterRadius - 100 )	{	//Barrel
			mTrackQuality = isHighQuality ? HQ_Barrel: MQ_Barrel; //11,10
		}
		else if( fabs(mTrackEndPoint.Z()) > ECALHalfZ - 200 ) {		//Endcap
			mTrackQuality = isHighQuality ? HQ_Endcap : MQ_Endcap; //21,20
		}
		else if(isHighQuality) {
			mTrackQuality = VTX; //type: 1
		} 
		// two cases for type == 0
		else if (mTrackStartingPoint.Mag()>50 && mTrackEndPoint.Mag()<1000 && mTrackHit<50) {
			mTrackQuality = ILL;
		}
		else {
			mTrackQuality = LQ;
		}
	}
	else {
		// FIXME:: maybe should put to non-categorized tracks
		mTrackQuality = ILL;
	}
}

void ArborTrack::CheckPreInteraction()
{
	mIsPreInteractionTrack = false;

	//FIXME: make steering parameters
	//and the this cut condition. If always false, then make the same result with SortTrack. 
	if( mTrackEndPoint.Perp() < 1680 && mTrackEndPoint.Perp() > 400 && fabs(mTrackEndPoint.Z()) < 2000 ) { 
		//std::cout << "R: " << mTrackEndPoint.Perp() << ", Z: " << mTrackEndPoint.Z() << endl;
		mIsPreInteractionTrack = true;
	}
}
