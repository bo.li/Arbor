#include "ArborCaloCluster.h"

ArborCaloCluster::ArborCaloCluster()
	            : m_hits(0), m_isNearModuleBoundary(false), 
	            m_mergedByNearModuleCluster(0), m_isMergedByNearModuleCluster(false)
{ 
	m_Cluster.resize(MAXLAYER);
}

ARBORCALOHITS& ArborCaloCluster::GetHitsInLastLayer() 
{ 
	std::cout << "last layer is: " << m_LastLayer << std::endl;
	return m_Cluster[m_LastLayer];
}

ARBORCALOHITS& ArborCaloCluster::GetHitsInLayer(int layer)
{
   	return m_Cluster[layer];
}

void ArborCaloCluster::PutHitInLayer(ARBORCALOHIT hit, int layer)
{
	//std::cout << "    In putHitInLayer ";
	if(layer>=0&&layer<MAXLAYER) m_Cluster[layer].push_back(hit);
	
	hit->SetCluster(this);
	hit->SetClustered();

	if(hit->isNearModuleBoundary()) { 
		m_hitsNearBoundary.push_back(hit);
	   	m_isNearModuleBoundary = true;
	}

	m_LastLayer = layer;
	++m_hits;
}

ARBORCALOHITS& ArborCaloCluster::GetHitsNearBoundary() 
{ 
	return m_hitsNearBoundary; 
}

void ArborCaloCluster::MergeNearModuleCluster(ArborCaloCluster* cluster) 
{ 
	//std::cout << "In MergeNearModuleCluster " << std::endl;
	m_mergedNearModuleCluster.push_back(cluster);
   	cluster->m_mergedByNearModuleCluster= this;
   	cluster->m_isMergedByNearModuleCluster = true;
   	std::cout << "this cluster: " << this << " merges " << cluster << "with hits: " << cluster->GetHitsNumber()
	       	  << ", size of this subcluster: " << m_mergedNearModuleCluster.size() << std::endl;
}

std::vector<ArborCaloCluster*>& ArborCaloCluster::getMergedClusters() 
{ 
	return m_mergedNearModuleCluster; 
}
