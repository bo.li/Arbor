#ifndef _ARBORCALOCLUSTER_H_
#define _ARBORCALOCLUSTER_H_

#include <EVENT/CalorimeterHit.h>
#include <EVENT/LCCollection.h>
#include <UTIL/CellIDDecoder.h>

#include "ArborCaloHit.h"

// Store the hits layer by layer in the vector:
// the first vector is for all layers which has hit,
// the second vector is for hits in the same layer
// actualy the iterator of the sorted ArborCaloHit vector is stored
typedef std::vector<ArborCaloHit>::iterator              ARBORCALOHIT;
typedef std::vector<ARBORCALOHIT>                        ARBORCALOHITS;

const int MAXLAYER = 60;

class ArborCaloCluster
{
public:
	ArborCaloCluster();

	// I want to check we are really using a reference ...
	ARBORCALOHITS& GetHitsInLastLayer();

	ARBORCALOHITS& GetHitsInLayer(int layer);

	void PutHitInLayer(ARBORCALOHIT hit, int layer);

	ARBORCALOHITS& GetHitsNearBoundary();

	void MergeNearModuleCluster(ArborCaloCluster* cluster);

    std::vector<ArborCaloCluster*>& getMergedClusters();

	// GetClusterDistance(ArborCaloCluster& cluster);
	// writeCluster2LCIO()
	// GetCOG();

    inline int GetHitsNumber() { return m_hits; }

    inline bool isNearModuleBoundary() { return m_isNearModuleBoundary; }

    inline bool isMergedByNearModuleCluster() { return m_isMergedByNearModuleCluster; }

private:

	std::vector<ARBORCALOHITS> m_Cluster;
	ARBORCALOHITS    m_hitsNearBoundary;

	int m_LastLayer;
	int m_hits;

	// COG
	bool m_isNearModuleBoundary;

	std::vector<ArborCaloCluster*> m_mergedNearModuleCluster;
	ArborCaloCluster* m_mergedByNearModuleCluster;
	bool m_isMergedByNearModuleCluster;

	// isTrackLike;
};

#endif
