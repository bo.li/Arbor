// ArborHit3
// Here we try to merge the functions
// in ArborCaloHit and ArborHit (in the orginal Arbor)

#ifndef _ArborHit3_h_
#define _ArborHit3_h_

#include <EVENT/CalorimeterHit.h>
#include <EVENT/LCCollection.h>
#include <UTIL/CellIDDecoder.h>

#include <TVector3.h>

#include "ArborDef.h"

class ArborCaloCluster;

class ArborHit3
{
public:
	ArborHit3(CalorimeterHit* hit, CaloHitType hitType, int layerShift, int subID);

	inline int GetStave () const { return m_Stave;  }
	inline int GetLayer () const { return m_Layer;  }
	inline int GetModule() const { return m_Module; }

	inline       float  GetEnergy  () { return m_LCIOCaloHit->getEnergy();   }
	inline       float  GetTime    () { return m_hitTime;                    }
	//inline const float* GetPosition() { return m_LCIOCaloHit->getPosition(); }
	inline CaloHitType  GetHitType () { return m_Type;                       }

	inline CalorimeterHit*   GetLCIOHit() { return m_LCIOCaloHit; }
	inline ArborCaloCluster* GetCluster() { return m_ClusteredBy; }

	inline void SetClustered() { m_IsClustered = true; }
	inline void SetCluster(ArborCaloCluster* cluster) { m_ClusteredBy = cluster; }

	inline bool isClustered()  { return m_IsClustered; }

	double getDistanceWith(ArborHit3& hit);

	inline int GetCellID0() { return m_LCIOCaloHit->getCellID0(); }
	inline int GetCellID1() { return m_LCIOCaloHit->getCellID1(); }

	void print();

	void addLinkedHitInLayer(std::vector<ArborHit3>::iterator hit) { linkedHitsInLayer.push_back(hit); }

	std::vector< std::vector<ArborHit3>::iterator >& getLinkedHitsInLayer() {return linkedHitsInLayer; }
	
	static void printHitVector(std::vector<ArborHit3>& hitVec);

	void mergeHit(ArborHit3& hit);

	void SetPosition(TVector3 pos) { m_hitPos = pos; }

	void SetEnergy(double en) { m_hitEnergy = en; }
	void SetTime  (double t ) { m_hitTime   = t;  }

	inline void SetMerged() { m_IsMerged = true; }
	inline void SetDropped() { m_IsDropped = true; }
	inline void SetStartingHit() { m_IsStartingHit = true; }
	inline void SetIsoHit(bool isIso = true) { m_IsIsoHit = isIso; }

	inline bool IsMerged() { return m_IsMerged; }
	inline bool IsDropped() { return m_IsDropped; }
	inline bool IsStartingHit() { return m_IsStartingHit; }

	inline bool IsIsoHit() { return m_IsIsoHit; }

	void MakeMergedHit();

	/////////// implement the functions in the class of ArborHit
	inline int GetSubD() { return m_subD; }
	inline int GetStave() { return m_Stave; }
	inline TVector3 GetPosition() { return m_hitPos; }

private:
	int m_Layer;
	int m_Module;
	int m_Stave;

	CaloHitType m_Type;

	CalorimeterHit* m_LCIOCaloHit;

	bool m_IsClustered;

	ArborCaloCluster* m_ClusteredBy;

	TVector3 m_hitPos;
	float    m_hitTime;
	float    m_hitEnergy;

	int m_subD;

	std::vector< std::vector<ArborHit3>::iterator > linkedHitsInLayer;

	bool m_IsMerged;
	bool m_IsDropped;
	bool m_IsStartingHit;
	bool m_IsIsoHit;

	static std::string hitString[2][CALOHITTYPES][STRINGTYPES];

	static CellIDDecoder<CalorimeterHit> m_decoders[CALOHITTYPES];
};

struct arborCaloHitCompare {
    bool operator() (const ArborHit3& hitA, const ArborHit3& hitB) {
		// stave > module > layer, 
		// and smaller number will be ahead when sorting hit
		return hitA.GetStave()  != hitB.GetStave()   ?  hitA.GetStave() < hitB.GetStave() : ( // if staves equal, check module
		       hitA.GetModule() != hitB.GetModule()  ?  hitA.GetModule() < hitB.GetModule() : // if modules still equal, check layer
		       hitA.GetLayer()  < hitB.GetLayer() ) ;
	}
};

#endif
