#ifndef _ArborCaloHit_h_
#define _ArborCaloHit_h_

#include <EVENT/CalorimeterHit.h>
#include <EVENT/LCCollection.h>
#include <UTIL/CellIDDecoder.h>

#include "ArborDef.h"

class ArborCaloCluster;

class ArborCaloHit
{
public:
	ArborCaloHit(CalorimeterHit* hit, CaloHitType hitType);

	inline int GetStave () const { return m_Stave;  }
	inline int GetLayer () const { return m_Layer;  }
	inline int GetModule() const { return m_Module; }

	inline       float  GetEnergy  () { return m_LCIOCaloHit->getEnergy();   }
	inline       float  GetTime    () { return m_LCIOCaloHit->getTime();     }
	inline const float* GetPosition() { return m_LCIOCaloHit->getPosition(); }
	inline CaloHitType  GetHitType () { return m_Type;                       }

	inline CalorimeterHit*   GetLCIOHit() { return m_LCIOCaloHit; }
	inline ArborCaloCluster* GetCluster() { return m_ClusteredBy; }

	inline void SetClustered() { m_IsClustered = true; }
	inline void SetCluster(ArborCaloCluster* cluster) { m_ClusteredBy = cluster; }
	inline void SetNearModuleBoundary(bool nearBoundary = true) { m_IsNearModuleBoundary = nearBoundary; }

	inline bool isClustered()  { return m_IsClustered; }
	inline bool isNearModuleBoundary() { return m_IsNearModuleBoundary; }

	inline void SetNearSides( std::vector< std::pair<ModuleSide, double> > sides ) { m_nearSide = sides; }
	inline std::vector< std::pair<ModuleSide, double> > GetNearSides() { return m_nearSide; }

	double getDistanceWith(ArborCaloHit& hit);

	void print();

	static void printHitVector(std::vector<ArborCaloHit>& hitVec);

private:
	int m_Layer;
	int m_Module;
	int m_Stave;

	CaloHitType m_Type;

	CalorimeterHit* m_LCIOCaloHit;

	bool m_IsClustered;
	bool m_IsNearModuleBoundary;

	ArborCaloCluster* m_ClusteredBy;

	std::vector< std::pair<ModuleSide, double> > m_nearSide; 

	static std::string hitString[CALOHITTYPES][STRINGTYPES];
	static CellIDDecoder<CalorimeterHit> m_decoders[CALOHITTYPES];
};

struct arborCaloHitCompare {
    bool operator() (const ArborCaloHit& hitA, const ArborCaloHit& hitB) {
		// stave > module > layer, 
		// and smaller number will be ahead when sorting hit
		return hitA.GetStave()  != hitB.GetStave()   ?  hitA.GetStave() < hitB.GetStave() : ( // if staves equal, check module
		       hitA.GetModule() != hitB.GetModule()  ?  hitA.GetModule() < hitB.GetModule() : // if modules still equal, check layer
		       hitA.GetLayer()  < hitB.GetLayer() ) ;
	}
};

#endif
