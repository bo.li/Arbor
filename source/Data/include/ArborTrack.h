#ifndef _ArborTrack_h_
#define _ArborTrack_h_

#include <TObject.h>
#include <TVector3.h>
#include <EVENT/Track.h>

using namespace EVENT;

enum TrackQuality { HQ_Endcap = 0, HQ_Barrel, 
                    MQ_Endcap,     MQ_Barrel, 
	                HQ_Shoulder,   MQ_Shoulder, 
	                HQ_Forward,    MQ_Forward,
		            VTX,           LQ,        
					LE,            ILL, 
					PreInteraction 
                  };

class ArborTrack : public TObject
{
public:
	ArborTrack(Track* track) 
	   : mLCIOTrack(track)
	{
		EvaluateMomentum();
		EvaluateQuality();
	}

	//~ArborTrack();

    // -1 : this is smaller than the obj
    //  0 : this is equal to obj
    //  1 : this is greater than obj
    int Compare(const TObject* obj) const
    {   
		const ArborTrack* compTrack = dynamic_cast<const ArborTrack*>(obj);

        int    objQuality  = compTrack->mTrackQuality;
        double objMomentum = compTrack->mTrackMomentum;

        return mTrackQuality == objQuality ? 
               ( mTrackMomentum == objMomentum ? 0 : (mTrackMomentum < objMomentum ? 1 : -1) ) : 
               ( mTrackQuality > objQuality ? 1 : -1 ) ; 
    }   

    inline bool   IsSortable  () const       { return kTRUE;       }
	inline void   SetLCIOTrack(Track* track) { mLCIOTrack = track; }

	inline Track*       GetLCIOTrack() { return mLCIOTrack;     }
	inline double       GetMomentum()  { return mTrackMomentum; }
	inline TrackQuality GetQuality ()  { return mTrackQuality;  }

	inline void SetTrackIndex(int index) { mIndex = index; }
	inline int  GetTrackIndex()          { return mIndex;  }

	void EvaluateMomentum();
	void EvaluateQuality();

	void CheckPreInteraction();

private:
	TrackQuality mTrackQuality;

	double       mTrackMomentum;
	double       mTrackEnergy;
	double       mTrackTheta;
	double       mTrackPhi;

	TVector3     mTrackStartingPoint;
	TVector3     mTrackEndPoint;

	int          mIndex;
	int          mTrackHit;

	Track*       mLCIOTrack;

	bool         mIsPreInteractionTrack;
};

#endif
